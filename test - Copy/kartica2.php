<?Php
///////////////////////////////////////////  kartica //////////////////////////////////



require('fpdf.php');
require_once('../private/classes.php');

$year = isset($_GET['year']) ? $_GET['year'] : date('Y'); 
$customer_id = isset($_GET['customer_id']) ? $_GET['customer_id'] : null; 
$modul_id = isset($_GET['modul_id']) ? $_GET['modul_id'] : null; 
$ugovor_id = isset($_GET['ugovor_id']) ? $_GET['ugovor_id'] : null; 

$text_ugovor ="";
if($ugovor_id != null){
	$text_ugovor = " and Ugovor_no = '".$ugovor_id."' ";
}

$customer = new customer($customer_id);
$modul = new atribut($modul_id);

$pdf = new FPDF(); 
$pdf->AddPage();
$pdf->SetFont('Arial','B',12);
$width=$pdf->GetPageWidth(); // Width of Current Page
$height=$pdf->GetPageHeight(); // Height of Current Page
$width_cell=array(40,20,30,15,15,15,15,20);

$pdf->Image('logo.png',10,6,30);
$pdf->SetFont('Arial','B',15);
$pdf->Cell(50);

$title ='Opstina BAR Sekretarijat za ekonomiju i finansije '; 
$pdf->MultiCell(100,10,$title);
$y = $pdf->GetY();
$pdf -> Line(20, $y+8 , $width-20, $y+8);  
$pdf->Ln(4);
$pdf->Ln(6);

$pdf->SetFillColor(255,255,255);
$pdf->SetFont('Arial','B',10);		
$pdf -> SetX(75);
$pdf->Cell(50,10,'FINANSIJSKA KARTICA',0,0,'C',true); 
$pdf->SetFillColor(255,255,255);
$pdf->SetFont('Arial','B',10);		
$pdf -> SetX(170);
$pdf->Cell(20,10,date('Y-m-d'),0,0,'C',true); 
$pdf->Ln(4);
$pdf->Ln(4);

$pdf->SetFillColor(255,255,255);
$pdf->SetFont('Arial','',8);		
$pdf -> SetX(75);

$pdf->MultiCell(70,5,$modul->Name,'','L');
// die();
$dbhost=Configuration::$dbInfo['dbhost'];
$dbuser=Configuration::$dbInfo['dbuser'];
$dbpass=Configuration::$dbInfo['dbpass'];
$dbname=Configuration::$dbInfo['dbname'];
$connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
$connection2=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);

mysqli_set_charset($connection,"utf8");
mysqli_set_charset($connection2,"utf8");
// $output="";  
  
    $pdf -> SetX(20);
		$pdf->SetFillColor(255,255,255);
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(100,8,"Obveznik:  ".$customer->Name,0,0,'L',true); // First header column 
		$pdf->Ln(4); 
		$pdf->Ln(4);  	

		$y = $pdf->GetY();
	  	$pdf -> Line(20, $y , $width-20, $y);   	        				 
		$pdf -> Line(20, $y +1, $width-20, $y +1);   	        		
		$pdf->Ln(4);		

		// $temp_Ugovor_date = $list['Ugovor_date'];
		// if($temp_Ugovor_date != null and $temp_Ugovor_date != ""){
  //        $temp_Ugovor_date = date("Y-m-d", strtotime($list['Ugovor_date']));
	 //    }  
		// $pdf->Cell(150,8,'Datum sklapanja ugovora: '.$temp_Ugovor_date,0,0,'L',true); // First header

		// $pdf->Ln(4); 
		// $pdf->Ln(4);  
		$pdf->SetFillColor(193,229,252);
		$pdf->SetFont('Arial','B',5);
		$pdf -> SetX(20);
		$pdf->Cell(30,5,'Broj rijesenja',1,0,'C',true); // First header column 
		// $pdf->Cell(10,5,'Rata broj',1,0,'C',true); // First header column 
		$pdf->Cell(20,5,'Datum rijesenja',1,0,'C',true); // First header column 
		$pdf->Cell(20,5,'Nalog',1,0,'C',true); // First header column 
		$pdf->Cell(20,5,'Izvod',1,0,'C',true); // First header column 
		$pdf->Cell(20,5,'Datum uplate',1,0,'C',true); // First header column 
		$pdf->Cell(20,5,'Duguje',1,0,'C',true); // Second header column
		$pdf->Cell(20,5,'Potrazuje',1,0,'C',true); // Third header column 
		$pdf->Cell(20,5,'Saldo',1,1,'C',true); // Third header column 
		// $pdf->Cell(45,5,'Napomena',1,1,'C',true); // Fourth header column	
		// $pdf->Ln(2);	 	

		$sql2 = " select * from bar.orders where Customer_id=".$customer->Id." and Type in ('Avans','Pocetno_Avans') and ".$year." = YEAR(Created_date) ".$text_ugovor." order by Id asc ";
		// $sql2 = " select * from bar.orders where Type in ('Uplata','Storno','Info') and Status!='Deleted'  and Uplata_poziv_na_broj = ". $list['Id'] ;
		$duguje_sum="0.00";
		$potrazuje_sum="0.00";
    $results2 = mysqli_query($connection2, $sql2);
    if(mysqli_num_rows($results2)) {        	
        	    		       		 
			$i = 0;
			while($list2 = mysqli_fetch_assoc($results2)) {				
        $pdf -> SetX(20);
				$pdf->SetFont('Arial','',5);				
				$pdf->Cell(30,3,$list2['Ugovor_no'],1,0,'C',false); // First column of row 1 

				$Ugovor_date = "";
				if($list2['Ugovor_date'] != null and $list2['Ugovor_date'] != ""){
		         $Ugovor_date = date("Y-m-d", strtotime($list2['Ugovor_date']));
		    } 
				$pdf->Cell(20,3,$Ugovor_date,1,0,'C',false); // First column of row 1

				$Nalog = "";
				if($list2['Nalog'] != null and $list2['Nalog'] != ""){
		         $Nalog = $list2['Nalog'];
		    } 
				$pdf->Cell(20,3,$Nalog,1,0,'C',false); // First column of row 1 

				$Bank_account = "";
				if($list2['Bank_account'] != null and $list2['Bank_account'] != ""){
		         $Bank_account =$list2['Bank_account'];
		    } 
				$pdf->Cell(20,3,$Bank_account,1,0,'C',false); // First column of row 1 

				$temp_Uplata_date = $list2['Uplata_date'];
				if($temp_Uplata_date != null and $temp_Uplata_date != ""){
		         $temp_Uplata_date = date("Y-m-d", strtotime($list2['Uplata_date']));
			    } 
				$pdf->Cell(20,3,$temp_Uplata_date,1,0,'C',false); // Second column of row 1 

				$pdf->SetFont('Arial','B',8);
				$temp_duguje='0.00';
				if($list2['Zaduzenje_iznos'] != null){
					$temp_duguje = $list2['Zaduzenje_iznos'];
				}
				$pdf->Cell(20,3,number_format($temp_duguje, 2, '.', ','),1,0,'R',false); // Third column of row 1 
				$duguje_sum=$duguje_sum + $temp_duguje;

				$temp_potrazuje='0.00';
				if($list2['Zaduzenje_uplaceno'] != null){
					$temp_potrazuje = $list2['Zaduzenje_uplaceno'];
				}
				$pdf->Cell(20,3,number_format($temp_potrazuje, 2, '.', ','),1,0,'R',false); // Fourth column of row 1
				$potrazuje_sum=$potrazuje_sum + $temp_potrazuje; 

				$pdf->Cell(20,3,number_format((0-($temp_duguje-($temp_potrazuje))), 2, '.', ','),1,1,'R',false);

      }
    } 
        
		$pdf->Ln(2);     
		$pdf -> SetX(110);
		$pdf->Cell(17,4,'UKUPNO:',1,0,'C',true); // Second header column
		$pdf -> SetX(130);
		$pdf->Cell(20,4,number_format($duguje_sum, 2, '.', ','),1,0,'R',true); // Second header column
		$pdf -> SetX(152);
		$pdf->Cell(18,4,number_format($potrazuje_sum, 2, '.', ','),1,0,'R',true); // Second header column
		$pdf -> SetX(172);
		$pdf->Cell(18,4,number_format(( 0 - (($duguje_sum)-($potrazuje_sum))), 2, '.', ','),1,0,'R',true); // Second header column
		$pdf->Ln(4);
		$pdf->Ln(4);
            
/////////////////////////////////////////////////////////////////////////////////////

$pdf->Output();

?>