<?Php
//////////////////////////// Analitička kartica svih obveznika /////////////////////

// Opis:
// Ova forma izvještaja je karakteristična za modul komunalije, na slici možete vidjeti kako izgleda, dakle zbirni
// izvještaj obveznika komunalija sa podacima kao na slici(iznos rate, datum uplate itd)

// http://localhost:3272/Knjiga/test/izvjestaj1.php?modul_id=1&datum_do=2021-08-01&datum_od=2020-01-01&customer_id=2

require('fpdf.php');
require_once("../private/classes.php");

$modul_id = isset($_GET['modul_id']) ? $_GET['modul_id'] : null;
$datum_od = isset($_GET['datum_od']) ? $_GET['datum_od'] : null;
$datum_do = isset($_GET['datum_do']) ? $_GET['datum_do'] : null;
$customer_id = isset($_GET['customer_id']) ? $_GET['customer_id'] : null;
// var_dump($_GET);

$modul_id_text = " ";
if($modul_id != null){
	$modul_id_text = " and Modul_id=".$modul_id." ";
}

$customer_id_text = " ";
if($customer_id != null){
	$customer_id_text = " where Id=".$customer_id." ";
}

//'2021-08-01' > Ugovor_date
$target_year = date("Y");

$datum_do_text = "";
if($datum_do != null && $datum_do !=""){
	$datum_do_text = " and ('".$datum_do."' >= Ugovor_date or '".$datum_do."' >= Uplata_date )" ;
	$datum_do_year = DateTime::createFromFormat("Y-m-d", $datum_do);
	if( $datum_do_year->format("Y") != $target_year ) {
		$target_year = $datum_do_year->format("Y");
	}
}

$datum_od_text = "";
if($datum_od != null && $datum_od !=""){
	$datum_od_text = " and ('".$datum_od."' <= Ugovor_date or '".$datum_od."' <= Uplata_date )";
	$datum_od_year = DateTime::createFromFormat("Y-m-d", $datum_od);
	if($datum_do != null && $datum_do !=""){
		$datum_do_year = DateTime::createFromFormat("Y-m-d", $datum_do);
		// var_dump($datum_od_year->format("Y"));
		// var_dump($datum_do_year->format("Y"));
		if( $datum_od_year->format("Y") != $datum_do_year->format("Y")) {
			$datum_od_text = " and '".$datum_do_year->format('Y')."-01-01' <= Ugovor_date ";
		}
	}
	else {
		if( $datum_od_year->format("Y") != $target_year ) {
			$target_year = $datum_od_year->format("Y");
		}
	}	
}

$pdf = new FPDF(); 
$pdf->AddPage();
$pdf->SetFont('Arial','B',12);
$width=$pdf->GetPageWidth(); // Width of Current Page
$height=$pdf->GetPageHeight(); // Height of Current Page
$width_cell=array(5,10,30,20,20,20,20,20);

$pdf->Image('logo.png',10,6,30);
// Arial bold 15
$pdf->SetFont('Arial','B',8);
// Move to the right
$pdf->Cell(50);
// Title
$modul_title_text = "za sve module ";
if($modul_id != null and $modul_id!=""){
	$modul = new atribut($modul_id);
	$modul_title_text = "samo za modul:".$modul->Name." ";
}

$customer_title_text = "za sve klijente ";
if($customer_id != null and $customer_id!=""){
	$cust = new customer($customer_id);
	$customer_title_text = "samo za klijenta:".$cust->Name." ";
}

$Datum_Od_title_text = " ";
if($datum_od != null and $datum_od!=""){	
	$Datum_Od_title_text = "Datum Od :".$datum_od." ";
}

$Datum_Do_title_text = " ";
if($datum_do != null and $datum_do!=""){	
	$Datum_Do_title_text = "Datum Do :".$datum_do." ";
}

$title ='Izvjestaj: Analiticka kartica '.$modul_title_text. $customer_title_text.$Datum_Od_title_text.$Datum_Do_title_text;  
// $title = iconv('UTF-8', 'CP1250//TRANSLIT', $title);
// $text = preg_replace("#[^a-zA-z]#", "", $text);
$pdf->MultiCell(100,10,$title);
// $pdf->Cell(100,10,'Izvještaj: Naziv Izvještaja',1,0,'C');
// Line break
$pdf->Ln(6);

$dbhost=Configuration::$dbInfo['dbhost'];
$dbuser=Configuration::$dbInfo['dbuser'];
$dbpass=Configuration::$dbInfo['dbpass'];
$dbname=Configuration::$dbInfo['dbname'];
$connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
$connection2=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
$connection3=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
$connection4=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
// $output="";  
mysqli_set_charset($connection,"utf8");
if (mysqli_connect_error($connection)){
throw new Exception("Problem sa konekcijom nad bazom: ".mysqli_connect_errno($connection).". Molimo kontaktirajte administratora portala.");  
} 
$sql_customer = " select * from customers ".$customer_id_text." order by Name asc; "  ;
$results_customer = mysqli_query($connection3, $sql_customer);
if(mysqli_num_rows($results_customer)) {	
    while($list_customer = mysqli_fetch_assoc($results_customer)) {   

      	$customer_id= $list_customer['Id'];
      	$customer_name= $list_customer['Name'];
		$sql = " select * from orders  where Customer_id=".$customer_id." and ".$target_year." = YEAR(Ugovor_date) and Type in ('Zaduzenje','Pocetno_stanje') ". $modul_id_text . $datum_do_text . $datum_od_text  . "  order by Id asc";
		// echo $sql;
		// die();
		$results = mysqli_query($connection, $sql);
		if(mysqli_num_rows($results)) {
		    while($list = mysqli_fetch_assoc($results)) {    

		      	$zaduzenje_id= $list['Id'];
		        $zaduzenje = new order($list['Id']);		        
		    	$y = $pdf->GetY();
		      	$pdf -> Line(20, $y , $width-20, $y);      	
		      	$pdf -> Line(20, $y+1 , $width-20, $y+1);      			       
				$pdf->Ln(3); 
				$pdf -> SetX(20);
				$pdf->SetFillColor(	255,255,255);
				$pdf->SetFont('Arial','',5);
				$pdf->Cell(50,3,'Korisnik:',1,0,'C',true); // First header column 
				$pdf -> SetX(100);
				$pdf->Cell(50,3,'Ukupno zaduzenje',1,0,'C',true); // Second header column
				$pdf -> SetX(160);
				$pdf->Cell(30,3,'Broj rata',1,0,'C',true); // Second header column
				$pdf->Ln(4);   
				$pdf -> SetX(20);
				$pdf->SetFillColor(	255,255,255);
				$pdf->SetFont('Arial','',5);
				$pdf->Cell(50,4,$customer_name,0,0,'C',true); // First header column 
				$pdf -> SetX(100);
				$pdf->Cell(50,4,$zaduzenje->Zaduzenje_iznos,0,0,'C',true); // Second header column
				$pdf -> SetX(160);
				if($zaduzenje->Broj_rata != null){
					$pdf->Cell(30,4,$zaduzenje->Broj_rata,0,0,'C',true); // Second header column
				}
				else {
					$pdf->Cell(30,4,"0",0,0,'C',true); // Second header column	
				}
				$pdf->Ln(6); 
				$pdf -> SetX(20);
				$pdf->SetFillColor(	255,255,255);
				$pdf->Cell(20,3,'Broj ugovora: ',1,0,'C',true); // Second header column
				$pdf -> SetX(42);	
				$pdf->Cell(80,3,$zaduzenje->Ugovor_no,0,0,'L',true); // Second header column  		  
				$pdf -> SetX(20);
				$pdf->SetFillColor(	255,255,255);
				$pdf -> SetX(160);
				$pdf->Cell(18,3,'Datum dospijeca:',1,0,'C',true); // Second header column
				$pdf -> SetX(180);
				$temp_Ugovor_date = $zaduzenje->Ugovor_date;
				if($zaduzenje->Ugovor_date != null and $zaduzenje->Ugovor_date!=""){
		         $temp_Ugovor_date = date("Y-m-d", strtotime($zaduzenje->Ugovor_date));
			    }  
				$pdf->Cell(20,3,$temp_Ugovor_date,0,0,'L',true); // Second header column
		    	// $pdf->Ln(4);   
		    	$pdf->Ln(4);   
				$sql2 = " select * from orders where Uplata_poziv_na_broj=".$list['Id']." and Type in ('Uplata','Storno','Info') and Status!='Deleted' order by Id asc";
		        $results2 = mysqli_query($connection2, $sql2);
		        $pdf->SetFillColor(193,229,252);
				$pdf->SetFont('Arial','',5);
				$pdf -> SetX(20);
				$pdf->Cell($width_cell[0],5,'#',1,0,'C',true); // First header column 
				$pdf->Cell($width_cell[1],5,'Tip',1,0,'C',true); // First header column 
				$pdf->Cell($width_cell[2],5,'Rata broj',1,0,'C',true); // First header column 
				$pdf->Cell($width_cell[3],5,'Datum rate',1,0,'C',true); // Second header column
				$pdf->Cell($width_cell[4],5,'Iznos rate',1,0,'C',true); // Third header column 
				$pdf->Cell($width_cell[4],5,'Nalog',1,0,'C',true); // Third header column 
				$pdf->Cell($width_cell[5],5,'Izvod',1,0,'C',true); // Third header column 
				$pdf->Cell($width_cell[6],5,'Datum_uplate',1,0,'C',true); // Fourth header column
				$pdf->Cell($width_cell[7],5,'Uplaceno',1,1,'C',true); // Fourth header column				
		        if(mysqli_num_rows($results2)) {        			        	   		
					$i=0; 
					while($list2 = mysqli_fetch_assoc($results2)) {
				      	$i++;
				        $pdf -> SetX(20);
						$pdf->SetFont('Arial','',5);
						$temp_znak_prefix = "";
						if($list2['Type'] == "Storno"){
							$temp_znak_prefix = "- ";
						}

						// Rows of data 
						$pdf->Cell($width_cell[0],5,$i,0,0,'C',false); // First column of row 1 						
						if($list2['Type'] == "Info"){
							$pdf->Cell($width_cell[1],5,"Zaduzenje",0,0,'C',false); // First column of row 1 
						}
						else {
							$pdf->Cell($width_cell[1],5,$list2['Type'],0,0,'C',false); // First column of row 1 	
						}
						if($list2['Komentar'] != null){
							$pdf->Cell($width_cell[2],5,$list2['Komentar'],0,0,'C',false); // First column of row 1 
						}
						else {
							$pdf->Cell($width_cell[2],5,"",0,0,'C',false); // First column of row 1 	
						}
						$temp_Uplata_rata_date = $list2['Uplata_rata_date'];						
						if($temp_Uplata_rata_date != null and $temp_Uplata_rata_date != ""){
				         $temp_Uplata_rata_date = date("Y-m-d", strtotime($list2['Uplata_rata_date']));
					    }  
						$pdf->Cell($width_cell[3],5,$temp_Uplata_rata_date,0,0,'C',false); // Second column of row 1 					
						if($list2['Type'] == "Info"){
							$pdf->Cell($width_cell[4],5,$zaduzenje->Zaduzenje_iznos,0,0,'C',false); // Third column of row 1 
						}
						else {
							$pdf->Cell($width_cell[4],5,$zaduzenje->Iznos_jedne_rate,0,0,'C',false); // Third column of row 1 
						}
						$pdf->Cell($width_cell[4],5,$list2['Nalog'],0,0,'C',false); // Third column of row 1 
						$pdf->Cell($width_cell[5],5,$list2['Bank_account'],0,0,'C',false); // Fourth column of row 1 

						$temp_Uplata_date = $list2['Uplata_date'];
						if($temp_Uplata_date != null and $temp_Uplata_date != ""){
				         $temp_Uplata_date = date("Y-m-d", strtotime($list2['Uplata_date']));		    
					    } 
						$pdf->Cell($width_cell[6],5,$temp_Uplata_date,0,0,'C',false); // Fourth column of row 1
						$pdf->Cell($width_cell[7],5,$temp_znak_prefix.$list2['Uplata_iznos'],0,1,'C',false); // Fourth column of row 1
			        }	         
			    }
			    $pdf -> SetX(65);
			    $pdf->Cell(20,4,'Duguje: ',1,0,'C',true); // Second header column
				$pdf -> SetX(87);
				$pdf->Cell(15,4,$zaduzenje->Zaduzenje_iznos,0,0,'C',true); // Second header column
		        $pdf -> SetX(145);
				$pdf->Cell(20,4,'Ukupno uplaceno:',1,0,'C',true); // Second header column
				$pdf -> SetX(167);
				$pdf->Cell(15,4,$zaduzenje->Zaduzenje_uplaceno,0,0,'C',true); // Second header column
		        $pdf->Ln(4);
		        $pdf->Ln(4);		        
		   }
		}
		$sql_avans = " select * from orders  where Customer_id=".$customer_id." and  Type in ('Avans','Pocetno_stanje') ". $modul_id_text . $datum_do_text . $datum_od_text  . "  order by Id asc";		
		// echo $sql_avans;
		// die();
		$results_avans = mysqli_query($connection4, $sql_avans);
		if(mysqli_num_rows($results_avans)) {
			$y = $pdf->GetY();
	      	$pdf -> Line(20, $y , $width-20, $y);      	
	      	$pdf -> Line(20, $y+1 , $width-20, $y+1);      			       
			$pdf->Ln(3); 
			$pdf -> SetX(20);
			$pdf->SetFillColor(	255,255,255);
			$pdf->SetFont('Arial','',5);
			$pdf->Cell(50,3,'Korisnik:',1,0,'C',true); // First header column 
			$pdf -> SetX(100);
			// $pdf->Cell(50,3,'Ukupno zaduzenje',1,0,'C',true); // Second header column
			$pdf -> SetX(160);
			// $pdf->Cell(30,3,'Broj rata',1,0,'C',true); // Second header column
			$pdf->Ln(4);   
			$pdf -> SetX(20);
			$pdf->SetFillColor(	255,255,255);
			$pdf->SetFont('Arial','',5);
			$pdf->Cell(50,4,$customer_name,0,0,'C',true); // First header column 
			$pdf -> SetX(100);
			$y_temp_ukupno_zaduzenje = $pdf->GetY();
			// $pdf->Cell(50,4,"0.00",0,0,'C',true); // Second header column
			$pdf -> SetX(160);				
			// $pdf->Cell(30,4,"0",0,0,'C',true); // Second header column					
			$pdf->Ln(6); 
			$pdf -> SetX(20);
			$pdf->SetFillColor(	255,255,255);
			// $pdf->Cell(20,3,'Broj ugovora: ',1,0,'C',true); // Second header column
			$pdf -> SetX(42);	
			// $pdf->Cell(80,3,"",0,0,'L',true); // Second header column  		  
			$pdf -> SetX(20);
			$pdf->SetFillColor(	255,255,255);
			$pdf -> SetX(160);
			// $pdf->Cell(18,3,'Datum dospijeca:',1,0,'C',true); // Second header column
			$pdf -> SetX(180);				
			// $pdf->Cell(20,3,"",0,0,'L',true); // Second header column
	    	// $pdf->Ln(4);   
	    	$pdf->SetFillColor(193,229,252);
			$pdf->SetFont('Arial','',5);
			$pdf -> SetX(20);
			$pdf->Cell($width_cell[0],5,'#',1,0,'C',true); // First header column 
			$pdf->Cell(30,5,'Broj ugovora',1,0,'C',true); // First header column 
			$pdf->Cell(10,5,'Rata broj',1,0,'C',true); // First header column 
			$pdf->Cell($width_cell[3],5,'Datum dospijeca',1,0,'C',true); // Second header column
			$pdf->Cell($width_cell[4],5,'Iznos rate',1,0,'C',true); // Third header column 
			$pdf->Cell($width_cell[4],5,'Nalog',1,0,'C',true); // Third header column 
			$pdf->Cell($width_cell[5],5,'Izvod',1,0,'C',true); // Third header column 
			$pdf->Cell($width_cell[6],5,'Datum_uplate',1,0,'C',true); // Fourth header column
			$pdf->Cell($width_cell[7],5,'Uplaceno',1,1,'C',true); // Fourth header column		   			        	   		
			$i=0; 
			$duguje_sum = "0.00";
			$uplaceno_sum = "0.00";
		    while($list_avans = mysqli_fetch_assoc($results_avans)) {		    			        		        
		      	$i++;
		        $pdf -> SetX(20);
				$pdf->SetFont('Arial','',5);
				$pdf->Cell($width_cell[0],5,$i,0,0,'C',false); // First column of row 1 

				if($list_avans['Ugovor_no'] == null){
					$pdf->Cell(30,5,"",0,0,'C',false); // First column of row 1 
				}
				else {
					$pdf->Cell(30,5,$list_avans['Ugovor_no'],0,0,'C',false); // First column of row 1 	
				}
				if($list_avans['Broj_rata'] != null){
					$pdf->Cell(10,5,$list_avans['Broj_rata'],0,0,'C',false); // First column of row 1 
				}
				else {
					$pdf->Cell(10,5,"",0,0,'C',false); // First column of row 1 	
				}
				// if($list_avans['Ugovor_date'] != null){

				// }
				$temp_Datum_dospijeca = $list_avans['Ugovor_date'];						
				if($temp_Datum_dospijeca != null and $temp_Datum_dospijeca != ""){
		         $temp_Datum_dospijeca = date("Y-m-d", strtotime($list_avans['Ugovor_date']));
			    }  
			    else {
			    	$temp_Datum_dospijeca="";
			    }
				$pdf->Cell($width_cell[3],5,$temp_Datum_dospijeca,0,0,'C',false); // Second column of row 1 

				if($list_avans['Zaduzenje_iznos'] != null){
					$pdf->Cell($width_cell[4],5,$list_avans['Zaduzenje_iznos'],0,0,'C',false); // Third column of row 1 
					$duguje_sum = $duguje_sum + ($list_avans['Zaduzenje_iznos']);
				}
				else {
					$pdf->Cell($width_cell[4],5,"0.00",0,0,'C',false); // Third column of row 1 
				}

				if($list_avans['Nalog'] != null){
					$pdf->Cell($width_cell[4],5,$list_avans['Nalog'],0,0,'C',false); // Third column of row 1 
				}
				else {
					$pdf->Cell($width_cell[4],5,"",0,0,'C',false); // Third column of row 1 
				}

				if($list_avans['Bank_account'] != null){
					$pdf->Cell($width_cell[5],5,$list_avans['Bank_account'],0,0,'C',false); // Third column of row 1 
				}
				else {
					$pdf->Cell($width_cell[5],5,"",0,0,'C',false); // Third column of row 1 
				}

				// $pdf->Cell($width_cell[4],5,$list2['Nalog'],0,0,'C',false); // Third column of row 1 
				// $pdf->Cell($width_cell[5],5,$list2['Bank_account'],0,0,'C',false); // Fourth column of row 1 

				$temp_Uplata_date = $list_avans['Uplata_date'];
				if($temp_Uplata_date != null and $temp_Uplata_date != ""){
		         $temp_Uplata_date = date("Y-m-d", strtotime($list_avans['Uplata_date']));		    
			    } 
			    else {
			    	$temp_Uplata_date = ""	;
			    }
				$pdf->Cell($width_cell[6],5,$temp_Uplata_date,0,0,'C',false); // Fourth column of row 1

				if($list_avans['Zaduzenje_uplaceno'] != null and $list_avans['Zaduzenje_uplaceno'] != ""){
		         	$pdf->Cell($width_cell[7],5,$list_avans['Zaduzenje_uplaceno'],0,1,'C',false); // Fourth column of row 1	
		         	$uplaceno_sum = $uplaceno_sum + ($list_avans['Zaduzenje_uplaceno']);
			    } 
			    else {
			    	$pdf->Cell($width_cell[7],5,"0.00",0,1,'C',false); // Fourth column of row 1
			    }
				// $pdf->Cell($width_cell[6],5,$temp_Uplata_date,0,0,'C',false); // Fourth column of row 1

				// $pdf->Cell($width_cell[7],5,$temp_znak_prefix.$list2['Uplata_iznos'],0,1,'C',false); // Fourth column of row 1         
		    }
		    $pdf -> SetX(65);
		    $pdf->Cell(20,4,'Duguje: ',1,0,'C',true); // Second header column
			$pdf -> SetX(87);
			$pdf->Cell(15,4,$duguje_sum,0,0,'C',true); // Second header column
	        $pdf -> SetX(145);
			$pdf->Cell(20,4,'Ukupno uplaceno:',1,0,'C',true); // Second header column
			$pdf -> SetX(167);
			$pdf->Cell(15,4,$uplaceno_sum,0,0,'C',true); // Second header column
	        $pdf->Ln(4);
	        $pdf->Ln(4);	        		   
		}
	}
}



// $pdf -> Line(20, 10, $width-20, 10);
// $pdf->Ln(4);   
// $pdf -> SetX(20);
// $pdf->SetFillColor(193,229,252);
// $pdf->SetFont('Arial','',5);
// $pdf->Cell(50,5,'Korisnik:',1,0,'C',true); // First header column 
// $pdf -> SetX(100);
// $pdf->Cell(50,5,'Ukupno zaduzenje',1,0,'C',true); // Second header column
// $pdf -> SetX(160);
// $pdf->Cell(30,5,'Broj rata',1,0,'C',true); // Second header column

// $pdf->Ln(4);   
// $pdf -> SetX(20);
// $pdf->SetFillColor(193,229,252);
// $pdf->SetFont('Arial','',5);
// $pdf->Cell(50,5,'Jadranko Bodiroga:',1,0,'C',true); // First header column 
// $pdf -> SetX(100);
// $pdf->Cell(50,5,'100.99',1,0,'C',true); // Second header column
// $pdf -> SetX(160);
// $pdf->Cell(30,5,'60',1,0,'C',true); // Second header column
// $pdf->Ln(4);   

// $pdf->Ln(4);   
// $pdf -> SetX(20);
// $pdf->SetFillColor(193,229,252);
// $pdf -> SetX(100);
// $pdf->Cell(50,5,'Datum dospijeca:',1,0,'C',true); // Second header column
// $pdf -> SetX(160);
// $pdf->Cell(30,5,'2021-06-01',1,0,'C',true); // Second header column

// $pdf -> SetY(35);
// $pdf->SetFillColor(193,229,252);

// $pdf->SetFont('Arial','',5);
// $pdf -> SetX(20);
// $pdf->Cell($width_cell[0],10,'#',1,0,'C',true); // First header column 
// $pdf->Cell($width_cell[1],10,'Tip',1,0,'C',true); // First header column 
// $pdf->Cell($width_cell[2],10,'Rata broj',1,0,'C',true); // First header column 
// $pdf->Cell($width_cell[3],10,'Datum rate',1,0,'C',true); // Second header column
// $pdf->Cell($width_cell[4],10,'Iznos rate',1,0,'C',true); // Third header column 
// $pdf->Cell($width_cell[5],10,'Izvod',1,0,'C',true); // Third header column 
// $pdf->Cell($width_cell[6],10,'Datum_uplate',1,0,'C',true); // Fourth header column
// $pdf->Cell($width_cell[7],10,'Uplaceno',1,1,'C',true); // Fourth header column
// //// header is over ///////

// $pdf -> SetX(20);
// $pdf->SetFont('Arial','',5);
// // First row of data 
// $pdf->Cell($width_cell[0],10,'1',1,0,'C',false); // First column of row 1 
// $pdf->Cell($width_cell[1],10,'Uplata',1,0,'C',false); // First column of row 1 
// $pdf->Cell($width_cell[2],10,'Rata broj 3',1,0,'C',false); // First column of row 1 
// $pdf->Cell($width_cell[3],10,'2021-01-01',1,0,'C',false); // Second column of row 1 
// $pdf->Cell($width_cell[4],10,'50.00',1,0,'C',false); // Third column of row 1 
// $pdf->Cell($width_cell[5],10,'75',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[6],10,'2021-01-01',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[7],10,'50.00',1,1,'C',false); // Fourth column of row 1 
// //  First row of data is over 
// //  Second row of data 
// $pdf -> SetX(20);
// $pdf->Cell($width_cell[0],10,'2',1,0,'C',false); // First column of row 2 
// $pdf->Cell($width_cell[1],10,'Uplata',1,0,'C',false); // First column of row 2 
// $pdf->Cell($width_cell[2],10,'Rata broj 2',1,0,'C',false); // First column of row 2 
// $pdf->Cell($width_cell[3],10,'2021-01-01',1,0,'C',false); // Second column of row 1 
// $pdf->Cell($width_cell[4],10,'50.00',1,0,'C',false); // Third column of row 1 
// $pdf->Cell($width_cell[5],10,'75',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[6],10,'2021-01-01',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[7],10,'50.00',1,1,'C',false); // Fourth column of row 1 
// //   Sedond row is over 
// //  Third row of data
// $pdf -> SetX(20);
// $pdf->Cell($width_cell[0],10,'3',1,0,'C',false); // First column of row 3
// $pdf->Cell($width_cell[1],10,'Uplata',1,0,'C',false); // First column of row 3
// $pdf->Cell($width_cell[2],10,'Rata broj 1',1,0,'C',false); // First column of row 3
// $pdf->Cell($width_cell[3],10,'2021-01-01',1,0,'C',false); // Second column of row 1 
// $pdf->Cell($width_cell[4],10,'50.00',1,0,'C',false); // Third column of row 1 
// $pdf->Cell($width_cell[5],10,'75',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[6],10,'2021-01-01',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[7],10,'50.00',1,1,'C',false); // Fourth column of row 1 
   
// // $pdf -> Line(20, 80, $width-20, 80); 
// $pdf->Ln(4); //Break
//////////////////////////////////////////////Korisnik/////////////////////////////////////

// $pdf -> Line(20, 90, $width-20, 90);
// $pdf->Ln(4);   
// $pdf -> SetX(20);
// $pdf->SetFillColor(193,229,252);
// $pdf->SetFont('Arial','',5);
// $pdf->Cell(50,5,'Korisnik:',1,0,'C',true); // First header column 
// $pdf -> SetX(100);
// $pdf->Cell(50,5,'Ukupno zaduzenje',1,0,'C',true); // Second header column
// $pdf -> SetX(160);
// $pdf->Cell(30,5,'Broj rata',1,0,'C',true); // Second header column

// $pdf->Ln(4);   
// $pdf -> SetX(20);
// $pdf->SetFillColor(193,229,252);
// $pdf->SetFont('Arial','',5);
// $pdf->Cell(50,5,'Dubravko Bodiroga:',1,0,'C',true); // First header column 
// $pdf -> SetX(100);
// $pdf->Cell(50,5,'1110.99',1,0,'C',true); // Second header column
// $pdf -> SetX(160);
// $pdf->Cell(30,5,'6',1,0,'C',true); // Second header column
// $pdf->Ln(4);   

// $pdf->Ln(4);   
// $pdf -> SetX(20);
// $pdf->SetFillColor(193,229,252);
// $pdf -> SetX(100);
// $pdf->Cell(50,5,'Datum dospijeca:',1,0,'C',true); // Second header column
// $pdf -> SetX(160);
// $pdf->Cell(30,5,'2021-06-01',1,0,'C',true); // Second header column

// $pdf -> SetY(105);
// $pdf -> SetX(20);
// $pdf->SetFillColor(193,229,252);
// $pdf->SetFont('Arial','',5);
// $pdf->Cell($width_cell[0],10,'Broj',1,0,'C',true); // First header column 
// $pdf->Cell($width_cell[1],10,'Datum rate',1,0,'C',true); // Second header column
// $pdf->Cell($width_cell[2],10,'Iznos rate',1,0,'C',true); // Third header column 
// $pdf->Cell($width_cell[3],10,'Izvod',1,0,'C',true); // Third header column 
// $pdf->Cell($width_cell[4],10,'Datum_uplate',1,0,'C',true); // Fourth header column
// $pdf->Cell($width_cell[5],10,'Uplaceno',1,1,'C',true); // Fourth header column
// //// header is over ///////
// $pdf -> SetX(20);
// $pdf->SetFont('Arial','',5);
// // First row of data 
// $pdf->Cell($width_cell[0],10,'1',1,0,'C',false); // First column of row 1 
// $pdf->Cell($width_cell[1],10,'2021-01-01',1,0,'C',false); // Second column of row 1 
// $pdf->Cell($width_cell[2],10,'50.00',1,0,'C',false); // Third column of row 1 
// $pdf->Cell($width_cell[3],10,'75',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[4],10,'2021-01-01',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[5],10,'50.00',1,1,'C',false); // Fourth column of row 1 
// //  First row of data is over 
// //  Second row of data 
// $pdf -> SetX(20);
// $pdf->Cell($width_cell[0],10,'2',1,0,'C',false); // First column of row 2 
// $pdf->Cell($width_cell[1],10,'2021-01-01',1,0,'C',false); // Second column of row 1 
// $pdf->Cell($width_cell[2],10,'50.00',1,0,'C',false); // Third column of row 1 
// $pdf->Cell($width_cell[3],10,'75',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[4],10,'2021-01-01',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[5],10,'50.00',1,1,'C',false); // Fourth column of row 1 
// //   Sedond row is over 
// //  Third row of data
// $pdf -> SetX(20);
// $pdf->Cell($width_cell[0],10,'3',1,0,'C',false); // First column of row 3
// $pdf->Cell($width_cell[1],10,'2021-01-01',1,0,'C',false); // Second column of row 1 
// $pdf->Cell($width_cell[2],10,'50.00',1,0,'C',false); // Third column of row 1 
// $pdf->Cell($width_cell[3],10,'75',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[4],10,'2021-01-01',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[5],10,'50.00',1,1,'C',false); // Fourth column of row 1 
   
// $pdf -> Line(20, 150, $width-20, 150); 
// $pdf->Ln(4); //Break

/////////////////////////////////////////////////////////////////////////////////////

$pdf->Output();

?>