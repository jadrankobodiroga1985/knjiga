<?Php
/////////////////////////////////////////// Presjek stanja (ukupno zaduženje/ukupne uplate) //////////////////////////////////

// Opis:
// Izvještaj koji bi trebalo da sadrži isključivo podatak o: Ukupno zaduženje Ukupo napladeno Saldo, sa
// mogudnošdu presjeka stanja po određenim datumima, tj. Za određenu godinu ili opcija OD - DO, takođe
// sortiranje po odvojenom modulu kako bismo za svaki prihod imali pojedinačno izvještaj.

// http://localhost:3272/Knjiga/test/izvjestaj2.php?modul_id=1&datum_do=2021-08-01&datum_od=2020-01-01&customer_id=2
require('fpdf.php');
require_once('../private/classes.php');

$modul_id = isset($_GET['modul_id']) ? $_GET['modul_id'] : null;
$datum_od = isset($_GET['datum_od']) ? $_GET['datum_od'] : null;
$datum_do = isset($_GET['datum_do']) ? $_GET['datum_do'] : null;
$customer_id = isset($_GET['customer_id']) ? $_GET['customer_id'] : null;

$modul_id_text = " ";
if($modul_id != null){
	$modul_id_text = " and Modul_id=".$modul_id." ";
}

$customer_id_text = " ";
if($customer_id != null){
	$customer_id_text = " and Customer_id=".$customer_id." ";
}

$target_year = date("Y");

$datum_do_text = "";
if($datum_do != null && $datum_do !=""){
	$datum_do_text = " and '".$datum_do."' >= Ugovor_date ";
	$datum_do_year = DateTime::createFromFormat("Y-m-d", $datum_do);
	if( $datum_do_year->format("Y") != $target_year ) {
		$target_year = $datum_do_year->format("Y");
	}
}

$datum_od_text = "";
if($datum_od != null && $datum_od !=""){
	$datum_od_text = " and '".$datum_od."' <= Ugovor_date ";
	$datum_od_year = DateTime::createFromFormat("Y-m-d", $datum_od);
	if($datum_do != null && $datum_do !=""){
		$datum_do_year = DateTime::createFromFormat("Y-m-d", $datum_do);
		// var_dump($datum_od_year->format("Y"));
		// var_dump($datum_do_year->format("Y"));
		if( $datum_od_year->format("Y") != $datum_do_year->format("Y")) {
			$datum_od_text = " and '".$datum_do_year->format('Y')."-01-01' <= Ugovor_date ";
		}
	}
	else {
		if( $datum_od_year->format("Y") != $target_year ) {
			$target_year = $datum_od_year->format("Y");
		}
	}	
}

$pdf = new FPDF(); 
$pdf->AddPage();
$pdf->SetFont('Arial','B',12);
$width=$pdf->GetPageWidth(); // Width of Current Page
$height=$pdf->GetPageHeight(); // Height of Current Page
$width_cell=array(10,20,30,20,20,20,20,20);

$pdf->Image('logo.png',10,6,30);
// Arial bold 15
$pdf->SetFont('Arial','B',8);
// Move to the right
$pdf->Cell(50);
// Title
$modul_title_text = "za sve module ";
if($modul_id != null and $modul_id!=""){
	$modul = new atribut($modul_id);
	$modul_title_text = "samo za modul:".$modul->Name." ";
}

$customer_title_text = "za sve klijente ";
if($customer_id != null and $customer_id!=""){
	$cust = new customer($customer_id);
	$customer_title_text = "samo za klijenta:".$cust->Name." ";
}

$Datum_Od_title_text = " ";
if($datum_od != null and $datum_od!=""){	
	$Datum_Od_title_text = "Datum Od :".$datum_od." ";
}

$Datum_Do_title_text = " ";
if($datum_do != null and $datum_do!=""){	
	$Datum_Do_title_text = "Datum Do :".$datum_do." ";
}

$title ='Izvjestaj Presjek stanja (ukupno zaduzenje/ukupne uplate) '.$modul_title_text. $customer_title_text.$Datum_Od_title_text.$Datum_Do_title_text; 
// $title = iconv('UTF-8', 'CP1250//TRANSLIT', $title);
// $text = preg_replace("#[^a-zA-z]#", "", $text);
$pdf->MultiCell(100,5,$title);
// $pdf->Cell(100,10,'Izvještaj: Naziv Izvještaja',1,0,'C');
// Line break
$pdf->Ln(10);
$y = $pdf->GetY();
// $pdf -> Line(20, $y , $width-20, $y); 


$dbhost=Configuration::$dbInfo['dbhost'];
$dbuser=Configuration::$dbInfo['dbuser'];
$dbpass=Configuration::$dbInfo['dbpass'];
$dbname=Configuration::$dbInfo['dbname'];
$connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
$connection2=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
// $output="";  
mysqli_set_charset($connection,"utf8");
if (mysqli_connect_error($connection)){
throw new Exception("Problem sa konekcijom nad bazom: ".mysqli_connect_errno($connection).". Molimo kontaktirajte administratora portala.");  
}      
$sql = " select coalesce(sum(Zaduzenje_iznos),0) Zaduzenje ,coalesce(sum(Zaduzenje_uplaceno),0) Uplaceno, 0-(coalesce(sum(Zaduzenje_iznos),0) - coalesce(sum(Zaduzenje_uplaceno),0)) Saldo from bar.orders where Type in ('Zaduzenje','Pocetno_stanje','Avans','Pocetno_Avans')  and ".$target_year." = YEAR(Ugovor_date) ". $modul_id_text . $datum_do_text . $datum_od_text . $customer_id_text ;
$results = mysqli_query($connection, $sql);
if(mysqli_num_rows($results)) {

	$y = $pdf->GetY();
  	$pdf -> Line(20, $y , $width-20, $y);  
  	$pdf->Ln(1);
  	$y = $pdf->GetY();
  	$pdf -> Line(20, $y , $width-20, $y);  
  	$pdf->Ln(2);
	$pdf -> SetX(20);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetFont('Arial','B',10);		
	$pdf -> SetX(30);
	$pdf->Cell(50,10,'Ukupno zaduzenje',0,0,'C',true); // Second header column
	$pdf -> SetX(85);
	$pdf->Cell(50,10,'Ukupno potrazuje',0,0,'C',true); // Second header column
	$pdf -> SetX(140);
	$pdf->Cell(50,10,'Saldo',0,0,'C',true); // Second header column
	$pdf->Ln(4);

	$y = $pdf->GetY();
  	$pdf -> Line(20, $y+8 , $width-20, $y+8);  
  	$pdf->Ln(4);
  	// $i=0;
      while($list = mysqli_fetch_assoc($results)) {    
      	// if($list['Saldo'] > 0){
	      	// $i++;
	  //     	$pdf -> SetX(20);
	      	$pdf->Ln(10);
			$pdf->SetFillColor(255,255,255);
			$pdf->SetFont('Arial','B',10);
			$pdf -> SetX(30);
			$pdf->SetTextColor(255,0,0);
			$pdf->Cell(50,10,number_format($list['Zaduzenje'], 2, '.', ','),1,0,'C',true); // Second header column
			$pdf -> SetX(85);
			$pdf->SetTextColor(0,0,255);
			$pdf->Cell(50,10,number_format($list['Uplaceno'], 2, '.', ','),1,0,'C',true); // Second header column
			$pdf -> SetX(140);
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(50,10,number_format($list['Saldo'], 2, '.', ','),1,0,'C',true); // Second header column
			$pdf->Ln(4);

			// $pdf->Cell(10,3,$i.'.',0,0,'C',true); // First header column 
			// $pdf -> SetX(30);
			// $pdf->Cell(100,3,$list['Name'],0,0,'L',true); // First header column 
			// $pdf -> SetX(130);
			// $pdf->Cell(20,3,$list['Duguje'],0,0,'C',true); // Second header column
			// $pdf -> SetX(150);
			// $pdf->Cell(20,3,$list['Potrazuje'],0,0,'C',true); // Second header column
			// $pdf -> SetX(170);
			// $pdf->Cell(20,3,$list['Saldo'],0,0,'C',true); // Second header column	

			// $pdf->Ln(4);
			// $y = $pdf->GetY();
		 //  	$pdf -> Line(20, $y , $width-20, $y);  
		  	$pdf->Ln(2);
		  // }
    	}		
	}

/////////////////////////////////////////////////////////////////////////////////////

$pdf->Output();

?>