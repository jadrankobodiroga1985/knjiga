<?php 
  require_once("private/classes.php");
  // $modul = new atribut($_GET['modul_id']);
  // echo "id:".$modul->Id;
  // echo "Name:".$modul->Name;
  $year = date("Y");
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com -->
  <title>Pocetno stanje</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="live_css/bootstrap.min.css">
  <link href="live_css/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="live_css/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="live_css/jquery.min.js"></script>
  <script src="live_css/bootstrap.min.js"></script>
  <style>
  body {
    font: 400 15px Lato, sans-serif;
    line-height: 1.8;
    color: #818181;
  }
  h2 {
    font-size: 24px;
    text-transform: uppercase;
    color: #303030;
    font-weight: 600;
    margin-bottom: 30px;
  }
  h4 {
    font-size: 19px;
    line-height: 1.375em;
    color: #303030;
    font-weight: 400;
    margin-bottom: 30px;
  }  
  .jumbotron {
    background-color: #f4511e;
    color: #fff;
    padding: 100px 25px;
    font-family: Montserrat, sans-serif;
  }
  .container-fluid {
    padding: 60px 50px;
  }
  .bg-grey {
    background-color: #f6f6f6;
  }
  .logo-small {
    color: #f4511e;
    font-size: 50px;
  }
  .logo {
    color: #f4511e;
    font-size: 200px;
  }
  .thumbnail {
    padding: 0 0 15px 0;
    border: none;
    border-radius: 0;
  }
  .thumbnail img {
    width: 100%;
    height: 100%;
    margin-bottom: 10px;
  }
  .carousel-control.right, .carousel-control.left {
    background-image: none;
    color: #f4511e;
  }
  .carousel-indicators li {
    border-color: #f4511e;
  }
  .carousel-indicators li.active {
    background-color: #f4511e;
  }
  .item h4 {
    font-size: 19px;
    line-height: 1.375em;
    font-weight: 400;
    font-style: italic;
    margin: 70px 0;
  }
  .item span {
    font-style: normal;
  }
  .panel {
    border: 1px solid #f4511e; 
    border-radius:0 !important;
    transition: box-shadow 0.5s;
  }
  .panel:hover {
    box-shadow: 5px 0px 40px rgba(0,0,0, .2);
  }
  .panel-footer .btn:hover {
    border: 1px solid #f4511e;
    background-color: #fff !important;
    color: #f4511e;
  }
  .panel-heading {
    color: #fff !important;
    background-color: #f4511e !important;
    padding: 25px;
    border-bottom: 1px solid transparent;
    border-top-left-radius: 0px;
    border-top-right-radius: 0px;
    border-bottom-left-radius: 0px;
    border-bottom-right-radius: 0px;
  }
  .panel-footer {
    background-color: white !important;
  }
  .panel-footer h3 {
    font-size: 32px;
  }
  .panel-footer h4 {
    color: #aaa;
    font-size: 14px;
  }
  .panel-footer .btn {
    margin: 15px 0;
    background-color: #f4511e;
    color: #fff;
  }
  .navbar {
    margin-bottom: 0;
    background-color: #f4511e;
    z-index: 9999;
    border: 0;
    font-size: 12px !important;
    line-height: 1.42857143 !important;
    letter-spacing: 4px;
    border-radius: 0;
    font-family: Montserrat, sans-serif;
  }
  .navbar li a, .navbar .navbar-brand {
    color: #fff !important;
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
    color: #f4511e !important;
    background-color: #fff !important;
  }
  .navbar-default .navbar-toggle {
    border-color: transparent;
    color: #fff !important;
  }
  footer .glyphicon {
    font-size: 20px;
    margin-bottom: 20px;
    color: #f4511e;
  }
  .slideanim {visibility:hidden;}
  .slide {
    animation-name: slide;
    -webkit-animation-name: slide;
    animation-duration: 1s;
    -webkit-animation-duration: 1s;
    visibility: visible;
  }
  @keyframes slide {
    0% {
      opacity: 0;
      transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      transform: translateY(0%);
    }
  }
  @-webkit-keyframes slide {
    0% {
      opacity: 0;
      -webkit-transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      -webkit-transform: translateY(0%);
    }
  }
  @media screen and (max-width: 768px) {
    .col-sm-4 {
      text-align: center;
      margin: 25px 0;
    }
    .btn-lg {
      width: 100%;
      margin-bottom: 35px;
    }
  }
  @media screen and (max-width: 480px) {
    .logo {
      font-size: 150px;
    }
  }
  td ,th {
    text-align: center;
    font-weight: bold;
  }
  </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbar-default navbar-fixed-top" style="background-color: darkslategrey">
  <div class="container" style="width: 100%;margin-left: 0px;padding: 15px;">
    <div class="navbar-header" style="width:50%">
      <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button> -->
      
      <img src="logo.jpg" style="height:60px;float: left;">
     <!--  <div style="width:80%;float: left;margin-left: 10px;">
        <a class="navbar-brand" style="margin-left: 10px;float: none;width: 100%;font-size: 13px;">Modul: </a>
        <input id="modul_id" type="hidden" value="  " > 
      </div> -->
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="moduls.php">Moduli</a></li>
        
        <li><a href="logout.php">LogOut</a></li>
        <!-- <li><a href="#pricing">PRICING</a></li> -->
        <!-- <li><a href="#contact">CONTACT</a></li> -->
      </ul>
    </div>
  </div>
</nav>

<div class="jumbotron text-center" style="background-color: grey;height: 100px;">
  <div style="width:100%;height: 70px;">
      <!-- <h1 style="font-size: 40px;float: left;">Rokšped d.o.o.</h1>  -->
      <div style="float:left; width:50%; ">
        <!-- <h1 style="font-size: 40px;float: left;font-style: italic;">Rokšped d.o.o.</h1>  -->

        <div class="input-group" style="width:100%">  
          <div class="form-group" style="width:200px">
            <label for="sel1" style="float:left">Godina:</label>
            <select class="form-control" id="year" name="year" >
              <!-- <option>Tip</option> -->
              <!-- <option>Modul</option> -->
              <option value="2022" selected="selected">2022</option>
              <option value="2021" >2021</option>
              <!-- <option>Ugovor datum</option> -->
              <!-- <option>Status</option> -->
              <!-- <option>3</option> -->
              <!-- <option>4</option> -->
            </select>
          </div>

          <!-- <input id="filtar_value" type="email" class="form-control" size="50" placeholder="Unesite željenu riječ ..." required style="width: 30%;margin-left: 20px !important ;"> -->
          <div class="input-group-btn" style="float: left;">
            <button type="button" class="btn btn-danger" onclick="create_pocetno_stanje();">Kreiraj pocetno stanje</button>
          </div>
        </div>

      </div>
      <script type="text/javascript">
        
         function create_pocetno_stanje(){
          var year = document.getElementById('year').value;
          // alert("year:"+year);
            var xhttp; 
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {
                // alert("rezultat: "+this.responseText);  
                document.getElementById("pocetno_stanje_list").innerHTML = this.responseText;
                }          
            };             
            xhttp.open("GET", "create_pocetno_stanje.php?year="+year, true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send( );
          }


      </script>
  </div>

  <!-- <form> -->
    
  <!-- </form> -->

  <script type="text/javascript">
    
   function get_customer_orders_list(){
    // var my = this;
    // this.state.documents1 = ["Loading data ..."];
    // my.forceUpdate();
    var filtar_value = document.getElementById("filtar_value").value;
    var filtar = document.getElementById("filtar").value;
    alert("Poceo: "+filtar_value + "Filtar: "+filtar);
    var xhttp; 
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        alert("rezultat:"+this.responseText);       
        }          
    };
    xhttp.open("GET", "get_customer_orders_list.php?filtar_value=" + filtar_value +"&filtar=" + filtar, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send( );
  }

  </script>

  
</div>

<!-- Container (About Section) -->
<div id="about" class="container-fluid" style="padding-top: 0px;">
  <!-- <div class="row">
    <div class="col-sm-8">
      <h2>About Company Page</h2><br>
      <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</h4><br>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
      <br><button class="btn btn-default btn-lg">Get in Touch</button>
    </div>
    <div class="col-sm-4">
      <span class="glyphicon glyphicon-signal logo"></span>
    </div>
  </div> -->

  <table class="table table-bordered">
    <thead>
      <tr style="border: 3px solid black;">

        <th style="border-right: 2px solid black">Redni broj</th>
        <th>Klijent</th>
        <th>Tip</th>
        
        <th>Ugovor broj</th>

        <th style="border-left: 2px solid black;">Datum početka zaduženja</th>
        <th>Valuta zaduženja</th>
        <th>Iznos zaduženja</th>
        <th>Do sada uplaćeno</th>
        <th>Broj rata za uplatu</th>
        <th>Uplacen broj rata </th>
        <th>Iznos jedne rate</th>
        <th>Datum pocetka redovnih rata</th>
        <th>Iznos prve rata za uplatu</th>
        <th>Datum plaćanja prve rate</th>

   <!--      <th style="border-left: 2px solid black;">Uplata iznos</th>
        <th>Datum naloga uplate</th>
        <th>Datum rate</th>
        <th>Uplata banka</th> -->

        <th style="border-left: 2px solid black;border-right: 2px solid black">Status naloga</th>

      </tr>
    </thead>
    <tbody id="pocetno_stanje_list">
        <?php echo order::get_pocetna_stanja_list_html($year+1); ?>
    </tbody>
  </table>
  <script type="text/javascript">
    
    function redirect_to_update_page(order_id,modul_id){
      window.open('index5.php?order_id='+order_id+'&modul_id='+modul_id,'_blank');
    }

  </script>
 

</div>




</body>
</html>
