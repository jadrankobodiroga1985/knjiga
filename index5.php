<?php 
  require_once("private/classes.php");

  $order =  isset($_GET['order_id']) ? (new order($_GET['order_id'])) : null;
  $customer = isset($_GET['order_id']) ? (new customer($order->Customer_id)) : null;
  $modul_id = isset($_GET['modul_id']) ? $_GET['modul_id'] : null;
  if($order != null) {
    $modul_id = $order->Modul_id;
  }
  $modul = new atribut($modul_id);

  $year_selected = isset($_GET['year']) ? $_GET['year'] : null;
  $year = date("Y");
  if($year_selected != null){
    $year=$year_selected;    
  }  
  else if($order != null){
    if($order->Ugovor_date != null){
       $year =  date('Y', strtotime($order->Ugovor_date));
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com -->
  <title>Uplate </title>
  <!-- <link rel="stylesheet" href="live_css/w3.css"> -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="live_css/bootstrap.min.css">
  <link href="live_css/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="live_css/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="live_css/jquery.min.js"></script>
  <script src="live_css/bootstrap.min.js"></script>

  
  <style>
  body {
    font: 400 15px Lato, sans-serif;
    line-height: 1.8;
    color: #818181;
  }
  h2 {
    font-size: 24px;
    text-transform: uppercase;
    color: #303030;
    font-weight: 600;
    margin-bottom: 30px;
  }
  h4 {
    font-size: 19px;
    line-height: 1.375em;
    color: #303030;
    font-weight: 400;
    margin-bottom: 30px;
  }  
  .jumbotron {
    background-color: #f4511e;
    color: #fff;
    padding: 100px 25px;
    font-family: Montserrat, sans-serif;
  }
  .container-fluid {
    padding: 60px 50px;
  }
  .bg-grey {
    background-color: #f6f6f6;
  }
  .logo-small {
    color: #f4511e;
    font-size: 50px;
  }
  .logo {
    color: #f4511e;
    font-size: 200px;
  }
  .thumbnail {
    padding: 0 0 15px 0;
    border: none;
    border-radius: 0;
  }
  .thumbnail img {
    width: 100%;
    height: 100%;
    margin-bottom: 10px;
  }
  .carousel-control.right, .carousel-control.left {
    background-image: none;
    color: #f4511e;
  }
  .carousel-indicators li {
    border-color: #f4511e;
  }
  .carousel-indicators li.active {
    background-color: #f4511e;
  }
  .item h4 {
    font-size: 19px;
    line-height: 1.375em;
    font-weight: 400;
    font-style: italic;
    margin: 70px 0;
  }
  .item span {
    font-style: normal;
  }
  .panel {
    border: 1px solid #f4511e; 
    border-radius:0 !important;
    transition: box-shadow 0.5s;
  }
  .panel:hover {
    box-shadow: 5px 0px 40px rgba(0,0,0, .2);
  }
  .panel-footer .btn:hover {
    border: 1px solid #f4511e;
    background-color: #fff !important;
    color: #f4511e;
  }
  .panel-heading {
    color: #fff !important;
    background-color: #f4511e !important;
    padding: 25px;
    border-bottom: 1px solid transparent;
    border-top-left-radius: 0px;
    border-top-right-radius: 0px;
    border-bottom-left-radius: 0px;
    border-bottom-right-radius: 0px;
  }
  .panel-footer {
    background-color: white !important;
  }
  .panel-footer h3 {
    font-size: 32px;
  }
  .panel-footer h4 {
    color: #aaa;
    font-size: 14px;
  }
  .panel-footer .btn {
    margin: 15px 0;
    background-color: #f4511e;
    color: #fff;
  }
  .navbar {
    margin-bottom: 0;
    background-color: #f4511e;
    z-index: 9999;
    border: 0;
    font-size: 12px !important;
    line-height: 1.42857143 !important;
    letter-spacing: 4px;
    border-radius: 0;
    font-family: Montserrat, sans-serif;
  }
  .navbar li a, .navbar .navbar-brand {
    color: #fff !important;
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
    color: #f4511e !important;
    background-color: #fff !important;
  }
  .navbar-default .navbar-toggle {
    border-color: transparent;
    color: #fff !important;
  }
  footer .glyphicon {
    font-size: 20px;
    margin-bottom: 20px;
    color: #f4511e;
  }
  .slideanim {visibility:hidden;}
  .slide {
    animation-name: slide;
    -webkit-animation-name: slide;
    animation-duration: 1s;
    -webkit-animation-duration: 1s;
    visibility: visible;
  }
  @keyframes slide {
    0% {
      opacity: 0;
      transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      transform: translateY(0%);
    }
  }
  @-webkit-keyframes slide {
    0% {
      opacity: 0;
      -webkit-transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      -webkit-transform: translateY(0%);
    }
  }
  @media screen and (max-width: 768px) {
    .col-sm-4 {
      text-align: center;
      margin: 25px 0;
    }
    .btn-lg {
      width: 100%;
      margin-bottom: 35px;
    }
  }
  @media screen and (max-width: 480px) {
    .logo {
      font-size: 150px;
    }
  }
  td ,th ,tr {
    text-align: left;
    font-weight: bold;
    font-size: 15px;
    height: 12px;
    width: 50px;
  }
  td {
    padding: 0px;
  }

  #id02 #id01 {
    @include 'live_css/w3.css';
  }

/* Dropdown Button */
.dropbtn {
  background-color: #04AA6D;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
  cursor: pointer;
}

/* Dropdown button on hover & focus */
.dropbtn:hover, .dropbtn:focus {
  background-color: #3e8e41;
}

/* The search field */
#myInput {
  box-sizing: border-box;
  /*background-image: url('searchicon.png');*/
  background-position: 14px 12px;
  background-repeat: no-repeat;
  font-size: 16px;
  padding: 14px 20px 12px 45px;
  border: none;
  border-bottom: 1px solid #ddd;
}

/* The search field when it gets focus/clicked on */
#myInput:focus {outline: 3px solid #ddd;}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  position: relative;
  display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f6f6f6;
  min-width: 230px;
  border: 1px solid #ddd;
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #f1f1f1}

/* Show the dropdown menu (use JS to add this class to the .dropdown-content container when the user clicks on the dropdown button) */
.show {display:block;}

  </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbar-default navbar-fixed-top" style="background-color: darkslategrey">
  <div class="container" style="width: 100%;margin-left: 0px;padding: 15px;">
    <div class="navbar-header" style="width:50%">
      <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button> -->
      
      <img src="logo.jpg" style="height:60px;float: left;">
      <div style="width:80%;float: left;margin-left: 10px;">
        <a class="navbar-brand" style="margin-left: 10px;float: none;width: 100%;font-size: 13px;">Modul: <?php echo $modul->Name; ?> </a>
        <input id="modul_id" type="hidden" value="<?php if($modul != null) { echo $modul->Id; } ?>" > 
      </div>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="index.php?modul_id=<?php echo $modul->Id; ?>">Nazad</a></li>       
        <li><a href="reports.php">Izvjestaji</a></li>
        <li><a href="logout.php">LogOut</a></li>
        <!-- <li><a href="#pricing">PRICING</a></li> -->
        <!-- <li><a href="#contact">CONTACT</a></li> -->
      </ul>
    </div>
  </div>
</nav>

<div class="jumbotron text-center" style="background-color: grey;">
  <div style="width:100%;height: 160px;">

      <div style="float:left; width:70%; ">

        <div style="width:40%;float: left;">

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 12px;padding: 5px;">
                   <label style="margin-left:0px"><b>Vrsta naloga: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black; height: 100%;">
                   <input class="w3-input w3-border w3-margin-bottom" type="text" value="<?php if($order != null) {echo $order->Type;} ?>"  name="tip" style="width:100%" readonly>    
              </div>    
            </div>

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 12px;padding: 5px;">
                   <label style="margin-left:0px"><b>Broj ugovora: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black; height: 100%;">
                   <input class="w3-input w3-border w3-margin-bottom" type="text" value="<?php if($order != null) { echo $order->Ugovor_no;} ?>"  name="broj_ugovora" style="width:100%" readonly>    
              </div>    
            </div>

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 10px;padding: 5px;">
                   <label style="margin-left:0px"><b>Datum sklapanja ugovora: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black;">
                   <input class="w3-input w3-border w3-margin-bottom" type="text" value="<?php if($order != null) {echo date("Y-m-d", strtotime($order->Ugovor_date));} ?>"  name="datum_ugovora" style="width:100%" readonly>    
              </div>    
            </div>

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 10px;padding: 5px;">
                   <label style="margin-left:0px"><b>Datum kraja ugovora: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black;">
                   <input class="w3-input w3-border w3-margin-bottom" type="text" value="<?php if($order != null) { echo date("Y-m-d", strtotime($order->Ugovor_valuta_date)); } ?>"  name="datum_kraja_ugovora" style="width:100%" readonly>    
              </div>    
            </div>

             <div style="width:100%;border:0px solid black;float:left; ">
              <div style="width: 40%;float:left;font-size: 12px;padding: 5px;">
                   <label style="margin-left:0px"><b>Napomena </b></label>
              </div>
              <div style="width: 60%;float:left;color: black;">                  
                   <textarea style="width:100%" name="Napomena" id="Napomena" cols="40"  rows="3" value="<?php  if($order != null) { echo $order->Napomena;} ?>" readonly><?php if($order != null) { if($order->Napomena != null) {echo $order->Napomena; } }?></textarea>
              </div>    
            </div>

        </div>

        <div style="width:60%;float: left;">

          <div style="width:50%;float:left;">
            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 12px;padding: 5px;">
                   <label style="margin-left:0px"><b>Iznos zaduzenja: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black;">
                   <input class="w3-input w3-border w3-margin-bottom" type="text" value="<?php if($order != null) {echo $order->Zaduzenje_iznos;} ?>"  name="iznos_zaduzenje" style="width:100%" readonly>    
              </div>    
            </div>

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 10px;padding: 5px;">
                   <label style="margin-left:0px"><b>Broj rata za uplatu: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black;">
                   <input class="w3-input w3-border w3-margin-bottom" type="text" value="<?php if($order != null) { echo $order->Broj_rata;} ?>"  name="broj_rata" style="width:100%" readonly>    
              </div>    
            </div>

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 12px;padding: 5px;">
                   <label style="margin-left:0px"><b>Iznos jedne rate: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black;">
                   <input class="w3-input w3-border w3-margin-bottom" type="text" value="<?php if($order != null) {echo $order->Iznos_jedne_rate;} ?>"  name="iznos_jedne_rate" style="width:100%" readonly>    
              </div>    
            </div>

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 10px;padding: 5px;">
                   <label style="margin-left:0px"><b>Datum pocetka rata: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black;">
                   <input class="w3-input w3-border w3-margin-bottom" type="text" value="<?php if($order != null) { if($order->Pocetak_redovnih_rata != null) { echo date("Y-m-d", strtotime($order->Pocetak_redovnih_rata)); } } ?>"  name="datum_pocetka_rata" style="width:100%" readonly>    
              </div>    
            </div>

           <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 12px;padding: 5px;">
                   <label style="margin-left:0px"><b>Iznos prve rate: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black;">
                   <input class="w3-input w3-border w3-margin-bottom" type="text" value="<?php if($order != null) { echo $order->Prva_rata_iznos; }?>"  name="iznos_prve_rate" style="width:100%" readonly>    
              </div>    
            </div>

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 12px;padding: 5px;">
                   <label style="margin-left:0px"><b>Datum prve rate: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black;">
                   <input class="w3-input w3-border w3-margin-bottom" type="text" value="<?php if($order != null) { if($order->Prva_rata_valuta_date != null) { echo date("Y-m-d", strtotime($order->Prva_rata_valuta_date)); }}?>"  name="datum_prve_rate" style="width:100%" readonly>    
              </div>    
            </div>

            <div style="width:100%;border:2px solid black;float:left;margin-top: 0px;">              
              <div style="width:100%;">
                <button style="width:100%;background-color:blue" onclick="window.location.replace('./update_modul.php?year=<?php echo $year; ?>&modul_id=<?php if($order != null) { echo $order->Modul_id; } ?>&order_id=<?php if($order != null) { echo $order->Id; } ?>');"> Izmijeni podatke ovog zaduzenja</button>
              </div>  
            </div>

          </div>

           <div style="width:50%;float:left;">

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 12px;padding: 5px;">
                   <label style="margin-left:0px"><b>Uplaceno do sada: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black;">
                   <input class="w3-input w3-border w3-margin-bottom" type="text" value="<?php if($order != null) { echo $order->Zaduzenje_uplaceno; }?>"  name="uplaceno" style="width:100%" readonly >    
              </div>    
            </div>

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 12px;padding: 5px;">
                   <label style="margin-left:0px"><b>Uplacen broj rata: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black;">
                   <input class="w3-input w3-border w3-margin-bottom" type="text" value="<?php if($order != null) { echo $order->Broj_rata_uplaceno; } ?>"  name="broj_rata_uplacen" style="width:100%" readonly>    
              </div>    
            </div>

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 12px;padding: 5px;">
                   <label style="margin-left:0px"><b>Status: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black;">
                   <input class="w3-input w3-border w3-margin-bottom" type="text" value="<?php if($order != null) { echo $order->Status; } ?>"  name="broj_rata_uplacen" style="width:100%" readonly>    
              </div>    
            </div>

            <div style="width:100%;border:0px solid black;float:right;">
              <!-- <div style="width: 40%;float:left;font-size: 12px;padding: 5px;">
                   <label style="margin-left:0px"><b>Status: </b></label>
              </div> -->
              <div style="width: 80%;float:right;color: black;">
                   <button style="width: 100%;height: 50px;"  <?php if($order != null) { echo "onClick=\"window.open('./test/kartica.php?order_id=".$order->Id."','_blank');\""; } ?> >Stampaj</button>   
              </div>    
            </div>

          </div>

        </div>

      </div>

      <div style="width: 30%;float: right;">
          
              <div class="dropdown" style="width:100%">

                <button onclick="myFunction()" class="dropbtn" style="width: 70%">Pronadji ugovor</button>

                <select class="w3-input w3-border w3-margin-bottom"  style="width:20%;color:black;float: right;" id="yearSelect" onchange="selectYear()" >
                   <?php  echo order::get_years_of_orders($year,"ugovor"); ?>
                </select>

                <div id="myDropdown" class="dropdown-content" style = "width:100%" >
                  <input type="text" placeholder="..." id="myInput" onkeyup="filterFunction()" style="color:black;width: 100%;">
                  <?php  echo order::get_all_list_of_zaduzenja_from_modul($modul->Id,$year); ?>                
                </div>
              </div>

          <!-- </div> -->
          <script type="text/javascript">

             function selectYear() {
              var year = document.getElementById("yearSelect").value;
              var order_text="";
              <?php if($order!=null) { echo "order_text='&order_id=".$order->Id."';"; } ?>
              // alert(year);
              window.open('./index5.php?modul_id=<?php echo $modul->Id; ?>&year='+year,'"_self"');
            }

            /* When the user clicks on the button,
              toggle between hiding and showing the dropdown content */
              function myFunction() {
                document.getElementById("myDropdown").classList.toggle("show");
              }

              function filterFunction() {
                var input, filter, ul, li, a, i;
                input = document.getElementById("myInput");
                filter = input.value.toUpperCase();
                div = document.getElementById("myDropdown");
                a = div.getElementsByTagName("a");
                for (i = 0; i < a.length; i++) {
                  txtValue = a[i].textContent || a[i].innerText;
                  if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    a[i].style.display = "";
                  } else {
                    a[i].style.display = "none";
                  }
                }
              }
            
            function redirect_to_order(order_id){
              alert('klinio'+order_id);
              window.location.replace("./index5.php?order_id="+order_id);
            }

          </script>

          <div style="width:100%;">
            <h1 style="font-size: 40px;float: right;font-style: italic;margin-top: 0px;"><?php if($order != null) { echo $customer->Name; }?></h1> 
          </div>

          <p style="font-size: 12px;float: left;width: 100%;text-align: right;margin-bottom: 0px;">PIB/PDV_broj:<?php if($order != null) { echo $customer->PIB;} ?> &nbsp; <?php if($order != null) { echo $customer->PDV_broj;} ?></p></br>
          <p style="font-size: 12px;float: left;width: 100%;text-align: right;margin-bottom: 0px">JMBG: &nbsp;<?php if($order != null) { echo $customer->JMBG; } ?> &nbsp;</p></br>
          <p style="font-size: 12px;float: left;width: 100%;text-align: right;margin-bottom: 0px">Adresa:&nbsp;<?php if($order != null) { echo $customer->Address; } ?> &nbsp;</p> </br>
          <p style="font-size: 12px;float: left;width: 100%;text-align: right;margin-bottom: 0px">Email:&nbsp;<?php if($order != null) { echo $customer->Email; } ?>  &nbsp;</p> </br>
          <p style="font-size: 12px;float: left;width: 100%;text-align: right;margin-bottom: 0px">Tel:&nbsp;<?php if($order != null) { echo $customer->Telefon; } ?>  &nbsp;</p> </br>

        </div>
     
  </div>

  <!-- <form> -->
    <!-- <form> -->
    <div id="id01" class="w3-modal" style="display:none">
      <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:1000px">

        <div class="w3-center"><br>
          <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal" style="background-color:red">&times;</span>
          <!-- <img src="img_avatar4.png" alt="Avatar" style="width:30%" class="w3-circle w3-margin-top"> -->
        </div>

        
          <div class="input-group">

            <div class="dropdown" style="margin-left: 5%;">
              <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown" style="float: left;">Filtar
              <span class="caret"></span></button>
              <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                <li role="presentation"><a role="menuitem" tabindex="-1" onclick="filtar_set('PIB');">PIB</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" onclick="filtar_set('PDV_broj');">PDV broj</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" onclick="filtar_set('Name');">Naziv klijenta</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" onclick="filtar_set('JMBG');">JMBG</a></li>
                <!-- <li role="presentation" class="divider"></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ostalo...</a></li> -->
              </ul>
            </div>
            <script type="text/javascript">

              var filtar = "";
              function filtar_set(new_filtar){
                filtar = new_filtar;
              }

              function get_customers_filtared_list(){
                var filtar_value = document.getElementById('search_customer_input').value;
                var xhttp; 
                xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                  if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("customers_filtared_list").innerHTML = this.responseText;
                  }          
                };             
                xhttp.open("GET", "get_orders_configuration_AJAX.php?filtar="+filtar+"&filtar_value=" + filtar_value +"&process=customers", true);
                xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xhttp.send( );
              }

            </script>

            <input id="search_customer_input" type="email" class="form-control" size="50" placeholder="Unesite željenu riječ ..." required style="width: 50%;margin-left: 20xp;">
            <div class="input-group-btn" style="float: left;">
              <button type="button" class="btn btn-danger" onclick="get_customers_filtared_list();">Pretraži</button>
            </div>
          </div>
  <!-- </form> -->
        <div id="about" class="container-fluid" style="padding-top: 0px;">
        <table class="table table-bordered" style="color:black">
          <thead>
            <tr>
              <th>#</th>
              <th>Kompanija:</th>
              <th>Zaduzenje:</th>
              <th>Uplate:</th>
              <th>Saldo:</th>
              <th style="width:50px"></th>
              <!-- <th>Dugovanje u valuti:</th> -->
            </tr>
          </thead>
          <tbody id="customers_filtared_list">
            
          </tbody>
        </table>
      </div>

      <script type="text/javascript">

              var modul_id= '<?php  if($order != null) { echo $order->Modul_id; } ?>';
              var customer_id = '<?php if($order != null) { echo $order->Customer_id; } ?>';
              var zaduzenje_id = '<?php if($order != null) { echo $order->Id; } ?>';
              // alert(modul_id);
              
              function change_customer(cust_id,cust_name){
                customer_id = cust_id;
                alert($customer_id);
                document.getElementById('id01').style.display='none';
                document.getElementById('id02').style.display='block';
                get_zaduzenja_list(customer_id,modul_id);
              }

              function odaberi_zaduzenje(zad_id,ugovor){
                zaduzenje_id = zad_id;
                alert(zaduzenje_id);
                // document.getElementById('poziv_na_broj_value').value = ugovor;
                // document.getElementById('poziv_na_broj_id').value = zaduzenje_id;
                document.getElementById('id02').style.display='none'
              }

            function get_zaduzenja_list(customer_id,modul_id){
              var xhttp; 
              xhttp = new XMLHttpRequest();
              xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                  // alert("rezultat: "+this.responseText);  
                  document.getElementById("lista_zaduzenja").innerHTML = this.responseText;
                  }          
              };             
              xhttp.open("GET", "get_orders_configuration_AJAX.php?customer_id="+customer_id+"&modul_id=" + modul_id +"&process=zaduzenje", true);
              xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
              xhttp.send( );
            }

      </script>

      </div>
    </div>

    <div id="id02" class="w3-modal" style="display:none">
      <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="width:1200px">

        <div class="w3-center"><br>
          <span onclick="document.getElementById('id02').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal" style="background-color:red">&times;</span>
        </div>        
        <div id="about" class="container-fluid" style="padding-top: 20px;">
        <table class="table table-bordered" style="color:black">
          <thead>
            <tr>
              <th>#</th>
              <th>Broj ugovora:</th>
              <th>Rata:</th>
              <th>Modul:</th>
              <th>Zaduzenje:</th>
              <th>Saldo:</th>
              <th>Komentar:</th>
              <th>Status:</th>
              <th>Datum valute:</th>
              <th style="width:50px"></th>
              <!-- <th>Dugovanje u valuti:</th> -->
            </tr>
          </thead>
          <tbody id="lista_zaduzenja">
                                            
          </tbody>
        </table>        

      </div>
      </div>
    </div>
    
  <!-- </form> -->

  <script type="text/javascript">
    
   function get_customer_orders_list(){
    
    var filtar_value = document.getElementById("filtar_value").value;
    var filtar = document.getElementById("filtar").value;
    alert("Poceo: "+filtar_value + "Filtar: "+filtar);
    var xhttp; 
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        alert("rezultat:"+this.responseText);       
        }          
    };
    xhttp.open("GET", "get_customer_orders_list.php?filtar_value=" + filtar_value +"&filtar=" + filtar, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send( );
  }

  </script>

  
</div>

<!-- Container (About Section) -->
<div id="about" class="container-fluid" style="padding-top: 0px;">
 

  <table class="table table-bordered">
    <thead>
      <tr style="border: 3px solid black;">

        <th style="border-right: 2px solid black">#</th>
        <th>Tip naloga</th>
        <th>Br. rata</th>
        <th style="width:50px">Datum prispeća</th>        
        <th style="width:100px">Duguje</th>
        <th>Nalog Br.</th>
        <th>Izvod Br.</th>  
        <th>Budzet</th>
        
        <th>Datum uplate</th>
        <th>Potrazuje</th>
        <th style="width:250px">Napomena</th>
        <th></th>        
        <th></th>        
        <!-- <th>Brisanje</th>         -->

      </tr>
    </thead>
    <tbody>
      <?php  if($order != null) { echo $order->get_uplate_from_zaduzenje(); } ?>

<form action="./uplata.php" method="post">
      <tr>
        <input type="hidden"  name="process"  value="uplata_insert">
        <input type="hidden"  name="zaduzenje_id"  value="<?php if($order !=null) { echo $order->Id; } ?>">
        <td style="border-left: 2px solid black;border-right: 2px solid black"></td>
        <td>
          <select  name="vrsta_naloga">
            <!-- <option></option> -->
            <option value="Uplata">Uplata</option>
            <option value="Storno">Storno</option>
            <option value="Info">Zaduzenje</option>
          </select>   
        </td>
        <td></td>
        <td style="padding: 0px;">
          <input type="date" class="w3-input w3-border w3-margin-bottom"  style="height:100%" name="Uplata_rata_date" readonly> 
        </td>
        <td style="padding: 0px;">
          <input class="w3-input w3-border w3-margin-bottom" type="text"  style="width:100%;height:100%" name="Duguje">
        </td>
        <td style="padding: 0px;">
          <input class="w3-input w3-border w3-margin-bottom" type="text" style="width:100%;height:100%" name="Nalog">
        </td>
        <td style="padding: 0px;">
          <input class="w3-input w3-border w3-margin-bottom" type="text"  style="width:100%;height:100%" name="Izvod"> 
        </td>
        <td style="padding: 0px;">
          <input class="w3-input w3-border w3-margin-bottom" type="text"  style="width:100%;height:100%" name="Budzet">
        </td>
        <td style="padding: 0px;">
          <input type="date" class="w3-input w3-border w3-margin-bottom"  style="height:100%" name="datum_uplate">
        </td>
        <td style="padding: 0px;">
          <input class="w3-input w3-border w3-margin-bottom" type="text" value="0.00" style="width:100%;height:100%" name="Potrazuje">
        </td>
        <td style="padding: 0px;">
          <input class="w3-input w3-border w3-margin-bottom" type="text"  style="width:100%;height:100%" name="Napomena">
        </td>
        <td style="padding: 0px;">
          <input type="submit" class="btn btn-danger" value="Unesi">
        </td>
     </tr>
 </form>
      
    </tbody>
  </table>
  <div style="width:100%;border: 1px solid black;">

    <div style="width:100%;padding: 15px;border: 1px solid black;height: 70px;">
      <div style="width:30%;border:1px solid black;float: left; text-align: center;">
         <label><b>Zaduzenje:</b></label>
         <input style="color: blue;" class="w3-input w3-border w3-margin-bottom" type="text"  name="Zaduzenje" value="<?php if($order != null) { echo $order->Zaduzenje_iznos; } ?>">        
      </div>

      <div style="width:30%;border:1px solid black;float:left;margin-left: 5%; text-align: center;">
         <label><b>Uplate:</b></label>
        <input style="color: green;" class="w3-input w3-border w3-margin-bottom" type="text"  name="Uplate" value="<?php if($order != null) { echo $order->Zaduzenje_uplaceno; } ?>">        
      </div>

      <div style="width:30%;border:1px solid black;float:right; text-align: center;">
         <label><b>Saldo:</b></label>
         <input style="color: red;" class="w3-input w3-border w3-margin-bottom" type="text"  name="Saldo" value="<?php if($order != null) { echo ($order->Zaduzenje_uplaceno - $order->Zaduzenje_iznos); } ?>" >        
      </div>
    </div>

  </div>

</div>



</body>
</html>
