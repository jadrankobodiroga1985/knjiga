<?php 
  require_once('private/classes.php');
  $modul = new atribut($_GET['modul_id']);
  $customer = new customer($_GET['customer_id']);
  echo "customer_id:".$customer->Id."  modul_id".$modul->Id;
  // die();

  $message = isset($_SESSION['info']) ? $_SESSION['info'] : "";
  $username = isset($_GET['username']) ? $_GET['username'] : "test";
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com -->
  <title>Kreiranje uplate/zaduzenja</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="live_css/bootstrap.min.css">
  <link href="live_css/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="live_css/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="live_css/jquery.min.js"></script>
  <script src="live_css/bootstrap.min.js"></script>

  <link rel="stylesheet" href="live_css/w3.css">
<!-- <body> -->
  <style>
  body {
    font: 400 15px Lato, sans-serif;
    line-height: 1.8;
    color: #818181;
  }
  h2 {
    font-size: 24px;
    text-transform: uppercase;
    color: #303030;
    font-weight: 600;
    margin-bottom: 30px;
  }
  h4 {
    font-size: 19px;
    line-height: 1.375em;
    color: #303030;
    font-weight: 400;
    margin-bottom: 30px;
  }  
  .jumbotron {
    background-color: #f4511e;
    color: #fff;
    padding: 100px 25px;
    font-family: Montserrat, sans-serif;
  }
  .container-fluid {
    padding: 60px 50px;
  }
  .bg-grey {
    background-color: #f6f6f6;
  }
  .logo-small {
    color: #f4511e;
    font-size: 50px;
  }
  .logo {
    color: #f4511e;
    font-size: 200px;
  }
  .thumbnail {
    padding: 0 0 15px 0;
    border: none;
    border-radius: 0;
  }
  .thumbnail img {
    width: 100%;
    height: 100%;
    margin-bottom: 10px;
  }
  .carousel-control.right, .carousel-control.left {
    background-image: none;
    color: #f4511e;
  }
  .carousel-indicators li {
    border-color: #f4511e;
  }
  .carousel-indicators li.active {
    background-color: #f4511e;
  }
  .item h4 {
    font-size: 19px;
    line-height: 1.375em;
    font-weight: 400;
    font-style: italic;
    margin: 70px 0;
  }
  .item span {
    font-style: normal;
  }
  .panel {
    border: 1px solid #f4511e; 
    border-radius:0 !important;
    transition: box-shadow 0.5s;
  }
  .panel:hover {
    box-shadow: 5px 0px 40px rgba(0,0,0, .2);
  }
  .panel-footer .btn:hover {
    border: 1px solid #f4511e;
    background-color: #fff !important;
    color: #f4511e;
  }
  .panel-heading {
    color: #fff !important;
    background-color: #f4511e !important;
    padding: 25px;
    border-bottom: 1px solid transparent;
    border-top-left-radius: 0px;
    border-top-right-radius: 0px;
    border-bottom-left-radius: 0px;
    border-bottom-right-radius: 0px;
  }
  .panel-footer {
    background-color: white !important;
  }
  .panel-footer h3 {
    font-size: 32px;
  }
  .panel-footer h4 {
    color: #aaa;
    font-size: 14px;
  }
  .panel-footer .btn {
    margin: 15px 0;
    background-color: #f4511e;
    color: #fff;
  }
  .navbar {
    margin-bottom: 0;
    background-color: #f4511e;
    z-index: 9999;
    border: 0;
    font-size: 12px !important;
    line-height: 1.42857143 !important;
    letter-spacing: 4px;
    border-radius: 0;
    font-family: Montserrat, sans-serif;
  }
  .navbar li a, .navbar .navbar-brand {
    color: #fff !important;
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
    color: #f4511e !important;
    background-color: #fff !important;
  }
  .navbar-default .navbar-toggle {
    border-color: transparent;
    color: #fff !important;
  }
  footer .glyphicon {
    font-size: 20px;
    margin-bottom: 20px;
    color: #f4511e;
  }
  .slideanim {visibility:hidden;}
  .slide {
    animation-name: slide;
    -webkit-animation-name: slide;
    animation-duration: 1s;
    -webkit-animation-duration: 1s;
    visibility: visible;
  }
  @keyframes slide {
    0% {
      opacity: 0;
      transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      transform: translateY(0%);
    }
  }
  @-webkit-keyframes slide {
    0% {
      opacity: 0;
      -webkit-transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      -webkit-transform: translateY(0%);
    }
  }
  @media screen and (max-width: 768px) {
    .col-sm-4 {
      text-align: center;
      margin: 25px 0;
    }
    .btn-lg {
      width: 100%;
      margin-bottom: 35px;
    }
  }
  @media screen and (max-width: 480px) {
    .logo {
      font-size: 150px;
    }
  }
  </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbar-default navbar-fixed-top" style="background-color: darkslategrey">
  <div class="container" style="width: 100%;margin-left: 0px;padding: 15px;">
    <div class="navbar-header" style="width:50%">
      <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button> -->
      
      <img src="logo.jpg" style="height:60px;float: left;">
      <div style="width:80%;float: left;margin-left: 10px;">
        <a id="header_modul" class="navbar-brand" style="margin-left: 10px;float: none;width: 100%;font-size: 13px;">Modul: <?php echo $modul->Name; ?> </a>
      </div>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="index2.php?modul_id=<?php echo $modul->Id; ?>&customer_id=<?php echo $customer->Id;  ?>">Nazad</a></li>
        <li><a href="reports.php">Izvjestaji</a></li>
        <li><a href="logout.php">LogOut</a></li>
        <!-- <li><a href="#pricing">PRICING</a></li> -->
        <!-- <li><a href="#contact">CONTACT</a></li> -->
      </ul>
    </div>
  </div>
</nav>

<div class="jumbotron text-center" style="background-color: grey; padding-bottom: 0px;height: 220px;padding-top: 50px;">
  <div >
  <!-- <h1 style="font-size: 40px;">Rokšped d.o.o.</h1>  -->
      <div style="float:left; width:50%; ">
        <h1 id="customer_name" style="font-size: 40px;float: left;font-style: italic;"><?php echo $customer->Name; ?></h1> 
      </div>

      <div style="width: 50%;float: right;">
          <p style="font-size: 12px;float: left;width: 100%;text-align: right;margin-bottom: 0px;">PIB/PDV_broj:<?php echo $customer->PIB; ?> &nbsp; <?php echo $customer->PDV_broj; ?></p></br>
          <p style="font-size: 12px;float: left;width: 100%;text-align: right;margin-bottom: 0px">JMBG: &nbsp;<?php echo $customer->JMBG; ?> &nbsp;</p></br>
          <p style="font-size: 12px;float: left;width: 100%;text-align: right;margin-bottom: 0px">Adresa:&nbsp;<?php echo $customer->Address; ?> &nbsp;</p> </br>
          <p style="font-size: 12px;float: left;width: 100%;text-align: right;margin-bottom: 0px">Email:&nbsp;<?php echo $customer->Email; ?>  &nbsp;</p> </br>
          <p style="font-size: 12px;float: left;width: 100%;text-align: right;margin-bottom: 0px">Tel:&nbsp;<?php echo $customer->Telefon; ?>  &nbsp;</p> </br>
        </div>
     <!--  <button onclick="document.getElementById('id01').style.display='block'" class="w3-button w3-green w3-large" style="margin-right: 95%;">Promijeni klijenta</button>
 -->
       <div style="margin-top: 20px;width: 500px;" id="modul" >        
         <button onclick="document.getElementById('id01').style.display='block'" class="w3-button w3-green w3-large" style="float: left;">Promijeni klijenta</button>

        <!-- <input type="hidden"  size="50" placeholder=""  style="width: 50%;margin-left: 20xp;"  name="modul_id" id="modul_id" value="<?php echo $modul->Id; ?>"> -->
        <button  type="button" onclick="document.getElementById('id03').style.display='block'" class="w3-button w3-green w3-large" style="width: 200px;">Promijeni modul</button>
        <!-- <input type="racun"  size="50" placeholder="..."  style="margin-top: 10px;background: ghostwhite;"  id="modul_value" name="modul_value" readonly> -->
      </div>
<!-- <form> -->
    <div id="id01" class="w3-modal">
      <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:1000px">

        <div class="w3-center"><br>
          <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal" style="background-color:red">&times;</span>
          <!-- <img src="img_avatar4.png" alt="Avatar" style="width:30%" class="w3-circle w3-margin-top"> -->
        </div>

        
          <div class="input-group">

            <div class="dropdown" style="margin-left: 5%;">
              <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown" style="float: left;">Filtar
              <span class="caret"></span></button>
              <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                <li role="presentation"><a role="menuitem" tabindex="-1" onclick="filtar_set('PIB');">PIB</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" onclick="filtar_set('PDV_broj');">PDV broj</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" onclick="filtar_set('Name');">Naziv klijenta</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" onclick="filtar_set('JMBG');">JMBG</a></li>
                <!-- <li role="presentation" class="divider"></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ostalo...</a></li> -->
              </ul>
            </div>
            <script type="text/javascript">

              var filtar = "";
              function filtar_set(new_filtar){
                filtar = new_filtar;
                // alert("filtar"+filtar);
              }

              function get_customers_filtared_list(){
                var filtar_value = document.getElementById('search_customer_input').value;
                // alert("filtar_value"+filtar_value);
                var xhttp; 
                xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                  if (this.readyState == 4 && this.status == 200) {
                    // alert("rezultat: "+this.responseText);  
                    document.getElementById("customers_filtared_list").innerHTML = this.responseText;
                    }          
                };             
                xhttp.open("GET", "get_orders_configuration_AJAX.php?filtar="+filtar+"&filtar_value=" + filtar_value +"&process=customers", true);
                xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xhttp.send( );
              }

            </script>

            <input id="search_customer_input" type="email" class="form-control" size="50" placeholder="Unesite željenu riječ ..." required style="width: 50%;margin-left: 20xp;">
            <div class="input-group-btn" style="float: left;">
              <button type="button" class="btn btn-danger" onclick="get_customers_filtared_list();">Pretraži</button>
            </div>
          </div>
  <!-- </form> -->
        <div id="about" class="container-fluid" style="padding-top: 0px;">
        <table class="table table-bordered" style="color:black">
          <thead>
            <tr>
              <th>#</th>
              <th>Kompanija:</th>
              <th>Zaduzenje:</th>
              <th>Uplate:</th>
              <th>Saldo:</th>
              <th style="width:50px"></th>
              <!-- <th>Dugovanje u valuti:</th> -->
            </tr>
          </thead>
          <tbody id="customers_filtared_list">
            
          </tbody>
        </table>
      </div>

      <script type="text/javascript">
              
              function change_customer(customer_id,customer_name){
                modul_id = document.getElementById('modul_id').value;
                document.getElementById('customer_id').value = customer_id;
                document.getElementById('customer_name').innerHTML = customer_name;
                document.getElementById('id01').style.display='none';
                get_zaduzenja_list(customer_id,modul_id);
              }

      </script>

      </div>
    </div>

    <div id="id02" class="w3-modal">
      <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="width:1200px">

        <div class="w3-center"><br>
          <span onclick="document.getElementById('id02').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal" style="background-color:red">&times;</span>
          <!-- <img src="img_avatar4.png" alt="Avatar" style="width:30%" class="w3-circle w3-margin-top"> -->
        </div>        
        <div id="about" class="container-fluid" style="padding-top: 20px;">
        <table class="table table-bordered" style="color:black">
          <thead>
            <tr>
              <th>#</th>
              <th>Broj ugovora:</th>
              <th>Rata:</th>
              <th>Modul:</th>
              <th>Zaduzenje:</th>
              <th>Saldo:</th>
              <th>Komentar:</th>
              <th>Status:</th>
              <th>Datum valute:</th>
              <th style="width:50px"></th>
              <!-- <th>Dugovanje u valuti:</th> -->
            </tr>
          </thead>
          <tbody id="lista_zaduzenja">

            <?php echo order::get_list_of_customer_zaduzenja($customer->Id,$modul->Id); ?>                                           
          </tbody>
        </table>

        <script type="text/javascript">
            
            function odaberi_zaduzenje(zaduzenje_id,ugovor){
              document.getElementById('poziv_na_broj_value').value = ugovor;
              document.getElementById('poziv_na_broj_id').value = zaduzenje_id;
              document.getElementById('id02').style.display='none'
            }

            function get_zaduzenja_list(customer_id,modul_id){
              var xhttp; 
              xhttp = new XMLHttpRequest();
              xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                  // alert("rezultat: "+this.responseText);  
                  document.getElementById("lista_zaduzenja").innerHTML = this.responseText;
                  }          
              };             
              xhttp.open("GET", "get_orders_configuration_AJAX.php?customer_id="+customer_id+"&modul_id=" + modul_id +"&process=zaduzenje", true);
              xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
              xhttp.send( );
            }


        </script>

      </div>
      </div>
    </div>

     <div id="id03" class="w3-modal">
      <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:1000px">

        <div class="w3-center"><br>
          <span onclick="document.getElementById('id03').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal" style="background-color:red">&times;</span>
        </div>        
        <div id="about" class="container-fluid" style="padding-top: 0px;">
        <table class="table table-bordered" style="color:black">
          <thead>
            <tr>
              <th>#</th>
              <th>Modul:</th>  
              <th style="width:50px"></th>            
            </tr>
          </thead>
          <tbody id="promijeni_modul">

                <?php echo atribut::get_atributs_for_orders(); ?>
           
          <script type="text/javascript">
            
            function odaberi_modul(modul_id,modul_opis){
              cust_id = document.getElementById('customer_id').value;
              document.getElementById('modul_value').value = modul_opis;
              document.getElementById('modul_id').value = modul_id;
              document.getElementById('header_modul').innerHTML   ="Modul: "+ modul_opis;
              document.getElementById('id03').style.display='none';
              get_zaduzenja_list(cust_id,modul_id);
            }

          </script>
            
          </tbody>
        </table>
      </div>
      </div>
    </div>
<!-- </div> -->
      <!-- <p>We specialize in blablabla</p>  -->
  </div>
  
</div>

<!-- Container (About Section) -->
<div id="about" class="container-fluid" style="padding-top: 0px;padding-left: 25px">

  <form action="create_order.php" method="post">

    <input type="hidden"  size="50" placeholder=""  style="width: 50%;margin-left: 20xp;"  name="modul_id" id="modul_id" value="<?php echo $modul->Id; ?>">

    <input type="hidden"  size="50" placeholder="..."  style="margin-top: 10px;background: ghostwhite;"  id="modul_value" name="modul_value" value="<?php echo $modul->Name; ?>">

    <input type="hidden" class="form-control" id="customer_id" size="50" value="<?php echo $customer->Id; ?>"  name="customer_id"  >

    <input type="hidden" class="form-control" id="username" size="50" value="test"  name="username"  >

    <div class="input-group" style="width:100%">

      <script type="text/javascript">
var brojac =1;
       function setuj_formu() {
        var selectBox = document.getElementById("vrsta_naloga");
        var selectedValue = selectBox.options[selectBox.selectedIndex].value;
        // alert(selectedValue);
        if(brojac == 1){
          selectBox.remove("");
          brojac++;
        }
        if(selectedValue == "Uplata"){

          document.getElementById("bankovni_racun").style.display = 'block';
          document.getElementById("iznos_naloga").style.display = 'block';
          document.getElementById("Uplata_panel").style.display = 'block';
          document.getElementById("dateOfOrder").style.display = 'block';
          document.getElementById("poziv_na_broj").style.display = 'block';
          document.getElementById("submit").style.display = 'block';

          
          document.getElementById("zaduzenje_panel").style.display = 'none';
          document.getElementById("contract_no").style.display = 'none';
          document.getElementById("dateOfContract").style.display = 'none';
          document.getElementById("broj_rata").style.display = 'none';
          document.getElementById("iznos_rate").style.display = 'none';
          document.getElementById("iznos_prve_rate").style.display = 'none';
          document.getElementById("dateOfFirstInstalmentValute").style.display = 'none';          
          document.getElementById("dateOfInstalments").style.display = 'none';          
          document.getElementById("dateOfValute").style.display = 'none';
          // document.getElementById("modul").style.display = 'none';          
          // document.getElementById("komentar").style.display = 'none';
          document.getElementById("divRate").style.display = 'none';
          
        }
        else if(selectedValue == "Zaduženje") {         

          document.getElementById("zaduzenje_panel").style.display = 'block';
          document.getElementById("contract_no").style.display = 'block';
          document.getElementById("iznos_naloga").style.display = 'block';
          document.getElementById("dateOfOrder").style.display = 'none';          
          document.getElementById("broj_rata").style.display = 'block';
          document.getElementById("iznos_rate").style.display = 'block';
          document.getElementById("iznos_prve_rate").style.display = 'block';
          document.getElementById("dateOfFirstInstalmentValute").style.display = 'block';          
          document.getElementById("dateOfInstalments").style.display = 'block';          
          document.getElementById("dateOfValute").style.display = 'block';
          document.getElementById("dateOfContract").style.display = 'block';
          // document.getElementById("modul").style.display = 'block';    
          document.getElementById("submit").style.display = 'block';
          document.getElementById("divRate").style.display = 'block';
          
          document.getElementById("Uplata_panel").style.display = 'none';
          document.getElementById("poziv_na_broj").style.display = 'none';
          document.getElementById("bankovni_racun").style.display = 'none';      
          // document.getElementById("komentar").style.display = 'none';
          
        }
        
       }
      </script>
    <div style="width:100%">

        <div class="form-group" style="padding: 15px;border: 2px solid black;width: 300px;background: burlywood;float: left;">
          <label for="sel1">Odaberite vrstu naloga:</label>          
          <select  id="vrsta_naloga" name="vrsta_naloga" onchange="setuj_formu();">
            <option></option>
            <option>Uplata</option>
            <option>Zaduženje</option>
          </select>          
        </div>  

        <div style="width:200px;float:right;">
          <label for="sel1" style="float:right;color: red;font-weight: bold;"><?php echo $message;  ?> </label>
        </div>   
    </div>


    <div style="margin-top:20px;display:none;float:left;width: 100%;" id="iznos_naloga" >

      <div style="width:100%;height: 50px;">

        <div style="width:20%;float:left">
          <label for="contract_no" style="float:left">Iznos :</label>
          <input type="racun" class="form-control" size="50" placeholder="Unesite iznos ..."  style="width: 150px;margin-left:15px" name="iznos_naloga" id="iznos_naloga2" onchange="updejtuj_rate();" > </br>
          </br>
        </div>

        <div style="width:50%;float:left;">
          <label for="contract_no" style="float:left">Napomena :</label>
          <input type="racun" class="form-control" size="50" placeholder="Unesite napomenu ako zelite ..."  style="width: 80%;margin-left:15px" name="napomena"  > </br>
          </br>
        </div>

         <div style="width:20%;float:left;">
          <label for="contract_no" style="float:left">Nalog :</label>
          <input type="racun" class="form-control" size="50" placeholder="Unesite nalog ..."  style="width: 50%;margin-left:15px" name="nalog"  > </br>
          </br>
        </div>

      </div>

        <div id="zaduzenje_panel" style="border : 1px solid black;width: 100%;">

          <div style="border: 1px solid black; width: 40%;float: left;margin-top: 20px;padding: 15px;">

            <div class="form-group" style="height: 50px;display:none" id="contract_no" >
              <label for="contract_no" style="float:left">Broj ugovora:</label>
              <input type="racun" class="form-control" size="50" placeholder="Unesite broj ugovora..."  style="width: 300px;padding: 15px;margin-left: 10px;"  name="contract_no" >
            </div>   

            <div>
              <div id="dateOfContract"  style="display:none;">
                <label for="dateOfContract">Datum zaduženja:</label>
                <input type="date"  name="dateOfContract" style="margin-left:5px">
              </div>
              <div id="dateOfValute"  style="display:none;">
                <label for="dateOfValute">Valuta plaćanja:</label>
                <input type="date"  name="dateOfValute" style="margin-left:5px">
              </div>
            </div>
          </div> 

          <div style="width: 60%;margin-top: 20px;border: 1px solid black;padding: 15px;display:none;float: right;" id="divRate">

            <div style="width:50%; border:1px solid black;float: left;padding: 10px;">
                <div style="width:100%; display:none;height: 50px;" id="broj_rata">
                  <p style="width:120px;float:left;">Unesite broj rata:</p>
                  <input style="width:50px" type="racun" class="form-control" size="50" placeholder="Unesite broj rata" value="0"  style="margin-top: 10px"  name="broj_rata"  id="broj_rata2" onchange="updejtuj_rate();">
                </div>

                <div id="iznos_rate" style="display:none;width: 100%;height: 50px;">
                  <p style="width:120px;float: left;">Iznos jedne rate:</p>
                  <input type="racun" class="form-control" size="50" placeholder="Iznos rate..." value="0.00"  style="width: 100px;"  id="iznos_rate2" name="iznos_rate" >
                </div>

                <div id="dateOfInstalments"  style="display:none;">
                  <label for="dateOfValute">Datum pocetka rata:</label>
                  <input type="date"  name="dateOfInstalments">
                </div>
            </div>

            <div style="width:50%;float: right;border: 1px solid black;padding: 10px;">
              <div  id="iznos_prve_rate" style="display:none;width: 100%;height: 50px;margin-top: 10px;">
                <p style="width:120px;float: left;">Iznos prve rate:</p>
                <input type="racun" class="form-control" size="50" placeholder="Iznos prve rate..." value="0.00"  style="width: 100px;" id="iznos_prve_rate2" name="iznos_prve_rate" onchange="updejtuj_rate();">
              </div>
              <div id="dateOfFirstInstalmentValute"  style="display:none;">
                <label for="dateOfValute">Datum plaćanja prve rate:</label>
                <input type="date"  name="dateOfFirstInstalmentValute">
              </div>
            </div>

          </div>

        </div>
        <script type="text/javascript">
          
          function updejtuj_rate(){

            var temp_iznos = document.getElementById('iznos_naloga2').value;
            var temp_broj_rata = document.getElementById('broj_rata2').value;
            var temp_iznos_rate = document.getElementById('iznos_rate2').value;
            var temp_iznos_prve_rate = document.getElementById('iznos_prve_rate2').value;           

            // alert('Krenuo'+' temp_iznos:'+temp_iznos+' temp_broj_rata:'+temp_broj_rata+' temp_iznos_rate:'+temp_iznos_rate+' temp_iznos_prve_rate:'+temp_iznos_prve_rate);

            if(temp_broj_rata > 1 && temp_iznos > 0){
              if(temp_iznos_prve_rate > 0){
                temp_iznos_rate = (temp_iznos - temp_iznos_prve_rate)/(temp_broj_rata - 1 );
              }
              else {
                temp_iznos_rate = (temp_iznos)/(temp_broj_rata); 
              }
            }
            else {
              temp_iznos_rate = 0.00;
            }
            // document.getElementById('iznos_naloga2').value = temp_iznos.toFixed(2);
            document.getElementById('iznos_rate2').value = temp_iznos_rate.toFixed(2);
            // document.getElementById('iznos_prve_rate2').value = temp_iznos_prve_rate.toFixed(2);
            // document.getElementById('broj_rata2').value = temp_broj_rata.toFixed(0);
          }

        </script>

      <!-- </div> -->
      <div id="Uplata_panel" style="width:100%;border: 1px solid black;margin-bottom: 10px;" >

          <div style="width:40%;border:1px solid black;padding: 10px;float: left;">

            <div  id="bankovni_racun" style="display:none;width: 100%;height: 50px;margin-top: 10px;">
                <p style="width:100px;float: left;">Izvod:</p>
                <input type="racun" placeholder="Unesite bankarski izvod" class="form-control" size="50"   style="width: 200px;" name="bankovni_racun" >
            </div>

            <div id="dateOfOrder"  style="display:none;">
              <label for="dateOfOrder" style="margin-top: 20px">Datum naloga:</label>
              <input type="date"   name="dateOfOrder" style="margin-left:5px" >
            </div>            
          </div>

          <div style="display:none;margin-top: 15px;padding: 15px;width:60%;float:right;border: 1px solid black;" id="poziv_na_broj" >

            <button  type="button" onclick="document.getElementById('id02').style.display='block'" class="w3-button w3-green w3-large" >Odaberite ugovor ili zaduzenje za zatvaranje</button>
            <input type="racun"  size="50" placeholder="Broj ugovora ili zaduzenja..."  style="width: 400px;margin-left: 20xp;"  id="poziv_na_broj_value" readonly>
            <input type="racun"  size="50" placeholder=""  style="width: 100%;margin-left: 20xp;display: none;"  name="poziv_na_broj_id" id="poziv_na_broj_id">              
          </div>

          <!--  <div style="margin-top: 20px;display:none" >
            <input type="racun" class="form-control" size="50" placeholder="Unesite napomenu..."  style="width: 50%;margin-left: 20xp;"  name="napomena_uplata" id="napomena_uplata">
          </div> -->

     </div>

   </div>
      
      <div class="input-group-btn" style="float: left;display:none"  id="submit">
        <input  class="btn btn-danger" type="submit" value="Sacuvaj" style="width: 200px;height: 50px;">
        <!-- <button type="button" class="btn btn-danger" onclick="alert('pocinjem unos AJAX');">Unesi u sistem</button> -->
      </div>

    </div>

  </form>
  

</div>



</body>
</html>
