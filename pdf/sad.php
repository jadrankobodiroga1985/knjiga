<?Php
require('fpdf.php');
require_once('../private/classes.php');

$pdf = new FPDF(); 
$pdf->AddPage();
$pdf->SetFont('Arial','B',12);
$width=$pdf->GetPageWidth(); // Width of Current Page
$height=$pdf->GetPageHeight(); // Height of Current Page
$width_cell=array(10,20,30,20,20,20,20,20);


$dbhost=Configuration::$dbInfo['dbhost'];
$dbuser=Configuration::$dbInfo['dbuser'];
$dbpass=Configuration::$dbInfo['dbpass'];
$dbname=Configuration::$dbInfo['dbname'];
$connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
$connection2=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
// $output="";  
mysqli_set_charset($connection,"utf8");
if (mysqli_connect_error($connection)){
throw new Exception("Problem sa konekcijom nad bazom: ".mysqli_connect_errno($connection).". Molimo kontaktirajte administratora portala.");  
}      
$sql = " select * from orders  where  2021 = YEAR(Ugovor_date) and Type in ('Zaduzenje','Pocetno_stanje') and Modul_id=1 order by Id asc";
$results = mysqli_query($connection, $sql);
if(mysqli_num_rows($results)) {
	// $add_Y="50";
	// $num_of_Y = 0;
	$temp_height=0;
      while($list = mysqli_fetch_assoc($results)) {    

      	$zaduzenje_id= $list['Id'];
        $zaduzenje = new order($list['Id']);
        $customer = new customer($list['Customer_id']);

        if($temp_height >= $height){
    		$temp_height = 0;
    		// $pdf->AddPage();
    	}

      	// $pdf -> Line(20, 10 + $temp_height , $width-20, 10 + $temp_height);      	
      	// $temp_height = $temp_height + 10;
        if($temp_height >= $height){
    		$temp_height = 0;
    		// $pdf->AddPage();
    	}
		$pdf->Ln(4); 
		$pdf -> SetX(20);
		$pdf->SetFillColor(193,229,252);
		$pdf->SetFont('Arial','',5);
		$pdf->Cell(50,5,'Korisnik:',1,0,'C',true); // First header column 
		$pdf -> SetX(100);
		$pdf->Cell(50,5,'Ukupno zaduzenje',1,0,'C',true); // Second header column
		$pdf -> SetX(160);
		$pdf->Cell(30,5,'Broj rata',1,0,'C',true); // Second header column

		// $temp_height = $temp_height + 5;
        if($temp_height >= $height){
    		$temp_height = 0;
    		// $pdf->AddPage();
    	}
		$pdf->Ln(4);   

		$pdf -> SetX(20);
		$pdf->SetFillColor(193,229,252);
		$pdf->SetFont('Arial','',5);
		$pdf->Cell(50,5,$customer->Name,1,0,'C',true); // First header column 
		$pdf -> SetX(100);
		$pdf->Cell(50,5,$zaduzenje->Zaduzenje_iznos,1,0,'C',true); // Second header column
		$pdf -> SetX(160);
		$pdf->Cell(30,5,$zaduzenje->Broj_rata,1,0,'C',true); // Second header column

		// $temp_height = $temp_height + 5;
        if($temp_height >= $height){
    		$temp_height = 0;
    		// $pdf->AddPage();
    	}
		$pdf->Ln(4);   

		// $temp_height = $temp_height + 10;
  //       if($temp_height >= $height){
  //   		$temp_height = 0;
  //   		$pdf->AddPage();
  //   	}
		$pdf->Ln(4);   
		$pdf -> SetX(20);
		$pdf->SetFillColor(193,229,252);
		$pdf -> SetX(100);
		$pdf->Cell(50,5,'Datum dospijeca:',1,0,'C',true); // Second header column
		$pdf -> SetX(160);
		$pdf->Cell(30,5,$zaduzenje->Ugovor_date,1,0,'C',true); // Second header column

		// $temp_height = $temp_height + 5;
        if($temp_height >= $height){
    		$temp_height = 0;
    		// $pdf->AddPage();
    	}
    	$pdf->Ln(4);   
    	$pdf->Ln(4);   
		$sql2 = " select * from orders where Uplata_poziv_na_broj=".$list['Id']." and Type in ('Uplata','Storno') and Status!='Deleted' ";
        $results2 = mysqli_query($connection2, $sql2);
        if(mysqli_num_rows($results2)) {        	
        	$temp_height= $temp_height + 25;
        	if($temp_height >= $height){
        		$temp_height = 0;
        		$pdf->AddPage();
        	}
    		// $pdf -> SetY($temp_height);
			$pdf->SetFillColor(193,229,252);
			$pdf->SetFont('Arial','',5);
			$pdf -> SetX(20);
			$pdf->Cell($width_cell[0],10,'#',1,0,'C',true); // First header column 
			$pdf->Cell($width_cell[1],10,'Tip',1,0,'C',true); // First header column 
			$pdf->Cell($width_cell[2],10,'Rata broj',1,0,'C',true); // First header column 
			$pdf->Cell($width_cell[3],10,'Datum rate',1,0,'C',true); // Second header column
			$pdf->Cell($width_cell[4],10,'Iznos rate',1,0,'C',true); // Third header column 
			$pdf->Cell($width_cell[5],10,'Izvod',1,0,'C',true); // Third header column 
			$pdf->Cell($width_cell[6],10,'Datum_uplate',1,0,'C',true); // Fourth header column
			$pdf->Cell($width_cell[7],10,'Uplaceno',1,1,'C',true); // Fourth header column
			$temp_height= $temp_height + 10;
        	if($temp_height >= $height){
        		$temp_height = 0;
        		// $pdf->AddPage();
        	}

			$i=0; 
			while($list2 = mysqli_fetch_assoc($results2)) {
	          	$i++;
	          	$temp_height=$temp_height+10;
	          	if($temp_height >= $height){
	        		$temp_height = 0;
	        		// $pdf->AddPage();
	        	}
	            $pdf -> SetX(20);
				$pdf->SetFont('Arial','',5);
				// Rows of data 
				$pdf->Cell($width_cell[0],10,$i,1,0,'C',false); // First column of row 1 
				$pdf->Cell($width_cell[1],10,$list2['Type'],1,0,'C',false); // First column of row 1 
				$pdf->Cell($width_cell[2],10,$list2['Komentar'],1,0,'C',false); // First column of row 1 
				$pdf->Cell($width_cell[3],10,$list2['Uplata_rata_date'],1,0,'C',false); // Second column of row 1 
				$pdf->Cell($width_cell[4],10,$zaduzenje->Iznos_jedne_rate,1,0,'C',false); // Third column of row 1 
				$pdf->Cell($width_cell[5],10,$list2['Bank_account'],1,0,'C',false); // Fourth column of row 1 
				$pdf->Cell($width_cell[6],10,$list2['Uplata_date'],1,0,'C',false); // Fourth column of row 1 
				$pdf->Cell($width_cell[7],10,$list2['Uplata_iznos'],1,1,'C',false); // Fourth column of row 1
	        }	         
        }
        $temp_height = $temp_height + 45;
     //    if($temp_height >= $height){
    	// 	$temp_height = 0;
    	// 	$pdf->AddPage();
    	// }
        $pdf->Ln(4);
        // $num_of_Y++;
        // $pdf->AddPage();
      }
}




// $pdf -> Line(20, 10, $width-20, 10);
// $pdf->Ln(4);   
// $pdf -> SetX(20);
// $pdf->SetFillColor(193,229,252);
// $pdf->SetFont('Arial','',5);
// $pdf->Cell(50,5,'Korisnik:',1,0,'C',true); // First header column 
// $pdf -> SetX(100);
// $pdf->Cell(50,5,'Ukupno zaduzenje',1,0,'C',true); // Second header column
// $pdf -> SetX(160);
// $pdf->Cell(30,5,'Broj rata',1,0,'C',true); // Second header column

// $pdf->Ln(4);   
// $pdf -> SetX(20);
// $pdf->SetFillColor(193,229,252);
// $pdf->SetFont('Arial','',5);
// $pdf->Cell(50,5,'Jadranko Bodiroga:',1,0,'C',true); // First header column 
// $pdf -> SetX(100);
// $pdf->Cell(50,5,'100.99',1,0,'C',true); // Second header column
// $pdf -> SetX(160);
// $pdf->Cell(30,5,'60',1,0,'C',true); // Second header column
// $pdf->Ln(4);   

// $pdf->Ln(4);   
// $pdf -> SetX(20);
// $pdf->SetFillColor(193,229,252);
// $pdf -> SetX(100);
// $pdf->Cell(50,5,'Datum dospijeca:',1,0,'C',true); // Second header column
// $pdf -> SetX(160);
// $pdf->Cell(30,5,'2021-06-01',1,0,'C',true); // Second header column

// $pdf -> SetY(35);
// $pdf->SetFillColor(193,229,252);

// $pdf->SetFont('Arial','',5);
// $pdf -> SetX(20);
// $pdf->Cell($width_cell[0],10,'#',1,0,'C',true); // First header column 
// $pdf->Cell($width_cell[1],10,'Tip',1,0,'C',true); // First header column 
// $pdf->Cell($width_cell[2],10,'Rata broj',1,0,'C',true); // First header column 
// $pdf->Cell($width_cell[3],10,'Datum rate',1,0,'C',true); // Second header column
// $pdf->Cell($width_cell[4],10,'Iznos rate',1,0,'C',true); // Third header column 
// $pdf->Cell($width_cell[5],10,'Izvod',1,0,'C',true); // Third header column 
// $pdf->Cell($width_cell[6],10,'Datum_uplate',1,0,'C',true); // Fourth header column
// $pdf->Cell($width_cell[7],10,'Uplaceno',1,1,'C',true); // Fourth header column
// //// header is over ///////

// $pdf -> SetX(20);
// $pdf->SetFont('Arial','',5);
// // First row of data 
// $pdf->Cell($width_cell[0],10,'1',1,0,'C',false); // First column of row 1 
// $pdf->Cell($width_cell[1],10,'Uplata',1,0,'C',false); // First column of row 1 
// $pdf->Cell($width_cell[2],10,'Rata broj 3',1,0,'C',false); // First column of row 1 
// $pdf->Cell($width_cell[3],10,'2021-01-01',1,0,'C',false); // Second column of row 1 
// $pdf->Cell($width_cell[4],10,'50.00',1,0,'C',false); // Third column of row 1 
// $pdf->Cell($width_cell[5],10,'75',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[6],10,'2021-01-01',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[7],10,'50.00',1,1,'C',false); // Fourth column of row 1 
// //  First row of data is over 
// //  Second row of data 
// $pdf -> SetX(20);
// $pdf->Cell($width_cell[0],10,'2',1,0,'C',false); // First column of row 2 
// $pdf->Cell($width_cell[1],10,'Uplata',1,0,'C',false); // First column of row 2 
// $pdf->Cell($width_cell[2],10,'Rata broj 2',1,0,'C',false); // First column of row 2 
// $pdf->Cell($width_cell[3],10,'2021-01-01',1,0,'C',false); // Second column of row 1 
// $pdf->Cell($width_cell[4],10,'50.00',1,0,'C',false); // Third column of row 1 
// $pdf->Cell($width_cell[5],10,'75',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[6],10,'2021-01-01',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[7],10,'50.00',1,1,'C',false); // Fourth column of row 1 
// //   Sedond row is over 
// //  Third row of data
// $pdf -> SetX(20);
// $pdf->Cell($width_cell[0],10,'3',1,0,'C',false); // First column of row 3
// $pdf->Cell($width_cell[1],10,'Uplata',1,0,'C',false); // First column of row 3
// $pdf->Cell($width_cell[2],10,'Rata broj 1',1,0,'C',false); // First column of row 3
// $pdf->Cell($width_cell[3],10,'2021-01-01',1,0,'C',false); // Second column of row 1 
// $pdf->Cell($width_cell[4],10,'50.00',1,0,'C',false); // Third column of row 1 
// $pdf->Cell($width_cell[5],10,'75',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[6],10,'2021-01-01',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[7],10,'50.00',1,1,'C',false); // Fourth column of row 1 
   
// // $pdf -> Line(20, 80, $width-20, 80); 
// $pdf->Ln(4); //Break
//////////////////////////////////////////////Korisnik/////////////////////////////////////

// $pdf -> Line(20, 90, $width-20, 90);
// $pdf->Ln(4);   
// $pdf -> SetX(20);
// $pdf->SetFillColor(193,229,252);
// $pdf->SetFont('Arial','',5);
// $pdf->Cell(50,5,'Korisnik:',1,0,'C',true); // First header column 
// $pdf -> SetX(100);
// $pdf->Cell(50,5,'Ukupno zaduzenje',1,0,'C',true); // Second header column
// $pdf -> SetX(160);
// $pdf->Cell(30,5,'Broj rata',1,0,'C',true); // Second header column

// $pdf->Ln(4);   
// $pdf -> SetX(20);
// $pdf->SetFillColor(193,229,252);
// $pdf->SetFont('Arial','',5);
// $pdf->Cell(50,5,'Dubravko Bodiroga:',1,0,'C',true); // First header column 
// $pdf -> SetX(100);
// $pdf->Cell(50,5,'1110.99',1,0,'C',true); // Second header column
// $pdf -> SetX(160);
// $pdf->Cell(30,5,'6',1,0,'C',true); // Second header column
// $pdf->Ln(4);   

// $pdf->Ln(4);   
// $pdf -> SetX(20);
// $pdf->SetFillColor(193,229,252);
// $pdf -> SetX(100);
// $pdf->Cell(50,5,'Datum dospijeca:',1,0,'C',true); // Second header column
// $pdf -> SetX(160);
// $pdf->Cell(30,5,'2021-06-01',1,0,'C',true); // Second header column

// $pdf -> SetY(105);
// $pdf -> SetX(20);
// $pdf->SetFillColor(193,229,252);
// $pdf->SetFont('Arial','',5);
// $pdf->Cell($width_cell[0],10,'Broj',1,0,'C',true); // First header column 
// $pdf->Cell($width_cell[1],10,'Datum rate',1,0,'C',true); // Second header column
// $pdf->Cell($width_cell[2],10,'Iznos rate',1,0,'C',true); // Third header column 
// $pdf->Cell($width_cell[3],10,'Izvod',1,0,'C',true); // Third header column 
// $pdf->Cell($width_cell[4],10,'Datum_uplate',1,0,'C',true); // Fourth header column
// $pdf->Cell($width_cell[5],10,'Uplaceno',1,1,'C',true); // Fourth header column
// //// header is over ///////
// $pdf -> SetX(20);
// $pdf->SetFont('Arial','',5);
// // First row of data 
// $pdf->Cell($width_cell[0],10,'1',1,0,'C',false); // First column of row 1 
// $pdf->Cell($width_cell[1],10,'2021-01-01',1,0,'C',false); // Second column of row 1 
// $pdf->Cell($width_cell[2],10,'50.00',1,0,'C',false); // Third column of row 1 
// $pdf->Cell($width_cell[3],10,'75',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[4],10,'2021-01-01',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[5],10,'50.00',1,1,'C',false); // Fourth column of row 1 
// //  First row of data is over 
// //  Second row of data 
// $pdf -> SetX(20);
// $pdf->Cell($width_cell[0],10,'2',1,0,'C',false); // First column of row 2 
// $pdf->Cell($width_cell[1],10,'2021-01-01',1,0,'C',false); // Second column of row 1 
// $pdf->Cell($width_cell[2],10,'50.00',1,0,'C',false); // Third column of row 1 
// $pdf->Cell($width_cell[3],10,'75',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[4],10,'2021-01-01',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[5],10,'50.00',1,1,'C',false); // Fourth column of row 1 
// //   Sedond row is over 
// //  Third row of data
// $pdf -> SetX(20);
// $pdf->Cell($width_cell[0],10,'3',1,0,'C',false); // First column of row 3
// $pdf->Cell($width_cell[1],10,'2021-01-01',1,0,'C',false); // Second column of row 1 
// $pdf->Cell($width_cell[2],10,'50.00',1,0,'C',false); // Third column of row 1 
// $pdf->Cell($width_cell[3],10,'75',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[4],10,'2021-01-01',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[5],10,'50.00',1,1,'C',false); // Fourth column of row 1 
   
// $pdf -> Line(20, 150, $width-20, 150); 
// $pdf->Ln(4); //Break

/////////////////////////////////////////////////////////////////////////////////////

$pdf->Output();

?>