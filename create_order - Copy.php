<?php 
require_once('private/classes.php');
var_dump($_POST);
// die();
// var_dump(new order(1));
/////////////////////////////////////////////////////////////////////////////////

$username =  $_POST['username'] != '' ? $_POST['username'] : ""; ///////// Vrsta naloga 

$vrsta_naloga =  $_POST['vrsta_naloga'] != '' ? $_POST['vrsta_naloga'] : ""; ///////// Vrsta naloga Zaduženje/Uplata
$customer_id =  $_POST['customer_id'] ? $_POST['customer_id'] : ""; //////////// klijentov ID
$iznos_naloga =  $_POST['iznos_naloga'] ? str_replace(",",".",$_POST['iznos_naloga']) : "";////////// Iznos Zaduženja/Uplate
$napomena =  $_POST['napomena'] ? str_replace("'","",$_POST['napomena']) : "";////////// napomena
$nalog =  $_POST['nalog'] ? str_replace("'","",$_POST['nalog']) : "";////////// napomena

/////////////// ZaduŽenje ////////////////////////////////////////////////////

$contract_no =  $_POST['contract_no'] ? str_replace("'","",$_POST['contract_no']) : "";///////////// Broj potpisanog ugovora sa klijentom
$dateOfContract =  $_POST['dateOfContract'] ? $_POST['dateOfContract'] : "";//// Datum pocetka zaduzenja
$dateOfValute =  $_POST['dateOfValute'] ? $_POST['dateOfValute'] : "";////////// Datum kraja zaduzenja
$broj_rata =  $_POST['broj_rata'] ? str_replace(",","",$_POST['broj_rata']) : "";/////////////////// Opciono: Broj rata na koje se placa iznos
$iznos_rate =  $_POST['iznos_rate'] ? str_replace(",","",$_POST['iznos_rate']) : "";//////////////// Opciono: Iznos jedne redovne rate
$datum_pocetka_rata =  $_POST['dateOfInstalments'] ? $_POST['dateOfInstalments'] : "";//////////////// Opciono: Datum pocetka rata
$iznos_prve_rate =  $_POST['iznos_prve_rate'] ? str_replace(",","",$_POST['iznos_prve_rate']) : "";//Opciono: Iznos prve rate,ako se razlikuje od redovne 
$dateOfFirstInstalmentValute =  $_POST['dateOfFirstInstalmentValute'] ? $_POST['dateOfFirstInstalmentValute'] : ""; // Opciono: Datum placanja prve rate ako se razlikuje od redovne mjesecne rate
$modul_id =  $_POST['modul_id'] ? $_POST['modul_id'] : "";/////////////////////// ID modula 
$modul_value =  $_POST['modul_value'] ? str_replace("'","",$_POST['modul_value']) : "";////////////// Opis modula

//////////////// Uplate /////////////////////////////////////////////////////

$bankovni_racun =  $_POST['bankovni_racun'] ? str_replace("'","",$_POST['bankovni_racun']) : "";///// Opis bankovne transakcije kod uplate
$dateOfOrder =  $_POST['dateOfOrder'] ? $_POST['dateOfOrder'] : "";////////////// Datum uplate
$poziv_na_broj_id =  $_POST['poziv_na_broj_id'] ? $_POST['poziv_na_broj_id'] : "";// ID zaduzenja koje se zatvara uplatom



if($dateOfContract == ""){ $dateOfContract = "null";}
if($dateOfValute == ""){ $dateOfValute = "null";}
if($datum_pocetka_rata == ""){ $datum_pocetka_rata = "null";}
// if($datum_pocetka_rata == ""){ $datum_pocetka_rata = "null";}
if($dateOfFirstInstalmentValute == ""){ $dateOfFirstInstalmentValute = "null";}
if($dateOfOrder == ""){ $dateOfOrder = "null";}

////////////////////////////////////////////////////////////////////////////
$insert_id_temp = null;
if($vrsta_naloga == "Zaduženje"){ 
    $vrsta_naloga = "Zaduzenje";
    if($broj_rata > 0){
      $dateOfValute = izracunaj_datum_zadnje_rate($broj_rata,$datum_pocetka_rata,$dateOfFirstInstalmentValute);
    }
    $inserted_id = kreiraj_zaduzenje_klijenta($customer_id,$vrsta_naloga,$iznos_naloga,$contract_no,$dateOfContract,$dateOfValute,$broj_rata,$iznos_rate,$datum_pocetka_rata,$iznos_prve_rate,$dateOfFirstInstalmentValute,$modul_id,$modul_value,$napomena,$nalog);
    $insert_id_temp = $inserted_id; 
    $updated_id = updejtuj_customera($customer_id,$inserted_id);  
}
else if($vrsta_naloga == "Uplata"){    
    $inserted_id = kreiraj_uplatu_klijenta($customer_id,$bankovni_racun,$vrsta_naloga,$iznos_naloga,$poziv_na_broj_id,$dateOfOrder,$napomena,$nalog);
    $order_temp = new order($inserted_id);    
    $insert_id_temp = $order_temp->Uplata_poziv_na_broj;
    $updated_id = updejtuj_customera($customer_id,$inserted_id); 
}
header("Location: ./index3.php?modul_id=".$modul_id."&customer_id=".$customer_id."&username=".$username);
$_SESSION['info'] = "Insertovano uspjesno u sistem: <a href=\"index5.php?order_id=".$insert_id_temp."\" target=\"_blank\">Pogledaj prethodno insertovano</a>
";

function kreiraj_uplatu_klijenta($customer_id,$bankovni_racun,$vrsta_naloga,$iznos_naloga,$poziv_na_broj_id,$dateOfOrder,$napomena,$nalog){

  $zaduzenje = new order($poziv_na_broj_id);
  $komentar = "Uplacen cijelokupni iznos";
  $contract_no = $zaduzenje->Ugovor_no.";";
  $dateUplataRata = $zaduzenje->Ugovor_valuta_date;

  /////////////////////////////////// komentar //////////////////////////////////////////////

  if ($zaduzenje->Broj_rata > 0){
    $broj_uplata = $zaduzenje->Broj_rata_uplaceno+1;//broj_uplata($poziv_na_broj_id) + 1;// vraca broj uplata/rata koji nije Delete i referencirani na poziv na broj
    $komentar = "Uplata Rata ".$broj_uplata;
    $dateUplataRata = add_months_to_current_date($broj_uplata-1,$zaduzenje->Pocetak_redovnih_rata);
    if($broj_uplata == 1){
      if($zaduzenje->Prva_rata_valuta_date != null) {
        $dateUplataRata = $zaduzenje->Prva_rata_valuta_date;
      }
      if($zaduzenje->Prva_rata_iznos > $iznos_naloga) {
        $komentar = $komentar." nekompletno";
      }
    }
    else {
      if($zaduzenje->Prva_rata_valuta_date != null) {
        $dateUplataRata = add_months_to_current_date($broj_uplata-2,$zaduzenje->Pocetak_redovnih_rata);
      }      
      if($zaduzenje->Iznos_jedne_rate > $iznos_naloga) {
        $komentar = $komentar." nekompletno";
      }
    }
    $contract_no = $contract_no."Rata broj ".$broj_uplata.";";
    
    if($dateUplataRata == ""){
      $dateUplataRata = "null";
    }
  }
  else {
      $contract_no = $contract_no."Uplaceno;";
      if ($zaduzenje->Zaduzenje_iznos > $iznos_naloga){
        $komentar = $komentar." nekompletno";
      }
      else {
        if ($zaduzenje->Zaduzenje_iznos > $iznos_naloga){
          $komentar = $komentar." kompletno";
        }
      }
  }
  // updejtuj_customera($customer_id, $poziv_na_broj_id);
  /////////////////////////////////////////////////////////////////////////////////////////
//$ugovorNo // dateUplataRata // $komentar 

  $insert_id = insert_order($customer_id,$bankovni_racun,$vrsta_naloga,$zaduzenje->Modul_id,$zaduzenje->Modul_opis,$contract_no,"null","null","null","null","null","null","null","null","null","null",$iznos_naloga,
  $poziv_na_broj_id,$dateOfOrder,$dateUplataRata,$komentar,"Otvoren",$napomena,$nalog); 

  // updejtuj zaduzenje
  
  // echo "Insert: ".$insert_id;
  return $insert_id;

}

function kreiraj_zaduzenje_klijenta($customer_id,$vrsta_naloga,$iznos_naloga,$contract_no,$dateOfContract,$dateOfValute,$broj_rata,$iznos_rate,$datum_pocetka_rata,$iznos_prve_rate,$dateOfFirstInstalmentValute,$modul_id,$modul_value,$napomena,$nalog) {

  $komentar = "Placanje odjednom;";
  $broj_rata_uplaceno = "null";
  if($broj_rata > 0 ){
    $komentar = "Placanje u ".$broj_rata." rata;";
    $broj_rata_uplaceno = "0";
    if($iznos_prve_rate == "0.00"){
      $iznos_prve_rate="null";
    }
  }
  else {
    $broj_rata = "null";
    $iznos_rate = "null";
    $iznos_prve_rate = "null";
  }
  $insert_id = insert_order($customer_id,"",$vrsta_naloga,$modul_id,$modul_value,$contract_no,$dateOfContract,$dateOfValute,
  $iznos_naloga,"0.00",$broj_rata,$broj_rata_uplaceno,$iznos_rate,$datum_pocetka_rata,$iznos_prve_rate,$dateOfFirstInstalmentValute,"null",
  "null","null","null",$komentar,"Otvoren",$napomena,$nalog); 
  // updejtuj zaduzenje iznose

  return $insert_id;
}

function izracunaj_datum_zadnje_rate($broj_rata,$datum_pocetka_rata,$dateOfFirstInstalmentValute){
  echo "broj_rata:".$broj_rata." datum_pocetka_rata:".$datum_pocetka_rata." dateOfFirstInstalmentValute:".$dateOfFirstInstalmentValute;

  $new_valute_date =  add_months_to_current_date($broj_rata-1,$datum_pocetka_rata);

  if($dateOfFirstInstalmentValute != null && $dateOfFirstInstalmentValute != "null") {
    $new_valute_date =  add_months_to_current_date($broj_rata-2,$datum_pocetka_rata);
  }
  // echo "new_valute_date:".$new_valute_date;
  return $new_valute_date;
}

function add_months_to_current_date($add_months, $old_date){
  return date('Y-m-d', strtotime("+".$add_months." months", strtotime($old_date)));
}

function get_month_dif($date1,$date2){
  // $date1 = '2010-01-25'; $date2 = '2010-02-22';
  $ts1 = strtotime($date1);
  $ts2 = strtotime($date2);
  $year1 = date('Y', $ts1);
  $year2 = date('Y', $ts2);
  $month1 = date('m', $ts1);
  $month2 = date('m', $ts2);
  $day1 = date('d', $ts1);
  $day2 = date('d', $ts2);
  return (($year2 - $year1) * 12) + ($month2 - $month1) - ($day2 > $day1 ? 0 : 1);
}

// function izracunaj_datum_zadnje_rate($dateOfContract,$broj_rata,$dateOfFirstInstalmentValute){

// }

function updejtuj_customera($customer_id, $order_id){
  $order = new order($order_id);  
  $old_customer = new customer($customer_id);
  $Order_date = "";
  $Iznos_ordera = "";
  if($order->Type == "Zaduzenje"){
    $old_customer->set_zaduzenje($order->Zaduzenje_iznos);
    $Order_date = $order->Ugovor_date;
    $Iznos_ordera = $order->Zaduzenje_iznos;
  }
  else if ($order->Type == "Uplata"){
    $zaduzenje = new order($order->Uplata_poziv_na_broj);
    $result = $zaduzenje->add_uplata($order, $old_customer);
    $Order_date = $order->Uplata_date;
    $Iznos_ordera = $order->Uplata_iznos;
  }
  $new_customer = new customer($customer_id);
  $insert_id =  insert_transakcije($order->Type,$customer_id,$order_id,$Order_date,$Iznos_ordera ,$old_customer->Zaduzenje,$old_customer->Uplate,$old_customer->Saldo,$new_customer->Zaduzenje,$new_customer->Uplate,$new_customer->Saldo);
  echo "Trans_Id: ".$insert_id;
}

// Process insertovanja podataka za Order:

// Ako je Zaduzenje onda provjeriti da li ima rate
// updejtovati customera zaduzenje i saldo
// Ako ima onda insertovati onoliko redova zaduzenja koliko je trazeno (kalkulacija datuma value...)


// Ako je uplata insertovati red uplate
// updejtovati customera polja uplate i saldo
// ako ima poziv na broj, pronaci zaduzenje i zatvoriti ga ako je iznos veci od zaduzenja
// ako iam viska onda zatvarati sledece zaduzenje po datumu
// Ako nema poziv na broj ,pronaci listu zaduenja i zatvarati redom po datumima

//  insertovati transakcije
// try{
//   var_dump($_POST);

//   if($_SERVER['REQUEST_METHOD'] == 'POST'){

//     if(isset($_POST["username"]) && isset($_POST["password"])){

//       $username=$_POST["username"];
//       $password=$_POST["password"];
//       $email=$_POST["email"];  

//       if(isset($_POST['rememberme']) &&  $_POST['rememberme'] == 'yes'){
//           $marketing_flag = 'Yes';
//       }
//       else{

//         $marketing_flag = 'No';
//       }
//       insert_log($username,'create_user.php','inser_user');
//       create_user($username, $password, $email, $marketing_flag);
//     }
//     else if(isset($_POST["new_password"]) && isset($_SESSION["restart"])){

//       $session_id=$_SESSION["restart"];
//       $new_password=$_POST["new_password"];

//       $dbhost=Configuration::$dbInfo['dbhost'];
//       $dbuser=Configuration::$dbInfo['dbuser'];
//       $dbpass=Configuration::$dbInfo['dbpass'];
//       $dbname=Configuration::$dbInfo['dbname'];
//       $connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);

//       if (mysqli_connect_error($connection)){
//         die("Connection fail: ".mysqli_connect_errno($connection)." (".mysqli_connect_error($connection).")");
//       }
      
//       $sql_query= "select username from user_sessions where session_id = '{$session_id}' and status='Restart' order by id desc limit 1";   
//       $resultset = mysqli_query($connection, $sql_query) or die("database error:". mysqli_error($connection));
            
//       if(mysqli_num_rows($resultset)) {

//         while($list = mysqli_fetch_assoc($resultset)) {
//          $username=$list['username'];   
//         }
//         // var_dump($username);die();

//         $client= new user($username);
//         $client->set_status("active");
//         $client->change_password($new_password);
        
//         $_SESSION["message"]="Password usjesno promijenjen!!! </br> Molimo pokusajte da se  ulogujete";
//         $_SESSION["message_type"]="OK";
//         insert_log($username,'create_user.php','new_password');
//         redirect_to("login.php");
//       }

//     }
//     else {
//         throw new Exception("Greska !!! Nisu proslijedjeni svi potrebni podatci za  korisnika...Molim Vas kontaktirajte administatora sajta ");         
//      }  
//   }
//   else {
//      throw new Exception("Greska !!! Preskocen redosled site-a sa create_user ");  	
//   }
// }
// catch(Exception $e){
//     $_SESSION["message_type"]="error";
//     $_SESSION["message"]=$e->getMessage();
//     insert_log($e->getMessage(),'create_user.php','error');
//     header('Location: login.php');
// }  

?>