<?php 
  require_once("private/classes.php");
  $modul = new atribut($_GET['modul_id']);
  $customer = new customer($_GET['customer_id']);
  // echo "id:".$modul->Id;
  // echo "Name:".$modul->Name;
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com -->
  <title>Zaduzenja</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="live_css/bootstrap.min.css">
  <link href="live_css/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="live_css/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="live_css/jquery.min.js"></script>
  <script src="live_css/bootstrap.min.js"></script>
  <style>
  body {
    font: 400 15px Lato, sans-serif;
    line-height: 1.8;
    color: #818181;
  }
  h2 {
    font-size: 24px;
    text-transform: uppercase;
    color: #303030;
    font-weight: 600;
    margin-bottom: 30px;
  }
  h4 {
    font-size: 19px;
    line-height: 1.375em;
    color: #303030;
    font-weight: 400;
    margin-bottom: 30px;
  }  
  .jumbotron {
    background-color: #f4511e;
    color: #fff;
    padding: 100px 25px;
    font-family: Montserrat, sans-serif;
  }
  .container-fluid {
    padding: 60px 50px;
  }
  .bg-grey {
    background-color: #f6f6f6;
  }
  .logo-small {
    color: #f4511e;
    font-size: 50px;
  }
  .logo {
    color: #f4511e;
    font-size: 200px;
  }
  .thumbnail {
    padding: 0 0 15px 0;
    border: none;
    border-radius: 0;
  }
  .thumbnail img {
    width: 100%;
    height: 100%;
    margin-bottom: 10px;
  }
  .carousel-control.right, .carousel-control.left {
    background-image: none;
    color: #f4511e;
  }
  .carousel-indicators li {
    border-color: #f4511e;
  }
  .carousel-indicators li.active {
    background-color: #f4511e;
  }
  .item h4 {
    font-size: 19px;
    line-height: 1.375em;
    font-weight: 400;
    font-style: italic;
    margin: 70px 0;
  }
  .item span {
    font-style: normal;
  }
  .panel {
    border: 1px solid #f4511e; 
    border-radius:0 !important;
    transition: box-shadow 0.5s;
  }
  .panel:hover {
    box-shadow: 5px 0px 40px rgba(0,0,0, .2);
  }
  .panel-footer .btn:hover {
    border: 1px solid #f4511e;
    background-color: #fff !important;
    color: #f4511e;
  }
  .panel-heading {
    color: #fff !important;
    background-color: #f4511e !important;
    padding: 25px;
    border-bottom: 1px solid transparent;
    border-top-left-radius: 0px;
    border-top-right-radius: 0px;
    border-bottom-left-radius: 0px;
    border-bottom-right-radius: 0px;
  }
  .panel-footer {
    background-color: white !important;
  }
  .panel-footer h3 {
    font-size: 32px;
  }
  .panel-footer h4 {
    color: #aaa;
    font-size: 14px;
  }
  .panel-footer .btn {
    margin: 15px 0;
    background-color: #f4511e;
    color: #fff;
  }
  .navbar {
    margin-bottom: 0;
    background-color: #f4511e;
    z-index: 9999;
    border: 0;
    font-size: 12px !important;
    line-height: 1.42857143 !important;
    letter-spacing: 4px;
    border-radius: 0;
    font-family: Montserrat, sans-serif;
  }
  .navbar li a, .navbar .navbar-brand {
    color: #fff !important;
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
    color: #f4511e !important;
    background-color: #fff !important;
  }
  .navbar-default .navbar-toggle {
    border-color: transparent;
    color: #fff !important;
  }
  footer .glyphicon {
    font-size: 20px;
    margin-bottom: 20px;
    color: #f4511e;
  }
  .slideanim {visibility:hidden;}
  .slide {
    animation-name: slide;
    -webkit-animation-name: slide;
    animation-duration: 1s;
    -webkit-animation-duration: 1s;
    visibility: visible;
  }
  @keyframes slide {
    0% {
      opacity: 0;
      transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      transform: translateY(0%);
    }
  }
  @-webkit-keyframes slide {
    0% {
      opacity: 0;
      -webkit-transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      -webkit-transform: translateY(0%);
    }
  }
  @media screen and (max-width: 768px) {
    .col-sm-4 {
      text-align: center;
      margin: 25px 0;
    }
    .btn-lg {
      width: 100%;
      margin-bottom: 35px;
    }
  }
  @media screen and (max-width: 480px) {
    .logo {
      font-size: 150px;
    }
  }
  td ,th {
    text-align: center;
    font-weight: bold;
  }
  </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbar-default navbar-fixed-top" style="background-color: darkslategrey">
  <div class="container" style="width: 100%;margin-left: 0px;padding: 15px;">
    <div class="navbar-header" style="width:50%">

      
      <img src="logo.jpg" style="height:60px;float: left;">
      <div style="width:80%;float: left;margin-left: 10px;">
        <a class="navbar-brand" style="margin-left: 10px;float: none;width: 100%;font-size: 13px;">Modul: <?php echo $modul->Name; ?> </a>
        <input id="modul_id" type="hidden" value=" <?php echo $modul->Id; ?> " > 
      </div>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="index.php?modul_id=<?php echo ($_GET['modul_id']); ?>">Nazad</a></li>
     
        <li><a href="reports.php">Izvjestaji</a></li>
        <li><a href="logout.php">LogOut</a></li>

      </ul>
    </div>
  </div>
</nav>

<div class="jumbotron text-center" style="background-color: grey;">
  <div style="width:100%;height: 70px;">
      <!-- <h1 style="font-size: 40px;float: left;">Rokšped d.o.o.</h1>  -->
      <div style="float:left; width:50%; ">
        <h1 style="font-size: 40px;float: left;font-style: italic;"><?php echo $customer->Name;?></h1> 

        <div class="input-group" style="width:100%">  
          <div class="form-group" style="width:200px">
            <label for="sel1" style="float:left">Filtar:</label>
            <select id="filtar" class="form-control" id="vrsta_naloga" name="filtar">

              <option>Ugovor broj</option>
              <option>Ugovor datum</option>
              <option>Status</option>

            </select>
          </div>

          <input id="filtar_value" type="email" class="form-control" size="50" placeholder="Unesite željenu riječ ..." required style="width: 30%;margin-left: 20px !important ;">
          <div class="input-group-btn" style="float: left;">
            <button type="button" class="btn btn-danger" onclick="get_customer_orders_list();">Pretraži</button>
          </div>
        </div>

      </div>
      <div style="width: 50%;float: right;">
          <p style="font-size: 12px;float: left;width: 100%;text-align: right;margin-bottom: 0px;">PIB/PDV_broj:<?php echo $customer->PIB; ?> &nbsp; <?php echo $customer->PDV_broj; ?></p></br>
          <p style="font-size: 12px;float: left;width: 100%;text-align: right;margin-bottom: 0px">JMBG: &nbsp;<?php echo $customer->JMBG; ?> &nbsp;</p></br>
          <p style="font-size: 12px;float: left;width: 100%;text-align: right;margin-bottom: 0px">Adresa:&nbsp;<?php echo $customer->Address; ?> &nbsp;</p> </br>
          <p style="font-size: 12px;float: left;width: 100%;text-align: right;margin-bottom: 0px">Email:&nbsp;<?php echo $customer->Email; ?>  &nbsp;</p> </br>
          <p style="font-size: 12px;float: left;width: 100%;text-align: right;margin-bottom: 0px">Tel:&nbsp;<?php echo $customer->Telefon; ?>  &nbsp;</p> </br>
        </div>
      
  </div>

  <!-- <form> -->
    
  <!-- </form> -->

  <script type="text/javascript">
    
   function get_customer_orders_list(){
    // var my = this;
    // this.state.documents1 = ["Loading data ..."];
    // my.forceUpdate();
    var filtar_value = document.getElementById("filtar_value").value;
    var filtar = document.getElementById("filtar").value;
    var modul_id = <?php echo ($_GET['modul_id']);?>;
    var customer_id = <?php echo ($_GET['customer_id']);?>;
    //alert("Poceo: "+filtar_value + "Filtar: "+filtar);
    var xhttp; 
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        document.getElementById("tabela2").innerHTML = xhttp.response;      
        }          
    };
    xhttp.open("GET", "get_customer_orders_list.php?filtar_value=" + filtar_value +"&filtar=" + filtar+ "&modul_id=" + modul_id + "&customer_id=" + customer_id, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send( );
  }

  </script>

  
</div>

<!-- Container (About Section) -->
<div id="about" class="container-fluid" style="padding-top: 0px;">
 <table class="table table-bordered">
    <thead>
      <tr style="border: 3px solid black;">

        <th style="border-right: 2px solid black">#</th>
        <th>Tip</th>
        <!-- <th>Modul</th> -->
        <th>Ugovor broj</th>
        <th>Nalog</th>

        <th style="border-left: 2px solid black;">Datum početka zaduženja</th>
        <th>Valuta zaduženja</th>
        <th>Iznos zaduženja</th>
        <th>Do sada uplaćeno</th>
        <th>Broj rata za uplatu</th>
        <th>Uplacen broj rata </th>
        <th>Iznos jedne rate</th>
        <th>Datum početka rata</th>
        <th>Prva rata iznos</th>
        <th>Datum plaćanja prve rate</th>



        <th style="border-left: 2px solid black;border-right: 2px solid black">Status naloga</th>

      </tr>
    </thead>
    <tbody id="tabela2">

        <?php echo order::get_customer_orders_list_html(($_GET['customer_id']),($_GET['modul_id'])); ?>

      
    </tbody>
  </table>


</div>





</body>
</html>
