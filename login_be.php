<?php 
require_once("private/functions.php");

try{
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    if(isset($_POST["username"]) && isset($_POST["password"]) && $_POST["username"]!=null && $_POST["password"] !=null && trim($_POST["username"])!="" && trim($_POST["password"])!=""){

      $username=$_POST["username"];
      $password=$_POST["password"];      
      login_user($_POST["username"], $_POST["password"]);      
    }
    else {
          throw new Exception(" Username\Password POST Greska!!!");
    }
  }
  else {
       throw new Exception("POST Greska!!!");
  }
}

catch(Exception $e){
    $_SESSION["message_type"]="error";
    $_SESSION["Message"]=$e->getMessage();
    header('Location: login.php');
}    
    

?>