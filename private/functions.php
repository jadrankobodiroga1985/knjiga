<?php
session_start();

class Configuration {
	
	public static  $dbInfo = array(
		// 'driver' => 'mysql',
		'dbhost' => 'localhost',
		'dbuser' => 'root',
		'dbpass' => '',
		'dbname' => 'bar'
	);
}

// class Configuration {
	
// 	public static  $dbInfo = array(
// 		// 'driver' => 'mysql',
// 		'dbhost' => 'localhost',
// 		'dbuser' => 'root',
// 		'dbpass' => 'root',
// 		'dbname' => 'bar'
// 	);
// }

function insert_order($Customer_id,$Bank_account,$Type,$Modul_id,$Modul_opis,$Ugovor_no,$Ugovor_date,$Ugovor_valuta_date,$Zaduzenje_iznos,$Zaduzenje_uplaceno,$Broj_rata,$Broj_rata_uplaceno,$Iznos_jedne_rate,$Datum_pocetka_rata,$Prva_rata_iznos,$Prva_rata_valuta_date,$Uplata_iznos,$Uplata_poziv_na_broj,$Uplata_date,$Uplata_rata_date,$Komentar,$Status,$Napomena,$Nalog="null",$Budzet="null",$Created_date="sysdate()"){

	$dbhost=Configuration::$dbInfo['dbhost'];
	$dbuser=Configuration::$dbInfo['dbuser'];
	$dbpass=Configuration::$dbInfo['dbpass'];
	$dbname=Configuration::$dbInfo['dbname'];
    $conection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
    mysqli_set_charset($conection,'utf8');
    // var_dump($conection);
      // mysqli_query("set charset 'utf8'");

	$Bank_account = $Bank_account == "null"  ? $Bank_account : "'".$Bank_account."'" ;
	$Nalog = $Nalog == "null"  ? $Nalog : "'".$Nalog."'" ;
	$Budzet = $Budzet == "null"  ? $Budzet : "'".$Budzet."'" ;
	$Ugovor_date = $Ugovor_date == "null"  ? $Ugovor_date : "'".$Ugovor_date."'" ;
	$Ugovor_valuta_date = $Ugovor_valuta_date == "null" ? $Ugovor_valuta_date : "'".$Ugovor_valuta_date."'" ;
	$Datum_pocetka_rata = $Datum_pocetka_rata == "null" ? $Datum_pocetka_rata : "'".$Datum_pocetka_rata."'" ;
	$Prva_rata_valuta_date = $Prva_rata_valuta_date == "null" ? $Prva_rata_valuta_date : "'".$Prva_rata_valuta_date."'" ;
	$Uplata_date = $Uplata_date == "null" ? $Uplata_date : "'".$Uplata_date."'" ;
	$Uplata_rata_date = $Uplata_rata_date == "null" ? $Uplata_rata_date : "'".$Uplata_rata_date."'" ;
	// $Napomena = $Napomena == "null" ? $Napomena : "'".$Napomena."'" ;
	$last_id = NULL;
	$sql="";
	if($Created_date != "sysdate()"){ $Created_date = "'".$Created_date."'"; }
	try{
	  if (mysqli_connect_error($conection)){
	  	return $last_id;
	  }
	  $sql="insert into orders (`Customer_id`,`Bank_account`, `Type`,`Modul_id`,`Modul_opis`, `Ugovor_no`,`Ugovor_date`,`Ugovor_valuta_date`,`Zaduzenje_iznos`,`Zaduzenje_uplaceno`,`Broj_rata`,`Broj_rata_uplaceno`,`Iznos_jedne_rate`,`Pocetak_redovnih_rata`,`Prva_rata_iznos`,`Prva_rata_valuta_date`,`Uplata_iznos`,`Uplata_poziv_na_broj`,`Uplata_date`,`Uplata_rata_date`,`Komentar`,`Status`,`Created_date`,`Last_updated_date`,`Napomena`,`Nalog`,`Budzet`) VALUES (".$Customer_id.",".$Bank_account.",'".$Type."',".$Modul_id.",'".$Modul_opis."','".$Ugovor_no."',".$Ugovor_date.",".$Ugovor_valuta_date.", ".$Zaduzenje_iznos.",".$Zaduzenje_uplaceno.",".$Broj_rata.",".$Broj_rata_uplaceno.",".$Iznos_jedne_rate.",".$Datum_pocetka_rata.",".$Prva_rata_iznos.",".$Prva_rata_valuta_date.",".$Uplata_iznos.",".$Uplata_poziv_na_broj.",".$Uplata_date.",".$Uplata_rata_date.",'".$Komentar."','".$Status."',".$Created_date.", sysdate(),'".$Napomena."',".$Nalog.",".$Budzet.");";
	  // echo "SQL: ".$sql;
	  // mysqli_query($conection, $sql) or die(mysqli_error($conection));   
	    // mysqli_query("set charset 'utf8'"); 
	  if (mysqli_query($conection, $sql)) {	    
	    $last_id = $conection->insert_id;	    
	  }			  
	  else {
		$last_id= "ERROR ".(mysqli_error($conection));
		echo "SQL: ".$sql;		
	  }
	}	
	catch(Exception $e){
		echo "SQL: ".$sql;
		return $e." ERROR";
	}
  return $last_id;
}

 function insert_transakcije($Transaction_type,$Customer_id,$Order_id,$Order_date,$Iznos_ordera,$Last_customer_zaduzenje,$Last_customer_uplate ,$Last_customer_saldo,$New_customer_zaduzenje,$New_customer_uplate,$New_customer_saldo){

	$dbhost=Configuration::$dbInfo['dbhost'];
	$dbuser=Configuration::$dbInfo['dbuser'];
	$dbpass=Configuration::$dbInfo['dbpass'];
	$dbname=Configuration::$dbInfo['dbname'];
  $conection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
  mysqli_set_charset($conection,"utf8");
  $last_id = null;
	try{
	  if (mysqli_connect_error($conection)){
	  	return $last_id;
	  }
	  $sql="insert into transakcije (`Transaction_type`,`Customer_id`,`Order_id`,`Order_date`,`Iznos_ordera`, `Last_customer_zaduzenje`, `Last_customer_uplate`, `Last_customer_saldo`, `New_customer_zaduzenje`,`New_customer_uplate`, `New_customer_saldo`, `Created_date`) VALUES ('".$Transaction_type."', ".$Customer_id.",".$Order_id.", '".$Order_date."',  ".$Iznos_ordera.", ".$Last_customer_zaduzenje.", ".$Last_customer_uplate.", ".$Last_customer_saldo .", ".$New_customer_zaduzenje.", ".$New_customer_uplate.", ".$New_customer_saldo.", sysdate());";
	  echo "SQL: ".$sql;
	  // mysqli_query($conection, $sql)  or die(mysqli_error($conection));
	  if (mysqli_query($conection, $sql)) {	    
	    $last_id = $conection->insert_id;	    
		}			  
		else {
			$last_id= "ERROR";
	    }		  
	}	
	catch(Exception $e){
		$last_id= $e."ERROR";
	}
  return $last_id;
}

function login_user($username, $password){
	try{
		$required_fields=array($username,$password);
		if(validate_presences($required_fields)){

			if(empty($errors)){
			    $found_user=attempt_login($username, $password);

			    if($found_user){
			    	if (check_active_user($username)){ 						    				    	
			    		$session_result=create_user_session($username);
			    		redirect_to("moduls.php");
			        }
			        else{
			        	throw new Exception("Neuspijesno logovanje. Korisnikovo stanje nije aktivno.");			        	
			        }			     	
			    }
			    else {
			    	throw new Exception("Neuspijesno logovanje. Potrebno je unijeti ispravne podatke za logovanje.");		    	
			    }
			}
			else {
			  	throw new Exception("Problem pri logovanju. Potrebno je kontaktirati admina portala.");
			}	    
		}
		else{
			throw new Exception("Potrebno je unijeti potrebne podatke za logovanje.");
	    }
	}
	catch(Exception $e){
		$_SESSION["message_type"]="error";
		$_SESSION["Message"]=$e->getMessage();
		header('Location: login.php');
	}  
}

function validate_presences($required_fields){
	$dbhost=Configuration::$dbInfo['dbhost'];
	$dbuser=Configuration::$dbInfo['dbuser'];
	$dbpass=Configuration::$dbInfo['dbpass'];
	$dbname=Configuration::$dbInfo['dbname'];
  	$conection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
	foreach($required_fields as $field){
		$value=trim($field); 
		if (!has_presence($value)){
			$errors[$field]=fieldname_as_text($field)." Morate unijeti ispravne podatke. ";
			return false;
		}
		else {return true;
		}
	}
} 

function has_presence($value){
	return (isset($value) && $value != "");}

function fieldname_as_text($fieldname){
	$fieldname=str_replace("_", " ", $fieldname);
	$fieldname=ucfirst($fieldname);
	return $fieldname;}

function attempt_login($username, $password){
	$user=find_user_by_username($username);
	if($user){
		if(password_check($password, $user["password"])){
			return $user;
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}

function find_user_by_username($username){
	$dbhost=Configuration::$dbInfo['dbhost'];
	$dbuser=Configuration::$dbInfo['dbuser'];
	$dbpass=Configuration::$dbInfo['dbpass'];
	$dbname=Configuration::$dbInfo['dbname'];
  	$connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
	$safe_username=mysqli_real_escape_string($connection, $username);
	$query="select * from users where username='{$safe_username}' limit 1";
	$user_set=mysqli_query($connection, $query);
	if ($user_set){ 
		if($user=mysqli_fetch_assoc($user_set)){		
			return $user;
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}

function password_check($password, $existing_hash){
	$hash=crypt($password, $existing_hash);
	if ($hash===$existing_hash){ 
		return true;
	}
	else {
		return false;
	}
}

function check_active_user($username){
	$dbhost=Configuration::$dbInfo['dbhost'];
	$dbuser=Configuration::$dbInfo['dbuser'];
	$dbpass=Configuration::$dbInfo['dbpass'];
	$dbname=Configuration::$dbInfo['dbname'];
  	$connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
	$sql="select user_status from users where username='{$username}' limit 1";
	$result_data=mysqli_query($connection, $sql);
	$status=mysqli_fetch_assoc($result_data);
	if ($status["user_status"]=='active'){ 
		return $status;
	}
	else {
		return false;
	}
}

function create_user_session($username){
	$dbhost=Configuration::$dbInfo['dbhost'];
	$dbuser=Configuration::$dbInfo['dbuser'];
	$dbpass=Configuration::$dbInfo['dbpass'];
	$dbname=Configuration::$dbInfo['dbname'];
  	$connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
	$temp_session_id=sha1($username . microtime());
	$sql_query="insert into user_sessions (`username`, `session_id`,`date`) VALUES ('".$username."', '".$temp_session_id."',sysdate()) "; 	
 	$result=mysqli_query($connection, $sql_query);
 	if ($result && mysqli_affected_rows($connection)>=0){
 		$_SESSION['session_id']=$temp_session_id;
   		return true;
  	}
  	else {
  		$_SESSION['session_id']=null;
  		return false;
  	}
}

function redirect_to($new_location){
	header("Location: ".$new_location);
	exit;}

function insert_customer($Name,$Responsible_person,$JMBG,$PIB,$PDV_broj,$Address,$Status,$Zaduzenje,$Uplate,$Saldo, $Email,$Telefon){
	try{
		$dbhost=Configuration::$dbInfo['dbhost'];
		$dbuser=Configuration::$dbInfo['dbuser'];
		$dbpass=Configuration::$dbInfo['dbpass'];
		$dbname=Configuration::$dbInfo['dbname'];
  		$connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
  		$Zaduzenje=(round($Zaduzenje, 2));
      	$Uplate=(round($Uplate, 2));
      	$Saldo=(round($Saldo, 2));
  		mysqli_set_charset($connection,"utf8");
  		$sql_query=" insert into `bar`.`customers` (`Name`, `Responsible_person`, `JMBG`,`PIB`,`PDV_broj`,`Address`, `Status`, `Created_date`, `Zaduzenje`,`Uplate`,`Saldo`, `Email`, `Telefon`) VALUES ('".$Name."', '".$Responsible_person."', '".$JMBG."', '".$PIB."', '".$PDV_broj."', '".$Address."', '".$Status."',sysdate(), '".$Zaduzenje."', '".$Uplate."', '".$Saldo."','".$Email."','".$Telefon."');";
		$result=mysqli_query($connection, $sql_query);
		if ($result && mysqli_affected_rows($connection)>=0){
			return true;
		}
		else{
			throw new Exception("Neuspijesan unos. Potrebno je kontaktirati administratora sajta.");
 			return false;
		}
	}
	catch(Exception $e){
		$_SESSION["message_type"]="error";
		$_SESSION["Message"]=$e->getMessage();
		header('Location: index.php');
	}
}

function check_session(){
	$client=null;  
  	$session_id = isset($_SESSION["session_id"]) ? $_SESSION["session_id"] : null;
  	if($session_id != null){
  		$username = get_username_from_session_id($session_id);
  		if($username != null){
  			return $username;
  		}
  		else{
  			session_unset();
  			$_SESSION["message_type"]="error";
			$_SESSION["Message"]="Morate se ulogovati za pristup navedenoj stranici.";
			header('Location: login.php');  
  		}
  	}
  	else {
  		session_unset();
  		$_SESSION["message_type"]="error";
		$_SESSION["Message"]="Morate se ulogovati za pristup navedenoj stranici.";
		header('Location: login.php');   	
  	}
  }


function get_username_from_session_id($session_id){
	$username=null;
	$dbhost=Configuration::$dbInfo['dbhost'];
	$dbuser=Configuration::$dbInfo['dbuser'];
	$dbpass=Configuration::$dbInfo['dbpass'];
	$dbname=Configuration::$dbInfo['dbname'];
	$connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
    $sql_query= "select username from user_sessions where session_id = '{$session_id}' order by id desc limit 1";   
    $resultset = mysqli_query($connection, $sql_query);
    if(mysqli_num_rows($resultset)) {
	    while($list = mysqli_fetch_assoc($resultset)) {
	     	$username=$list['username'];   
	   	}
	   	return $username;
	}   
	else return null;
}


function update_order($Id,$Type,$Ugovor_no,$Ugovor_date,$Ugovor_valuta_date,$Zaduzenje_iznos,$Broj_rata,$Iznos_jedne_rate,$Pocetak_redovnih_rata,$Prva_rata_iznos,$Prva_rata_valuta_date,$Zaduzenje_uplaceno,$Broj_rata_uplaceno,$Status,$Napomena,$Nalog){
	try{
		$dbhost=Configuration::$dbInfo['dbhost'];
		$dbuser=Configuration::$dbInfo['dbuser'];
		$dbpass=Configuration::$dbInfo['dbpass'];
		$dbname=Configuration::$dbInfo['dbname'];
    	$connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
    	mysqli_set_charset($connection,"utf8");

    	$Ugovor_date = $Ugovor_date == "" ? "null" : "'".$Ugovor_date."'" ;
    	$Ugovor_valuta_date = $Ugovor_valuta_date == "" ? "null" : "'".$Ugovor_valuta_date."'" ;
    	$Zaduzenje_iznos = $Zaduzenje_iznos == "" ? "null" : "'".$Zaduzenje_iznos."'" ;
    	$Broj_rata = $Broj_rata == "" ? "null" : "'".$Broj_rata."'" ;
    	$Iznos_jedne_rate = $Iznos_jedne_rate == "" ? "null" : "'".$Iznos_jedne_rate."'" ;
    	$Pocetak_redovnih_rata = $Pocetak_redovnih_rata == "" ? "null" : "'".$Pocetak_redovnih_rata."'" ;
    	$Prva_rata_iznos = $Prva_rata_iznos == "" ? "null" : "'".$Prva_rata_iznos."'" ;
    	$Prva_rata_valuta_date = $Prva_rata_valuta_date == "" ? "null" : "'".$Prva_rata_valuta_date."'" ;
    	$Zaduzenje_uplaceno = $Zaduzenje_uplaceno == "" ? "null" : "'".$Zaduzenje_uplaceno."'" ;
    	$Broj_rata_uplaceno = $Broj_rata_uplaceno == "" ? "null" : "'".$Broj_rata_uplaceno."'" ;


  		$sql_query=" update `bar`.`orders` set `Type` = '".$Type."',`Ugovor_no` = '".$Ugovor_no."',`Ugovor_date` = ".$Ugovor_date.",`Ugovor_valuta_date` = ".$Ugovor_valuta_date.",`Zaduzenje_iznos` = ".$Zaduzenje_iznos.",`Broj_rata` = ".$Broj_rata.",`Iznos_jedne_rate` = ".$Iznos_jedne_rate.",`Pocetak_redovnih_rata` = ".$Pocetak_redovnih_rata.",`Prva_rata_iznos` = ".$Prva_rata_iznos.",`Prva_rata_valuta_date` = ".$Prva_rata_valuta_date.",`Zaduzenje_uplaceno` = ".$Zaduzenje_uplaceno.",`Broj_rata_uplaceno` = ".$Broj_rata_uplaceno.",`Status` = '".$Status."',`Napomena` = '".$Napomena."',`Nalog` = '".$Nalog."' where Id=".$Id;
	  		// echo "SQL: ".$sql_query;
	  		// mysqli_query($connection, $sql_query) or die(mysqli_error($connection));  
  			$result=mysqli_query($connection, $sql_query);
  			if ($result && mysqli_affected_rows($connection)>=0){  				
  				return true;
  			}
  			else{
  				throw new Exception("Neuspijesan update. Potrebno je kontaktirati administratora sajta.");
  			}
	}
	catch(Exception $e){
		$_SESSION["message_type"]="error";
		$_SESSION["Message"]=$e->getMessage();
		// return "Error:".$e->getMessage();;
		header('Location: index5.php');
	}

}

function update_order_2($Id,$Uplata_rata_date,$duguje,$nalog,$bankovni_racun,$budzet,$dateOfOrder,$iznos_naloga,$napomena){
	try{
		$dbhost=Configuration::$dbInfo['dbhost'];
		$dbuser=Configuration::$dbInfo['dbuser'];
		$dbpass=Configuration::$dbInfo['dbpass'];
		$dbname=Configuration::$dbInfo['dbname'];
    	$connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
    	mysqli_set_charset($connection,"utf8");

    	echo "duguje:".$duguje;
    	$Uplata_rata_date = $Uplata_rata_date == "" ? "null" : "'".$Uplata_rata_date."'" ;
    	$duguje = $duguje == "" ? "null" : "'".$duguje."'" ;
    	echo "duguje:".$duguje;
    	$nalog = $nalog == "" ? "null" : "'".$nalog."'" ;
    	$bankovni_racun = $bankovni_racun == "" ? "null" : "'".$bankovni_racun."'" ;
    	$budzet = $budzet == "" ? "null" : "'".$budzet."'" ;
    	$dateOfOrder = $dateOfOrder == "" ? "null" : "'".$dateOfOrder."'" ;
    	$iznos_naloga = $iznos_naloga == "" ? "null" : "'".$iznos_naloga."'" ;
    	$napomena = $napomena == "" ? "null" : "'".$napomena."'" ;
    	// $Zaduzenje_uplaceno = $Zaduzenje_uplaceno == "" ? "null" : "'".$Zaduzenje_uplaceno."'" ;
    	// $Broj_rata_uplaceno = $Broj_rata_uplaceno == "" ? "null" : "'".$Broj_rata_uplaceno."'" ;


  		$sql_query=" update `bar`.`orders` set `Uplata_rata_date` = ".$Uplata_rata_date.",`Zaduzenje_iznos` = ".$duguje.",`Nalog` = ".$nalog.",`Bank_account` = ".$bankovni_racun.",`Budzet` = ".$budzet.",`Uplata_date` = ".$dateOfOrder.",`Uplata_iznos` = ".$iznos_naloga.",`Napomena` = ".$napomena." where Id=".$Id;
	  		// echo "SQL: ".$sql_query;
	  		// mysqli_query($connection, $sql_query) or die(mysqli_error($connection));  
  			$result=mysqli_query($connection, $sql_query);
  			if ($result && mysqli_affected_rows($connection)>=0){  				
  				return true;
  			}
			else {
				$last_id= "ERROR ".(mysqli_error($connection));
				echo "SQL: ".$sql_query.$last_id;		
			}
	}	
	catch(Exception $e){
		echo "SQL: ".$sql;
		return $e." ERROR";
	}

}

function update_order_3($Id,$broj_rata,$datum_prispijeca,$duguje,$nalog,$bankovni_racun,$budzet,$dateOfOrder,$iznos_naloga,$ugovor_no,$napomena){
	try{
		$dbhost=Configuration::$dbInfo['dbhost'];
		$dbuser=Configuration::$dbInfo['dbuser'];
		$dbpass=Configuration::$dbInfo['dbpass'];
		$dbname=Configuration::$dbInfo['dbname'];
    	$connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
    	mysqli_set_charset($connection,"utf8");

    	$datum_prispijeca = $datum_prispijeca == "null" ? "null" : "'".$datum_prispijeca."'" ;
    	$duguje = $duguje == "null" ? "null" : "'".$duguje."'" ;
    	$nalog = $nalog == "null" ? "null" : "'".$nalog."'" ;
    	$bankovni_racun = $bankovni_racun == "null" ? "null" : "'".$bankovni_racun."'" ;
    	$budzet = $budzet == "null" ? "null" : "'".$budzet."'" ;
    	$dateOfOrder = $dateOfOrder == "null" ? "null" : "'".$dateOfOrder."'" ;
    	$iznos_naloga = $iznos_naloga == "null" ? "null" : "'".$iznos_naloga."'" ;
    	$ugovor_no = $ugovor_no == "null" ? "null" : "'".$ugovor_no."'" ;
    	$broj_rata = $broj_rata == "null" ? "null" : "'".$broj_rata."'" ;
    	$napomena = $napomena == "null" ? "null" : "'".$napomena."'" ;

  		$sql_query=" update `bar`.`orders` set `Broj_rata`= ".$broj_rata." , `Ugovor_date` = ".$datum_prispijeca.",`Zaduzenje_iznos` = ".$duguje.",`Nalog` = ".$nalog.",`Bank_account` = ".$bankovni_racun.",`Budzet` = ".$budzet.",`Uplata_date` = ".$dateOfOrder.",`Zaduzenje_uplaceno` = ".$iznos_naloga.",`Ugovor_no` = ".$ugovor_no.",`Napomena` = ".$napomena." where Id=".$Id;
  			$result=mysqli_query($connection, $sql_query);
  			if ($result && mysqli_affected_rows($connection)>=0){  				
  				return true;
  			}
			else {
				$last_id= "ERROR ".(mysqli_error($connection));
				echo "SQL: ".$sql_query.$last_id;		
			}
	}	
	catch(Exception $e){
		echo "SQL: ".$sql;
		return $e." ERROR";
	}

}



?>
