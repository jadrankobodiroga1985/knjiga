<?php

 // define('ROOT', 'C:/wamp64/www/srdja_projekat/knjiga/');
define('ROOT', 'C:/wamp64/www/Knjiga/');
require_once(ROOT . "private/functions.php");
  // require_once("../private/functions.php");



class order {

  public $Id;
  public $Customer_id;
  public $Bank_account;
  public $Type;
  public $Modul_id;
  public $Modul_opis;
  public $Ugovor_no;
  public $Ugovor_date;
  public $Ugovor_valuta_date;

  public $Zaduzenje_iznos;
  public $Zaduzenje_uplaceno;
  public $Broj_rata;
  public $Iznos_jedne_rate;
  public $Pocetak_redovnih_rata;
  public $Prva_rata_iznos;
  public $Prva_rata_valuta_date;

  public $Uplata_iznos;
  public $Uplata_poziv_na_broj;
  public $Uplata_date;

  public $Komentar;
  public $Status;
  public $Created_date;
  public $Last_updated_date;
  public $Napomena;
  public $Nalog;
  public $Budzet;

  public $connection;

public function __construct($Id){
  $this->Id=$Id;
  $this->db_connection();
  $this->get_order($this->Id);  
 }
 
 public function db_connection(){
  $dbhost=Configuration::$dbInfo['dbhost'];
  $dbuser=Configuration::$dbInfo['dbuser'];
  $dbpass=Configuration::$dbInfo['dbpass'];
  $dbname=Configuration::$dbInfo['dbname'];
  $this->connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
  mysqli_set_charset($this->connection,"utf8");
  if (mysqli_connect_error($this->connection)){
   die("Connection fail: ".mysqli_connect_errno($this->connection)." (".mysqli_connect_error($this->connection).")");
  }
 }


 public static function kreiraj_pocetna_stanja($year){
    $dbhost=Configuration::$dbInfo['dbhost'];
    $dbuser=Configuration::$dbInfo['dbuser'];
    $dbpass=Configuration::$dbInfo['dbpass'];
    $dbname=Configuration::$dbInfo['dbname'];
    $connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
    mysqli_set_charset($connection,"utf8");

    $connection2=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
    mysqli_set_charset($connection2,"utf8");

  $sql_query= " select Id from orders where type in ('Zaduzenje','Pocetno_stanje') and Zaduzenje_iznos > Zaduzenje_uplaceno and ".($year-1)." = YEAR(Ugovor_date) order by Id desc"; 
  $sql_query2= " select Id from orders where type in ('Avans','Pocetno_Avans') and Zaduzenje_iznos != Uplata_iznos and ".($year-1)." = YEAR(Ugovor_date) order by Id desc"; 
  // echo "SQL: ".$sql_query;
  // echo "SQL2: ".$sql_query2;
  try{
    $resultset = mysqli_query($connection, $sql_query) or die("database error:". mysqli_error($connection));      
    if(mysqli_num_rows($resultset)) {
      while($list = mysqli_fetch_assoc($resultset)) {
        $old_order = new order($list['Id']);
        $new_ugovor_no = $year." Pocetno_stanje:".$old_order->Ugovor_no;
        $new_cotract_date = $year."-01-01";
        $new_zaduzenje_iznos = $old_order->Zaduzenje_iznos - $old_order->Zaduzenje_uplaceno;
        $new_broj_rata_uplaceno = "null";
        $new_broj_rata = "null";
        $new_iznos_jedne_rate = "null";
        $new_pocetak_redovnih_rata = "null";
        $new_prva_rata_iznos = "null";
        $new_prva_rata_valuta_date = "null";

        if($old_order->Broj_rata != null){
          if($old_order->Broj_rata > $old_order->Broj_rata_uplaceno){

            $new_broj_rata_uplaceno = "0";
            $new_broj_rata = $old_order->Broj_rata - $old_order->Broj_rata_uplaceno;
            // echo "new_broj_rata: ".$new_broj_rata;
            $new_iznos_jedne_rate = $old_order->Iznos_jedne_rate;
            // echo "new_iznos_jedne_rate: ".$new_iznos_jedne_rate;
            $timestamp = strtotime($old_order->Pocetak_redovnih_rata);
            // echo "timestamp: ".$timestamp;
            $num_of_day = date("d", $timestamp);
            // echo "num_of_day: ".$num_of_day;
            $new_pocetak_redovnih_rata = $year."-01-".$num_of_day;
            // echo "new_pocetak_redovnih_rata: ".$new_pocetak_redovnih_rata;
            if($old_order->Broj_rata_uplaceno == 0){
              if($old_order->Prva_rata_iznos != null){
                $new_prva_rata_iznos = $old_order->Prva_rata_iznos;
                // echo "new_prva_rata_iznos: ".$new_prva_rata_iznos;          
              }
              if($old_order->Prva_rata_valuta_date != null){
                $new_prva_rata_iznos = $old_order->Prva_rata_valuta_date;
                // echo "new_prva_rata_iznos: ".$new_prva_rata_iznos;            
              }
            }                               
          }
        }           
        $new_komentar = "Pocetno stanje ".$new_zaduzenje_iznos.";";
        $new_created_date = $year."-01-01";
        $insert_id = insert_order($old_order->Customer_id,"","Pocetno_stanje",$old_order->Modul_id,$old_order->Modul_opis,$new_ugovor_no,$new_cotract_date,$old_order->Ugovor_valuta_date,
        $new_zaduzenje_iznos,"0.00",$new_broj_rata,$new_broj_rata_uplaceno,$new_iznos_jedne_rate,$new_pocetak_redovnih_rata,$new_prva_rata_iznos,$new_prva_rata_valuta_date,"null",
        $old_order->Id,"null","null",$new_komentar,"Otvoren",$old_order->Napomena,$old_order->Nalog,$old_order->Budzet,$new_created_date); 
        if(strpos($insert_id, 'ERROR') !== false){
          // echo "Greska";
          throw new Exception($insert_id);
        }
      }
    }


     $resultset2 = mysqli_query($connection2, $sql_query2) or die("database error:". mysqli_error($connection2));      
     if(mysqli_num_rows($resultset2)) {
      while($list2 = mysqli_fetch_assoc($resultset2)) {
        // echo "Id:".$list['Id'];
        $old_order = new order($list2['Id']);

        $new_ugovor_no="";
        if($old_order->Ugovor_no != null){
          $new_ugovor_no = $old_order->Ugovor_no;
        }

        $new_cotract_date = "null";
        if($old_order->Ugovor_date != null){
          $new_cotract_date = $year."-01-01";
        }

        $new_uplata_date = "null";
        if($old_order->Uplata_date != null){
          $new_uplata_date = $year."-01-01";
        }
        
        $new_zaduzenje_iznos = "null";
        $new_uplate_iznos = "null";
        if($old_order->Zaduzenje_iznos == null) { $old_order->Zaduzenje_iznos = "0.00"; }
        if($old_order->Uplata_iznos == null) { $old_order->Uplata_iznos = "0.00"; }
        if($old_order->Zaduzenje_iznos > $old_order->Uplata_iznos){
          $new_zaduzenje_iznos = $old_order->Zaduzenje_iznos - ($old_order->Uplata_iznos);
          $new_uplata_date = "null";
        }
        else {
          $new_uplate_iznos = $old_order->Uplata_iznos - ($old_order->Zaduzenje_iznos);
          $new_cotract_date = "null";
        }        

        $new_broj_rata_uplaceno = "null";
        $new_broj_rata = $old_order->Broj_rata == null ? "null" : $old_order->Broj_rata;
        $new_iznos_jedne_rate = "null";
        $new_pocetak_redovnih_rata = "null";
        $new_prva_rata_iznos = "null";
        $new_prva_rata_valuta_date = "null";
             
        $new_created_date = $year."-01-01";
        $new_komentar = "Pocetno stanje ".$new_zaduzenje_iznos.";";
        $insert_id = insert_order($old_order->Customer_id,"","Pocetno_Avans",$old_order->Modul_id,$old_order->Modul_opis,$new_ugovor_no,$new_cotract_date,"null",
        $new_zaduzenje_iznos,"0.00",$new_broj_rata,$new_broj_rata_uplaceno,$new_iznos_jedne_rate,"null",$new_prva_rata_iznos,$new_prva_rata_valuta_date,$new_uplate_iznos,
        $old_order->Id,$new_uplata_date,"null",$new_komentar,"Otvoren",$old_order->Napomena,$old_order->Nalog,$old_order->Budzet,$new_created_date); 
        if(strpos($insert_id, 'ERROR') !== false){
          echo "Greska";
          throw new Exception($insert_id);
        }
        // else {
        //   echo $insert_id;
        // }
        // echo "insert_id: ".$insert_id;
        //updejtuj customera
        // insertuj transakciju
        
       }
   }  
  return true;
  }
  catch(Exception $e){
    return $e."ERROR";
  }
 }

  public function set_new_order_state($new_state){
    try{       
       $sql="update orders set Status = '".$new_state."' where Id = ".$this->Id." limit 1";
       echo "SQL:".$sql;
       $result_data = mysqli_query($this->connection, $sql);
       // mysqli_query($this->connection, $sql) or die(mysqli_error($this->connection)); 
       if($result_data){
        // $customer->set_uplata(0-$uplata->Uplata_iznos);
         return "OK";
       }
       else {
         return "ERROR".(mysqli_error($this->connection));
       }                 
    }
    catch(Exception $e){
      return "ERROR ".$e->getMessage();    
    }
  }

  public function minus_uplata_do_sada(order $uplata, customer $customer){
    try{
       $status_text="";       
       if(($this->Zaduzenje_uplaceno - ($uplata->Uplata_iznos)) >= $this->Zaduzenje_iznos){
          $status_text=" ,Status='Zatvoren' ";
       }
       else {
          $status_text=" ,Status='Otvoren' ";
       }      
       $new_zaduzenje_uplaceno = $this->Zaduzenje_uplaceno - $uplata->Uplata_iznos;
       $sql="update orders set Zaduzenje_uplaceno = ".$new_zaduzenje_uplaceno.", Komentar = '".$this->Komentar.";Storno ".$uplata->Komentar."' ".$status_text." where Id = ".$this->Id." limit 1";
       echo "SQL:".$sql;
       $result_data = mysqli_query($this->connection, $sql);
       // mysqli_query($this->connection, $sql) or die(mysqli_error($this->connection)); 
       if($result_data){
        $customer->set_uplata(0-($uplata->Uplata_iznos));
         return "OK";
       }
       else {
         return "ERROR".(mysqli_error($this->connection));
       }                 
    }
    catch(Exception $e){
      return "ERROR ".$e->getMessage();    
    }
  }

  public function add_uplata_do_sada(order $uplata, customer $customer){
    try{
       $status_text="";       
       if(($this->Zaduzenje_uplaceno + ($uplata->Uplata_iznos)) >= $this->Zaduzenje_iznos){
          $status_text=" ,Status='Zatvoren' ";
       }
       else {
          $status_text=" ,Status='Otvoren' ";
       }     
       $new_zaduzenje_uplaceno = $this->Zaduzenje_uplaceno + ($uplata->Uplata_iznos);
       $sql="update orders set Zaduzenje_uplaceno = ".$new_zaduzenje_uplaceno.", Komentar = '".$this->Komentar.$uplata->Komentar."' ".$status_text." where Id = ".$this->Id." limit 1";
       $result_data = mysqli_query($this->connection, $sql);
       if($result_data){
         $customer->set_uplata($uplata->Uplata_iznos);
         return "OK";
       }
       else {
         return "ERROR".(mysqli_error($this->connection));
       }                 
    }
    catch(Exception $e){
      return "ERROR ".$e->getMessage();    
    }
  }

  public function add_uplata(order $uplata, customer $customer){
    try{
       $status_text="";       
       if(($this->Zaduzenje_uplaceno + ($uplata->Uplata_iznos)) >= $this->Zaduzenje_iznos){
          $status_text=" ,Status='Zatvoren' ";
       }
       else {
          $status_text=" ,Status='Otvoren' ";
       }
       $broj_rata_text="";
       if($this->Broj_rata !=null and $uplata->Type == "Uplata" and $this->Broj_rata > 0){

          $broj_rata_text=" ,Broj_rata_uplaceno= ".($this->Broj_rata_uplaceno+1)." ";
          if($uplata->Uplata_iznos < 0 and $this->Broj_rata_uplaceno >0 ) {
            $broj_rata_text=" ,Broj_rata_uplaceno= ".($this->Broj_rata_uplaceno-1)." ";
          }
       }
       $new_zaduzenje_uplaceno = $this->Zaduzenje_uplaceno + ($uplata->Uplata_iznos);
       $sql="update orders set Zaduzenje_uplaceno = ".$new_zaduzenje_uplaceno.", Komentar = '".$this->Komentar.$uplata->Komentar."' ".$status_text.$broj_rata_text." where Id = ".$this->Id." limit 1";
       $result_data = mysqli_query($this->connection, $sql);
       if($result_data){
         $customer->set_uplata($uplata->Uplata_iznos);
         return "OK";
       }
       else {
         return "ERROR".(mysqli_error($this->connection));
       }                 
    }
    catch(Exception $e){
      return "ERROR ".$e->getMessage();    
    }
  }

  public function minus_uplata(order $uplata, customer $customer){
    try{
       $status_text="";       
       if(($this->Zaduzenje_uplaceno - ($uplata->Uplata_iznos)) >= $this->Zaduzenje_iznos){
          $status_text=" ,Status='Zatvoren' ";
       }
       else {
          $status_text=" ,Status='Otvoren' ";
       }
       $broj_rata_text="";
       if($this->Broj_rata !=null and $uplata->Type == "Uplata"){
          $broj_rata_text=" ,Broj_rata_uplaceno= ".($this->Broj_rata_uplaceno-1)." ";
       }
       $new_zaduzenje_uplaceno = $this->Zaduzenje_uplaceno - $uplata->Uplata_iznos;
       $sql="update orders set Zaduzenje_uplaceno = ".$new_zaduzenje_uplaceno.", Komentar = '".$this->Komentar.";Storno ".$uplata->Komentar."' ".$status_text.$broj_rata_text." where Id = ".$this->Id." limit 1";
       echo "SQL:".$sql;
       $result_data = mysqli_query($this->connection, $sql);
       // mysqli_query($this->connection, $sql) or die(mysqli_error($this->connection)); 
       if($result_data){
        $customer->set_uplata(0-($uplata->Uplata_iznos));
         return "OK";
       }
       else {
         return "ERROR".(mysqli_error($this->connection));
       }                 
    }
    catch(Exception $e){
      return "ERROR ".$e->getMessage();    
    }
  }

 
 public function get_order($Id){
  
  $sql_query= "select * from orders where Id = '{$Id}' "; 
    
  $resultset = mysqli_query($this->connection, $sql_query) or die("database error:". mysqli_error($this->connection));     
    
  if(mysqli_num_rows($resultset)) {
    while($list = mysqli_fetch_assoc($resultset)) {
     $this->Customer_id = $list['Customer_id'];   
     $this->Bank_account = $list['Bank_account'];
     $this->Type = $list['Type'];
     $this->Modul_id = $list['Modul_id'];
     $this->Modul_opis = $list['Modul_opis'];    
     $this->Ugovor_no = $list['Ugovor_no'];
     $this->Ugovor_date = $list['Ugovor_date'];
     $this->Ugovor_valuta_date = $list['Ugovor_valuta_date'];

     $this->Zaduzenje_iznos = $list['Zaduzenje_iznos'];
     $this->Zaduzenje_uplaceno = $list['Zaduzenje_uplaceno'];
     $this->Broj_rata = $list['Broj_rata'];
     $this->Broj_rata_uplaceno = $list['Broj_rata_uplaceno'];
     $this->Iznos_jedne_rate = $list['Iznos_jedne_rate'];
     $this->Pocetak_redovnih_rata = $list['Pocetak_redovnih_rata'];
     $this->Prva_rata_iznos = $list['Prva_rata_iznos'];
     $this->Prva_rata_valuta_date = $list['Prva_rata_valuta_date'];

     $this->Uplata_iznos = $list['Uplata_iznos'];
     $this->Uplata_poziv_na_broj = $list['Uplata_poziv_na_broj'];
     $this->Uplata_date = $list['Uplata_date'];
     
     $this->Komentar = $list['Komentar'];     
     $this->Status = $list['Status'];     
     $this->Created_date = $list['Created_date'];     
     $this->Last_updated_date = $list['Last_updated_date']; 
     $this->Napomena = $list['Napomena']; 
     $this->Nalog = $list['Nalog'];    
     $this->Budzet = $list['Budzet'];    
   }
  }
  else {
   return false;
  }
  return true;
 }

/*public function delete_order(){
  // promijeni status ordera u Deleted
  // updejtuj komentar sa novom transakcijom za Delete
  if($this->Type == "Uplata" ) {      
      return false;// updejtuj Customerovu kolonu za uplate,tako i saldo      
      // updejtuj ordere zaduzenja koja se odnosi ova uplata
  }
  else ($this->Type == "Zaduženje") {
    return false;
      // updejtuj Customerovu kolonu za Zaduzenja,tako i saldo      
  }
      // insertuj novu transakciju
}*/

public static function get_all_list_of_zaduzenja_from_modul($modul_id,$year){

    $dbhost=Configuration::$dbInfo['dbhost'];
    $dbuser=Configuration::$dbInfo['dbuser'];
    $dbpass=Configuration::$dbInfo['dbpass'];
    $dbname=Configuration::$dbInfo['dbname'];
    $connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
    mysqli_set_charset($connection,"utf8");
    $return_html="";
    $sql_query = "select * from orders where Modul_id = ".$modul_id." and Type in ('Pocetno_stanje','Zaduzenje') and ".($year)." = YEAR(Ugovor_date)  order by Customer_id asc";      
    $result_data = mysqli_query($connection, $sql_query) or die("database error:". mysqli_error($this->connection));
    
    if(mysqli_num_rows($result_data)) { 
      $i=0;
      while($result_set = mysqli_fetch_assoc($result_data)) {           
             
        $customer = new customer($result_set['Customer_id']);
        // echo "customer_id:".$customer->Id;
        $return_html=$return_html."<a href=\"./index5.php?order_id=".$result_set['Id']."&modul_id=".$result_set['Modul_id']."&year=".$year."\" style=\"font-size:13px;font-weight:bold;text-align:left;padding:0px\">".$customer->Name." Ugovor_broj:".$result_set['Ugovor_no']." Iznos:".$result_set['Zaduzenje_iznos']."</a>";
       
      }     
      // echo $return_html;
      return $return_html;
    }
    else {      
      // echo "Nema rezultata...";
      return "Nema rezultata...";// "No results..."
    }
  }

  public static function get_years_of_orders($selected_year,$ugovor){

    $dbhost=Configuration::$dbInfo['dbhost'];
    $dbuser=Configuration::$dbInfo['dbuser'];
    $dbpass=Configuration::$dbInfo['dbpass'];
    $dbname=Configuration::$dbInfo['dbname'];
    $connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
    mysqli_set_charset($connection,"utf8");
    $return_html="";
    if($ugovor == "avans") {
    $sql_query = "select distinct YEAR(Ugovor_date) years from orders where Type in ('Pocetno_Avans','Avans') and YEAR(Ugovor_date) is not null order by years asc ";
    }
    else {
      $sql_query = "select distinct YEAR(Ugovor_date) years from orders where Type in ('Zaduzenje','Pocetno_stanje') and YEAR(Ugovor_date) is not null order by years asc ";
    }      
    $result_data = mysqli_query($connection, $sql_query) or die("database error:". mysqli_error($connection));
    
    if(mysqli_num_rows($result_data)) { 
      while($result_set = mysqli_fetch_assoc($result_data)) {           
       $text_selected = "";
       if($result_set['years'] == $selected_year){
          $text_selected = " selected ";
       }
       $return_html=$return_html."<option value=\"".$result_set['years']."\" ".$text_selected.">".$result_set['years']."</option>";          
      }     
      return $return_html;
    }
    else {      
      // echo "Nema rezultata...";
      return "Nema rezultata...";// "No results..."
    }
  }

public static function get_all_list_of_customers(){

    $dbhost=Configuration::$dbInfo['dbhost'];
    $dbuser=Configuration::$dbInfo['dbuser'];
    $dbpass=Configuration::$dbInfo['dbpass'];
    $dbname=Configuration::$dbInfo['dbname'];
    $connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
    mysqli_set_charset($connection,"utf8");
    $return_html="";
    $sql_query = "select * from customers  order by Name asc";      
    $result_data = mysqli_query($connection, $sql_query) or die("database error:". mysqli_error($this->connection));
    
    if(mysqli_num_rows($result_data)) { 
      $i=0;
      while($result_set = mysqli_fetch_assoc($result_data)) {                        
        $return_html=$return_html."<a onclick=\"set_customer('".$result_set['Id']."','".$result_set['Name']."');\" style=\"font-size:13px;font-weight:bold;text-align:left;padding:0px\">".$result_set['Name']."</a>";       
      }     
      // echo $return_html;
      return $return_html;
    }
    else {      
      // echo "Nema rezultata...";
      return "Nema rezultata...";// "No results..."
    }
  }



public static function get_pocetna_stanja_list_html($year){

    $dbhost=Configuration::$dbInfo['dbhost'];
    $dbuser=Configuration::$dbInfo['dbuser'];
    $dbpass=Configuration::$dbInfo['dbpass'];
    $dbname=Configuration::$dbInfo['dbname'];
    $connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
    mysqli_set_charset($connection,"utf8");
    $return_html="";
    $sql_query = "select * from orders where type in ('Pocetno_stanje','Pocetno_Avans') and ".($year)." = YEAR(Ugovor_date) order by Id asc";      
    $result_data = mysqli_query($connection, $sql_query) or die("database error:". mysqli_error($this->connection));
    
    if(mysqli_num_rows($result_data)) { 
      $i=0;
      while($result_set = mysqli_fetch_assoc($result_data)) {       
        
        
        $i++;

        $return_html=$return_html."<tr class=\"info\">";
        $return_html=$return_html."<td style=\"border-left: 2px solid black;border-right: 2px solid black\">".$i."</td>";
        $customer = new customer($result_set['Customer_id']);
        $return_html=$return_html."<td>".$customer->Name."</td>";
        $return_html=$return_html."<td>".$result_set['Type']."</td>";
        $return_html=$return_html."<td>".$result_set['Ugovor_no']."</td>";
        $return_html=$return_html."<td style=\"border-left: 2px solid black;\">".$result_set['Ugovor_date']."</td>";
        $return_html=$return_html."<td>".$result_set['Ugovor_valuta_date']."</td>";
        $return_html=$return_html."<td>".$result_set['Zaduzenje_iznos']." E</td>";
        $return_html=$return_html."<td>0.00 E</td>";
        $return_html=$return_html."<td>".$result_set['Broj_rata']."</td>";
        $return_html=$return_html."<td>".$result_set['Broj_rata_uplaceno']."</td>";
        $return_html=$return_html."<td>".$result_set['Iznos_jedne_rate']."</td>";
        $return_html=$return_html."<td>".$result_set['Pocetak_redovnih_rata']."</td>";
        $return_html=$return_html."<td>".$result_set['Prva_rata_iznos']."</td>";
        $return_html=$return_html."<td>".$result_set['Prva_rata_valuta_date']."</td>";
        $return_html=$return_html."<td style=\"border-left: 2px solid black;border-right: 2px solid black\">".$result_set['Status']."</td>";
        $return_html=$return_html."<td style=\"border-left: 2px solid black;border-right: 2px solid black\">";
        $return_html=$return_html."<button type=\"button\" class=\"btn btn-danger\" onclick=\"redirect_to_update_page('".$result_set['Id']."','".$result_set['Modul_id']."')\">Izmjena</button>";
        $return_html=$return_html."</td>";
        $return_html=$return_html."</tr>";
      }     
      // echo $return_html;
      return $return_html;
    }
    else {      
      // echo "Nema rezultata...";
      return "Nema rezultata...";// "No results..."
    }
  }

public static function get_customer_orders_list_html($Customer_id, $Modul_id=null, $filtar_type=null,$filtar_value=null,$year=null){

    $dbhost=Configuration::$dbInfo['dbhost'];
    $dbuser=Configuration::$dbInfo['dbuser'];
    $dbpass=Configuration::$dbInfo['dbpass'];
    $dbname=Configuration::$dbInfo['dbname'];
    $connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
    mysqli_set_charset($connection,"utf8");
    $return_html="";
    $dodatak="";
    if ($Modul_id != null){
        $dodatak = " and Modul_id = ".$Modul_id ;
    }
    if (($filtar_type == null) || ($filtar_value == null)){
        $filtar = "";
    }
    else {
       $filtar = " and ".$filtar_type." like '%".$filtar_value."%' ";
    }
    $year_text = "";
    if($year != null){
      $year_text = " and ".$year." = YEAR(Ugovor_date) ";
    }
    $sql_query = "select * from orders where Type in ('Zaduzenje','Pocetno_stanje') and Customer_id = ".$Customer_id.$dodatak.$filtar.$year_text." order by Id asc";     
    // echo $sql_query; 
    $result_data = mysqli_query($connection, $sql_query) or die("database error:". mysqli_error($connection));
    
    if(mysqli_num_rows($result_data)) { 
      $i=0;
      if ($Modul_id != null){
        while($result_set = mysqli_fetch_assoc($result_data)) { 
        $i++;        
        
        $class_type = "";
        $type = $result_set['Type'];
        if( $type == "Uplata"){
          $class_type = "class=\"active\"";
        }
        else if( $type == "Zaduženje"){
          if($result_set['Status'] == "Zatvoreno"){
            $class_type = "class=\"success\"";
          }          
        }
        //date("Y-m-d", strtotime($var) );
        $return_html=$return_html."<tr class=\"info\">";
        $return_html=$return_html."<td style=\"border-left: 2px solid black;border-right: 2px solid black\">".$i."</td>";
        $return_html=$return_html."<td>".$result_set['Type']."</td>";
        $return_html=$return_html."<td>".$result_set['Ugovor_no']."</td>";
        $return_html=$return_html."<td>".$result_set['Nalog']."</td>";
        $return_html=$return_html."<td style=\"border-left: 2px solid black;\">".date("Y-m-d", strtotime($result_set['Ugovor_date']))."</td>";
        $return_html=$return_html."<td>".date("Y-m-d", strtotime($result_set['Ugovor_valuta_date']))."</td>";
        $return_html=$return_html."<td>".$result_set['Zaduzenje_iznos']."E</td>";
        $return_html=$return_html."<td>".$result_set['Zaduzenje_uplaceno']."E</td>";
        $return_html=$return_html."<td>".$result_set['Broj_rata']."</td>";
        $return_html=$return_html."<td>".$result_set['Broj_rata_uplaceno']."</td>";
        $return_html=$return_html."<td>".$result_set['Iznos_jedne_rate']."</td>";
        if($result_set['Pocetak_redovnih_rata'] != null and $result_set['Pocetak_redovnih_rata']!=""){
          $return_html=$return_html."<td>".date("Y-m-d", strtotime($result_set['Pocetak_redovnih_rata']))."</td>";
        }
        else {
          $return_html=$return_html."<td>".$result_set['Pocetak_redovnih_rata']."</td>";
        }
        $return_html=$return_html."<td>".$result_set['Prva_rata_iznos']."</td>";
        $return_html=$return_html."<td>".date("Y-m-d", strtotime($result_set['Prva_rata_valuta_date']))."</td>"; 
        $return_html=$return_html."<td style=\"border-left: 2px solid black;border-right: 2px solid black\">".$result_set['Status']."</td>"; 
        $return_html=$return_html."<td style=\"border-left: 2px solid black;border-right: 2px solid black\">";  
        $return_html=$return_html."<a class=\"btn btn-primary\" href=\"index5.php?order_id=".$result_set['Id']."&modul_id=".$result_set['Modul_id']."\">Uplate</a>";  
        $return_html=$return_html."</td>";   
        $return_html=$return_html."</tr>";
      }

      }
     else {
      while($result_set = mysqli_fetch_assoc($result_data)) {  
      $i++;       
        
        $class_type = "";
        $type = $result_set['Type'];
        if( $type == "Uplata"){
          $class_type = "class=\"active\"";
        }
        else if( $type == "Zaduženje"){
          if($result_set['Status'] == "Zatvoreno"){
            $class_type = "class=\"success\"";
          }
          // else if($result_set['Valute_date'] < danas ){
          //   $class_type = "class=\"danger\"";
          // }          
        }
        $i++;
        $return_html=$return_html."<tr class=\"success\">";
        $return_html=$return_html."<td>".$i."</td>";
        $return_html=$return_html."<td>".$result_set['Type']."</td>";
        $return_html=$return_html."<td>".$result_set['Bank_account']."</td>";
        $return_html=$return_html."<td>".$result_set['Order_date']."</td>";
        $return_html=$return_html."<td>".$result_set['Valute_date']."</td>";
        $return_html=$return_html."<td>".$result_set['Zaduzenje_iznos']."E</td>";
        $return_html=$return_html."<td>".$result_set['Placanje_iznos']."E</td>";
        $return_html=$return_html."<td>".$result_set['Svrha_placanja_id']."</td>";
        $return_html=$return_html."<td>".$result_set['Uplata_poziv_na_broj']."</td>";
        $return_html=$return_html."<td>".$result_set['Zaduzenje_Id_jevi']."</td>";
        $return_html=$return_html."<td>".$result_set['Komentar']."</td>";
        $return_html=$return_html."<td>".$result_set['Status']."</td>";        
        $return_html=$return_html."</tr>";
        }
      }    
      return $return_html;
      //return true;
    }
    else {      
      echo "Nema rezultata...";
      return false;// "No results..."
    }
  }

  public  function get_uplate_from_zaduzenje(){

    $dbhost=Configuration::$dbInfo['dbhost'];
    $dbuser=Configuration::$dbInfo['dbuser'];
    $dbpass=Configuration::$dbInfo['dbpass'];
    $dbname=Configuration::$dbInfo['dbname'];
    $connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
    mysqli_set_charset($connection,"utf8");
    $return_html="";
    $sql_query = "select * from orders where  Uplata_poziv_na_broj = ".$this->Id." and Type in ('Uplata','Storno','Info') order by Id asc";     
    $result_data = mysqli_query($connection, $sql_query) or die("database error:". mysqli_error($this->connection));    
    if(mysqli_num_rows($result_data)) { 
      $i=0;
      while($result_set = mysqli_fetch_assoc($result_data)) {         
        $storno_prefix="";
        if($result_set['Type'] == "Storno"){
           $storno_prefix="-";
        }
        $del_prefix="";
        $del_sufix="";
        $del_style = "";
        if($result_set['Status'] == "Deleted"){
           $del_prefix="<del>";
           $del_sufix="</del>";
           $del_style= " ;background:lightcoral ";
        }
        $i++;
        $return_html=$return_html."<form action=\"./uplata.php\" method=\"post\">";
        $return_html=$return_html."<tr>";
        $return_html=$return_html."<input type=\"hidden\"  name=\"process\"  value=\"uplata_update\">";
        $return_html=$return_html."<input type=\"hidden\"  name=\"order_id\"  value=\"".$result_set['Id']."\">";

//// Redni broj
        $return_html=$return_html."<td style=\"border-left: 2px solid black;border-right: 2px solid black".$del_style."\">".$i."</td>";
//// Tip naloga
        if($result_set['Type'] == "Info"){
          $return_html=$return_html."<td>".$del_prefix."Zaduzenje".$del_sufix."</td>";
        }
        else {
           $return_html=$return_html."<td>".$del_prefix.$result_set['Type'].$del_sufix."</td>"; 
        }
//// Broj rata
        if($result_set['Type'] == 'Uplata' && strpos($result_set['Komentar'], 'Rata') !== false ){
          $return_html=$return_html."<td>".$del_prefix.(str_replace('nekompletno','',str_replace('Uplata Rata ','',$result_set['Komentar']))).$del_sufix."</td>";
        }
        else {
            $return_html=$return_html."<td></td>";
        }
///// Datum dospijeca
        if($result_set['Uplata_rata_date'] != null and $result_set['Uplata_rata_date']!=""){
          $return_html=$return_html."<td style=\"padding: 0px;\"><input type=\"date\" class=\"w3-input w3-border w3-margin-bottom\"  value=\"".date("Y-m-d", strtotime($result_set['Uplata_rata_date'])).$del_sufix."\" style=\"height:100%\"  name=\"Uplata_rata_date\" > </td>";
        }
        else {
          $return_html=$return_html."<td style=\"padding: 0px;\"><input type=\"date\" class=\"w3-input w3-border w3-margin-bottom\" style=\"height:100%\" value=\"\"  name=\"Uplata_rata_date\" ></td>";
        }        
////// Duguje
        if($result_set['Type'] == 'Info'){
          $return_html=$return_html."<td style=\"padding: 0px;\"><input class=\"w3-input w3-border w3-margin-bottom\" type=\"text\" value=\"".$result_set['Zaduzenje_iznos']."\"   style=\"width:100%;height:100%".$del_style."\" name=\"Duguje\"></td>";
        }
        else {
            $return_html=$return_html."<td style=\"padding: 0px;\"><input class=\"w3-input w3-border w3-margin-bottom\" type=\"text\" value=\"\" style=\"width:100%;height:100%".$del_style."\" name=\"Duguje\"  ></td>";
        }
////// Nalog
        $return_html=$return_html."<td style=\"padding: 0px;\"><input class=\"w3-input w3-border w3-margin-bottom\" type=\"text\" value=\"".$result_set['Nalog']."\" style=\"width:100%;height:100%\"  name=\"Nalog\" ></td>";
////// Izvod      
        $return_html=$return_html."<td style=\"padding: 0px;\"><input class=\"w3-input w3-border w3-margin-bottom\" type=\"text\" value=\"".$result_set['Bank_account']."\" style=\"width:100%;height:100%\" name=\"Izvod\" > </td>";
////// Budzet
         $return_html=$return_html."<td style=\"padding: 0px;\"><input class=\"w3-input w3-border w3-margin-bottom\" type=\"text\" value=\"".$result_set['Budzet']."\" style=\"width:100%;height:100%\" name=\"Budzet\" > </td>";
////// Datum uplate
         if($result_set['Uplata_date'] != null and $result_set['Uplata_date']!=""){
          $return_html=$return_html."<td style=\"padding: 0px;\"><input type=\"date\" class=\"w3-input w3-border w3-margin-bottom\"  value=\"".date("Y-m-d", strtotime($result_set['Uplata_date'])).$del_sufix."\" style=\"height:100%\" name=\"datum_uplate\" ></td>";
        }
        else {
          $return_html=$return_html."<td style=\"padding: 0px;\"><input type=\"date\" class=\"w3-input w3-border w3-margin-bottom\" style=\"height:100%\" value=\"\"  name=\"datum_uplate\" ></td>";
        }
////// Potrazuje
        $storno_prefix_minus = "";
        if($result_set['Type'] == "Storno"){
          $storno_prefix_minus="-";
        }
        $return_html=$return_html."<td style=\"padding: 0px;\"><input class=\"w3-input w3-border w3-margin-bottom\" type=\"text\" value=\"".$storno_prefix_minus.$result_set['Uplata_iznos']."\" style=\"width:100%;height:100%".$del_style."\" name=\"Potrazuje\" ></td>";
////// Napomena
         $return_html=$return_html."<td style=\"padding: 0px;\"><input class=\"w3-input w3-border w3-margin-bottom\" type=\"text\" value=\"".$result_set['Napomena']."\" style=\"width:100%;height:100%\" name=\"Napomena\" ></td>";
////// Komentar
          // $return_html=$return_html."<td><input class=\"w3-input w3-border w3-margin-bottom\" type=\"text\" value=\"".$del_prefix.$result_set['Komentar'].$del_sufix."\" style=\"width:100px\" name=\"Komentar\" ></td>";
////// Brisanje
           // if($result_set['Status'] != "Deleted") {
           // $return_html=$return_html."<form action=\"./storno.php\" method=\"post\">";
           // $return_html=$return_html."<input type=\"hidden\"  type=\"text\"  name=\"delete_order_id\" value=\"".$result_set['Id']."\"> ";   
           // $return_html=$return_html."<input type=\"hidden\" type=\"text\"  name=\"process\" value=\"Delete\"> ";
         if($result_set['Status'] != "Deleted"){
           $return_html=$return_html."<td style=\"padding: 0px;\"><input type=\"submit\" class=\"btn btn-danger\" value=\"Izmijeni\" style=\"width:100%\"></td> ";         
            $return_html=$return_html."</td>";
          }                 
          $return_html=$return_html."</form>"; 

         if($result_set['Status'] != "Deleted" ) {
             $return_html=$return_html."<td style=\"padding: 0px;\"><form action=\"./storno.php\" method=\"post\">";
             $return_html=$return_html."<input type=\"hidden\"  type=\"text\"  name=\"delete_order_id\" value=\"".$result_set['Id']."\"> ";   
             $return_html=$return_html."<input type=\"submit\" class=\"btn btn-danger\"  name=\"process\" value=\"Delete\" style=\"float:right\" > ";
             $return_html=$return_html."</form>";
             $return_html=$return_html."</td>";
          }      
          $return_html=$return_html."</tr>";
        
        // $return_html=$return_html."<td>".$del_prefix.$storno_prefix.$result_set['Uplata_iznos'].$del_sufix."</td>";
        // $return_html=$return_html."<td>".$del_prefix.$result_set['Napomena'].$del_sufix."</td>";
        // $return_html=$return_html."<td>".$del_prefix.$result_set['Komentar'].$del_sufix."</td>";
        // $return_html=$return_html."<td style=\"border-left: 2px solid black;border-right: 2px solid black;font-size:6px\">";
       
              
      }     
      echo $return_html;
      // return true;
    }
    else {      
      echo "Nema rezultata...";
      // return false;// "No results..."
    }
  }

  public static function get_list_of_customer_zaduzenja($cutomer_id,$modul_id){

    $dbhost=Configuration::$dbInfo['dbhost'];
    $dbuser=Configuration::$dbInfo['dbuser'];
    $dbpass=Configuration::$dbInfo['dbpass'];
    $dbname=Configuration::$dbInfo['dbname'];
    $connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
    mysqli_set_charset($connection,"utf8");

    $return_html="";
    $sql_query = "select * from orders where Customer_id = ".$cutomer_id." and Modul_id = ".$modul_id." and Status = 'Otvoren' and Type in ('Zaduzenje','Pocetno_stanje') order by Id asc";      
    $result_data = mysqli_query($connection, $sql_query) or die("database error:". mysqli_error($this->connection));
    
    if(mysqli_num_rows($result_data)) { 
      $i=1;
      while($result_set = mysqli_fetch_assoc($result_data)) {                            
        
        $return_html=$return_html."<tr>";
        $return_html=$return_html."<td>".$i."</td>";
        $return_html=$return_html."<td>".$result_set['Ugovor_no']."</td>";
        $return_html=$return_html."<td>".($result_set['Broj_rata_uplaceno']+1)."</td>";
        $return_html=$return_html."<td>".$result_set['Modul_opis']."</td>";

        $return_html=$return_html."<td>".$result_set['Zaduzenje_iznos']."E</td>";
        $return_html=$return_html."<td>".($result_set['Zaduzenje_iznos'] - $result_set['Zaduzenje_uplaceno'])."E</td>";        
       
        $komentar = "Uplata odjednom";
        $new_valute_date = $result_set['Ugovor_valuta_date'];
        $Broj_rata = $result_set['Broj_rata_uplaceno'];
        $datum_prve_rate = $result_set['Prva_rata_valuta_date'];
        $datum_pocetka_rata = $result_set['Pocetak_redovnih_rata'];

        if($Broj_rata != null){
          $komentar = "Rata broj:". ($Broj_rata + 1) ;
          if($Broj_rata == 0){
            if($datum_prve_rate != null){
              $datum_valute = $datum_prve_rate;
            }
            $datum_valute = $datum_pocetka_rata;
          }
          else {
            $datum_valute =  date('Y-m-d', strtotime("+".($Broj_rata)." months", strtotime($datum_pocetka_rata)));
            if($datum_prve_rate != null) {
            $new_valute_date =  date('Y-m-d', strtotime("+".($Broj_rata-1)." months", strtotime($datum_pocetka_rata)));
            }
          }
        }

        $return_html=$return_html."<td>".$komentar."</td>";
        $return_html=$return_html."<td>".$result_set['Status']."</td>";
        $return_html=$return_html."<td>".$new_valute_date."</td>";

        $return_html=$return_html."<td><a><button type=\"button\" class=\"btn btn-primary\" onclick=\"odaberi_zaduzenje(".$result_set['Id'].",'".$result_set['Ugovor_no']."');\">Odaberi</button></a></td>";
        $return_html=$return_html."</tr>";

        $i++;
      }     
      echo $return_html;
      // return true;
    }
    else {      
      echo "Nema rezultata...";
      // return false;// "No results..."
    }
  }



  public static function get_report_html($Pocetak,$Kraj){
      $dbhost=Configuration::$dbInfo['dbhost'];
      $dbuser=Configuration::$dbInfo['dbuser'];
      $dbpass=Configuration::$dbInfo['dbpass'];
      $dbname=Configuration::$dbInfo['dbname'];
      $connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
      mysqli_set_charset($connection,"utf8");
      if (mysqli_connect_error($connection)){
        throw new Exception("Problem sa konekcijom nad bazom: ".mysqli_connect_errno($connection).". Molimo kontaktirajte administratora portala.");    
      }
      if (($Pocetak == null) || ($Kraj == null)){
        $filtar = "";
      }
      else {
        $filtar = " ";
      }
      $sql = " select * from orders ".$filtar. "order by Id desc";
      $results = mysqli_query($connection, $sql);
      $brojac=0;
      $result='';
      $return_html="";

       $return_html=$return_html. "<thead>" ;
            $return_html=$return_html."<tr  style=\"border: 3px solid black;\">";
            $return_html=$return_html."<th>#</th>";
            $return_html=$return_html."<th>Ugovor_no:</th>";
            $return_html=$return_html."<th>Prva_rata_iznos:</th>";
            $return_html=$return_html."<th>Prva_rata_valuta_date:</th>";
            $return_html=$return_html."<th>Status:</th>";
            $return_html=$return_html. "<th><a href=\"pdf/pdf.php?report=1\"><button type=\"button\" class=\"btn btn-primary\">PDF Export</button></a></th>";

            $return_html=$return_html. "<th style=\"width:50px\"></th>" ;
            $return_html=$return_html. "</tr>" ;
            $return_html=$return_html. "</thead>" ; 
            $return_html=$return_html. "<tbody>" ;   
           

      if (is_array($results) || is_object($results)) {
        foreach($results as $result) {
          $brojac++;
            $return_html=$return_html. "<tr>";

            $return_html=$return_html."<td>".$brojac."</td>"; 
            $return_html=$return_html."<td>".$result['Ugovor_no']."</td>";
            $return_html=$return_html."<td>".$result['Prva_rata_iznos']."</td>";
            $return_html=$return_html."<td>".$result['Prva_rata_valuta_date']."</td>";
            $return_html=$return_html."<td>".$result['Status']."</td>";

            
            $return_html=$return_html. "</tr>";
            $return_html=$return_html. "<tbody>";
          
          
        }
      }
    return $return_html;
    } 


}
class customer{
	public $Id;
 	public $Name; 
 	public $Responsible_person; 
 	public $JMBG;   
 	public $PIB;  
 	public $PDV_broj;   
  	public $Address;
 	public $Status;
 	public $Created_date;
	public $Zaduzenje;
	public $Uplate;
	public $Saldo;
  public $Email;
  public $Telefon;
 	private $connection;

 	public function __construct($Id){
  		$this->Id=$Id;
  		$this->db_connection();
  		$this->get_customer_info($Id);  
 	}

 	public function db_connection(){
  		$dbhost=Configuration::$dbInfo['dbhost'];
		$dbuser=Configuration::$dbInfo['dbuser'];
		$dbpass=Configuration::$dbInfo['dbpass'];
		$dbname=Configuration::$dbInfo['dbname'];
  		$this->connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
      mysqli_set_charset($this->connection,"utf8");
  		if (mysqli_connect_error($this->connection)){
   			die("Connection fail: ".mysqli_connect_errno($this->connection)." (".mysqli_connect_error($this->connection).")");
  		}
 	}

    public function get_customer_info($Id){
    try{
      $sql= "select * from customers  where Id = ".$Id." " ; 
      //$sql= "select Name, Responsible_person, JMBG, PIB, PDV_broj, Address, Status, Created_date, Zaduzenje, Uplate, Saldo, Email, Telefon from customers  where Id = ".$Id." ";
      $result_data = mysqli_query($this->connection, $sql);
      if($result_data){
        if(mysqli_num_rows($result_data)){
          while($list = mysqli_fetch_assoc($result_data)){
            $this->Name = $list['Name'];
              $this->Responsible_person = $list['Responsible_person'];
              $this->JMBG = $list['JMBG'];
              $this->PIB = $list['PIB'];
              $this->PDV_broj = $list['PDV_broj'];
              $this->Address = $list['Address'];
              $this->Status = $list['Status'];
              $this->Created_date = $list['Created_date'];
              $this->Zaduzenje = $list['Zaduzenje'];
              $this->Uplate = $list['Uplate'];
              $this->Saldo = $list['Saldo'];
              $this->Email = $list['Email'];
              $this->Telefon = $list['Telefon'];
            }
        }
        else{
        throw new Exception("Potrebno je provjeriti ispravnost korisnika.");
        }
      }
      else{
      throw new Exception("Neuspjesno preuzimanje korisnickih informacija.");
      }
    }
    catch(Exception $e){
        $_SESSION["message_type"]="error";
        $_SESSION["Message"]=$e->getMessage();
        header('Location: moduls.php');
      }
  }


  public static function get_customer_sum_zaduzenje_for_modul($customer_id,$year,$modul_id){

    $dbhost=Configuration::$dbInfo['dbhost'];
    $dbuser=Configuration::$dbInfo['dbuser'];
    $dbpass=Configuration::$dbInfo['dbpass'];
    $dbname=Configuration::$dbInfo['dbname'];
    $connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
    mysqli_set_charset($connection,"utf8");
    $zaduzenje_suma="";
    $modul_txt = " ";
    if($modul_id != null){
      $modul_txt = " and Modul_id = ".$modul_id;
    }
    $sql_query = "select coalesce(sum(Zaduzenje_iznos),0) sum_zaduzenje from orders where Customer_id=".$customer_id." and type in ('Zaduzenje','Pocetno_stanje') and ".($year)." = YEAR(Ugovor_date) ".$modul_txt." ";      
    $result_data = mysqli_query($connection, $sql_query) or die("database error:". mysqli_error($this->connection));
    
    if(mysqli_num_rows($result_data)) { 
      while($result_set = mysqli_fetch_assoc($result_data)) {            
        $zaduzenje_suma= $result_set['sum_zaduzenje'];        
      }     
      return $zaduzenje_suma;
    }
    else {      
      return "0.00";// 
    }
  }

   public static function get_customer_sum_uplate_for_modul($customer_id,$year,$modul_id){

    $dbhost=Configuration::$dbInfo['dbhost'];
    $dbuser=Configuration::$dbInfo['dbuser'];
    $dbpass=Configuration::$dbInfo['dbpass'];
    $dbname=Configuration::$dbInfo['dbname'];
    $connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
    $connection2=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
    mysqli_set_charset($connection,"utf8");
    mysqli_set_charset($connection2,"utf8");
    if($modul_id != null){
      $modul_txt = " and Modul_id = ".$modul_id;
    }
    $zaduzenje_suma=0;
    $zaduzenje_suma2=0;
    $sql_query = "select coalesce(sum(
    case 
      when type in ('Avans','Pocetno_Avans') then (
                                                  (CASE
                                                      WHEN Zaduzenje_uplaceno IS NULL THEN 0        
                                                      ELSE Zaduzenje_uplaceno
                                                  END) -
                                                  (CASE
                                                      WHEN Zaduzenje_iznos IS NULL THEN 0        
                                                      ELSE Zaduzenje_iznos
                                                  END)
                                                  )
      else Zaduzenje_uplaceno 
      end    
    ),0) sum_zaduzenje_uplaceno from orders where Customer_id=".$customer_id." and type in ('Zaduzenje','Pocetno_stanje') and (".($year)." = YEAR(Uplata_date) or ".($year)." = YEAR(Ugovor_date)) ".$modul_txt." "; 

    $sql_query2 = "select coalesce(sum(
    case 
      when type in ('Avans','Pocetno_Avans') then (
                                                  (CASE
                                                      WHEN Zaduzenje_uplaceno IS NULL THEN 0        
                                                      ELSE Zaduzenje_uplaceno
                                                  END) -
                                                  (CASE
                                                      WHEN Zaduzenje_iznos IS NULL THEN 0        
                                                      ELSE Zaduzenje_iznos
                                                  END)
                                                  )
      else Zaduzenje_uplaceno 
      end    
    ),0) sum_zaduzenje_uplaceno from orders where Customer_id=".$customer_id." and type in ('Avans','Pocetno_Avans') and (".($year)." = YEAR(Uplata_date) or ".($year)." = YEAR(Ugovor_date)) ".$modul_txt." "; 

    $result_data = mysqli_query($connection, $sql_query) or die("database error:". mysqli_error($connection));
    $result_data2 = mysqli_query($connection2, $sql_query2) or die("database error:". mysqli_error($connection2));
    
    if(mysqli_num_rows($result_data)) { 
      while($result_set = mysqli_fetch_assoc($result_data)) {            
        $zaduzenje_suma= $result_set['sum_zaduzenje_uplaceno'];        
      }     
    }   

    if(mysqli_num_rows($result_data2)) { 
      while($result_set2 = mysqli_fetch_assoc($result_data2)) {            
        $zaduzenje_suma2= $result_set2['sum_zaduzenje_uplaceno'];        
      }
      if($zaduzenje_suma2 < 0){
        $zaduzenje_suma2 = 0;
      }      
    }   
    return $zaduzenje_suma + $zaduzenje_suma2;

  }

  public static function get_all_list_of_customers($modul_id,$year){

    $dbhost=Configuration::$dbInfo['dbhost'];
    $dbuser=Configuration::$dbInfo['dbuser'];
    $dbpass=Configuration::$dbInfo['dbpass'];
    $dbname=Configuration::$dbInfo['dbname'];
    $connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
    mysqli_set_charset($connection,"utf8");
    $return_html="";
    $sql_query = "select * from customers order by Id asc";      
    $result_data = mysqli_query($connection, $sql_query) or die("database error:". mysqli_error($this->connection));
    
    if(mysqli_num_rows($result_data)) { 
      $i=0;
      while($result_set = mysqli_fetch_assoc($result_data)) {           
             
        // $customer = new customer($result_set['Customer_id']);
        // echo "customer_id:".$customer->Id;
        $return_html=$return_html."<a href=\"./index5_2.php?customer_id=".$result_set['Id']."&modul_id=".$modul_id."&year=".$year."\" style=\"font-size:13px;font-weight:bold;text-align:left;padding:0px\">".$result_set['Name']."</a>";
       
      }     
      // echo $return_html;
      return $return_html;
    }
    else {      
      // echo "Nema rezultata...";
      return "Nema rezultata...";// "No results..."
    }
  }

 	
 	public function set_zaduzenje($new_zaduzenje){
 		try{
	    	if (is_numeric($new_zaduzenje)){
	    		$new_zaduzenje=(round($new_zaduzenje, 2));
	    		$Zaduzenje = $this->Zaduzenje + ($new_zaduzenje);
	    		$Saldo = $this->Saldo - ($new_zaduzenje);
	    		$sql="update customers set Zaduzenje = '".$Zaduzenje."', Saldo = '".$Saldo."' where id = ".$this->Id." limit 1";
	    		$result_data = mysqli_query($this->connection, $sql);
	    		if($result_data){
    				 return "OK";
  				}
  				else {
    				return "ERROR".(mysqli_error($connection));
  				}	    		
	    	}
	    	else {
	    		throw new Exception("Zaduzenje mora biti u numerickom formatu.");
	    	}
    	}
		catch(Exception $e){
       return "ERROR".$e->getMessage();
		// $_SESSION["message_type"]="error";
		// $_SESSION["Message"]=$e->getMessage();
		// header('Location: index.html');
		}
	}


	public function set_uplata($new_uplata){
 		try{
	    	if (is_numeric($new_uplata)){
	    		$new_uplata=(round($new_uplata, 2));
	    		$Uplate = $this->Uplate + ($new_uplata);
	    		$Saldo = $this->Saldo + ($new_uplata);
	    		$sql="update customers set Uplate = '".$Uplate."', Saldo = '".$Saldo."' where id = ".$this->Id." limit 1";
          echo "SQL:".$sql;
          // mysqli_query($this->connection, $sql) or die(mysqli_error($this->connection)); 
	    		$result_data = mysqli_query($this->connection, $sql);
      		if($result_data){
            return "OK";					
  				}
  				else {
    				return "ERROR".(mysqli_error($connection));
  				}	    		
	    	}
	    	else {
	    		throw new Exception("Uplata mora biti u numerickom formatu.");
	    	}
    	}
		catch(Exception $e){
      return "ERROR".$e->getMessage();
		// $_SESSION["message_type"]="error";
		// $_SESSION["Message"]=$e->getMessage();
		// header('Location: index.html');
		}
	}

	  public static function update_customer($Id,$Name,$Responsible_person,$JMBG,$PIB,$PDV_broj,$Address,$Status,$Zaduzenje,$Uplate,$Saldo,$Email,$Telefon){
  try{
      $dbhost=Configuration::$dbInfo['dbhost'];
      $dbuser=Configuration::$dbInfo['dbuser'];
      $dbpass=Configuration::$dbInfo['dbpass'];
      $dbname=Configuration::$dbInfo['dbname'];
      $connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
      $Zaduzenje=(round($Zaduzenje, 2));
      $Uplate=(round($Uplate, 2));
      $Saldo=(round($Saldo, 2));
      $sql_query=" update `bar`.`customers` set `Name` = '".$Name."',`Responsible_person` = '".$Responsible_person."',`JMBG` = '".$JMBG."',`PIB` = '".$PIB."',`PDV_broj` = '".$PDV_broj."',`Address` = '".$Address."',`Status` = '".$Status."',`Zaduzenje` = ".$Zaduzenje.",`Uplate` = ".$Uplate.",`Saldo` = ".$Saldo.",`Email` = '".$Email."',`Telefon` = '".$Telefon."' where Id=".$Id;
      $result=mysqli_query($connection, $sql_query);
      if ($result && mysqli_affected_rows($connection)>=0){
        return true;
      }
      else{
        throw new Exception("Neuspijesan unos. Potrebno je kontaktirati administratora sajta.");
        return false;
    }
  }
  catch(Exception $e){
    $_SESSION["message_type"]="error";
    $_SESSION["Message"]=$e->getMessage();
    header('Location: update_customer.php?customer_id=".$Id."');
  }
}



    public static function get_customers_list_html($filtar_type,$filtar_value,$html,$modul_id=null){
      $dbhost=Configuration::$dbInfo['dbhost'];
      $dbuser=Configuration::$dbInfo['dbuser'];
      $dbpass=Configuration::$dbInfo['dbpass'];
      $dbname=Configuration::$dbInfo['dbname'];
      $connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
      mysqli_set_charset($connection,"utf8");
      if (mysqli_connect_error($connection)){
        throw new Exception("Problem sa konekcijom nad bazom: ".mysqli_connect_errno($connection).". Molimo kontaktirajte administratora portala.");    
      }
      $modul_filtar = "";
      if($modul_id != null && $modul_id != ""){
        $modul_filtar = " where Id in (select distinct Customer_id from orders where Modul_id = ".$modul_id.") ";
      }
      if (($filtar_type == null) || ($filtar_value == null)){
        $filtar = "";
      }
      else {
        if($modul_filtar != ""){
          $filtar = " and ".$filtar_type." like '%".$filtar_value."%' ";
        }
        else {
          $filtar = " where ".$filtar_type." like '%".$filtar_value."%' "; 
        }
      }
      $sql = " select * from customers  ".$modul_filtar.$filtar. " order by Id desc";
      $results = mysqli_query($connection, $sql);
      $brojac=0;
      $max_value=150;
      $result='';
      $return_html="";
      if (is_array($results) || is_object($results)) {
        foreach($results as $result) {
          $brojac++;
          if ($html=="Main"){
            //$return_html=$return_html. "<div id=\"tabela1\">";     
            $return_html=$return_html. "<tr>";
            $return_html=$return_html."<td>".$brojac."</td>"; 
            $return_html=$return_html."<td>".$result['Name']."</td>";

            $target_year = date("Y");
            $Zaduzenje_sum = customer::get_customer_sum_zaduzenje_for_modul($result['Id'],$target_year,$modul_id);
            $return_html=$return_html."<td>".number_format($Zaduzenje_sum, 2, '.', ',')."</td>";

            $Uplate_sum = customer::get_customer_sum_uplate_for_modul($result['Id'],$target_year,$modul_id);
            $return_html=$return_html."<td>".number_format($Uplate_sum, 2, '.', ',')."</td>";


            $return_html=$return_html."<td>". number_format((0-($Zaduzenje_sum - $Uplate_sum)), 2, '.', ',')."</td>";
            $return_html=$return_html. "<td><a href=\"index2.php?modul_id=".($_GET['modul_id'])."&customer_id=".$result['Id']."\"><button type=\"button\" class=\"btn btn-primary\">Lista zaduzenja klijenta</button></a></td>";
            $return_html=$return_html. "<td><a href=\"update_customer.php?customer_id=".$result['Id']."&modul_id=".$modul_id."\"><button type=\"button\" class=\"btn btn-success\">Izmjena podataka klijenta</button></a></td>";
            $return_html=$return_html. "</tr>";
           // $return_html=$return_html. "</div>";
          }
          else if($html == "Order"){
            // $return_html=$return_html."<div class=\"col-xs-12 col-sm-12 col-md-6 col-lg-6\">";
            $return_html=$return_html."<tr>";
            $return_html=$return_html."<td>".$brojac."</td>";
            $return_html=$return_html."<td>".$result['Name']."</td>";
            $return_html=$return_html."<td>".$result['Zaduzenje']."E</td>";
            $return_html=$return_html."<td>".$result['Uplate']."E</td>";
            $return_html=$return_html."<td>".$result['Saldo']."E</td>";
            $return_html=$return_html."<td><button type=\"button\" class=\"btn btn-primary\" onclick=\"change_customer(".$result['Id'].",'".$result['Name']."');\">Odaberi</button></td>";
            $return_html=$return_html."</tr>";
          } 
            else if($html == "modify_customers"){
            $return_html=$return_html."<div class=\"col-xs-12 col-sm-12 col-md-6 col-lg-6\">";
            $return_html=$return_html."<tr>";
            $return_html=$return_html."<td>".$brojac."</td>";
            $return_html=$return_html."<td>".$result['Name']."</td>";
            //$return_html=$return_html."<td><button type=\"button\" class=\"btn btn-primary\" onclick=\"'location.href=update_customer.php?customer_id=".$result['Id']."';\">Odaberi</button></td>";
            $return_html=$return_html."<td><a class=\"btn btn-primary\" href=\"update_customer.php?customer_id=".$result['Id']."\">Odaberi</a></td>";
            $return_html=$return_html."</tr>";
          }
        }
      }
    return $return_html;
    } 

 public  function get_avans_uplate_from_customer($year,$modul_id){

    $dbhost=Configuration::$dbInfo['dbhost'];
    $dbuser=Configuration::$dbInfo['dbuser'];
    $dbpass=Configuration::$dbInfo['dbpass'];
    $dbname=Configuration::$dbInfo['dbname'];
    $connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
    mysqli_set_charset($connection,"utf8");
    $return_html="";
    $del_sufix="";
    $del_prefix="";
    $sql_query = "select * from orders where  Customer_id = ".$this->Id." and Type in ('Avans','Pocetno_Avans') and '".($year)."' = YEAR(Created_date) and Modul_id = ".$modul_id."  order by Id asc";     
    $result_data = mysqli_query($connection, $sql_query) or die("database error:". mysqli_error($this->connection));    
    if(mysqli_num_rows($result_data)) { 
      $i=0;
      while($result_set = mysqli_fetch_assoc($result_data)) {        
      
        $i++;
        $return_html=$return_html."<form action=\"./uplata_avans.php\" method=\"post\">";
        $return_html=$return_html."<tr>";
        $return_html=$return_html."<input type=\"hidden\"  name=\"process\"  value=\"uplata_update\">";
        $return_html=$return_html."<input type=\"hidden\"  name=\"order_id\"  value=\"".$result_set['Id']."\">";

//// Redni broj
        $return_html=$return_html."<td style=\"border-left: 2px solid black;border-right: 2px solid black\">".$i."</td>";
//// Ugovor_no        
           if($result_set['Ugovor_no'] == null)  {
             $return_html=$return_html."<td><input class=\"w3-input w3-border w3-margin-bottom\" type=\"text\" value=\"\"  style=\"width:100%;height:100%\"  name=\"Ugovor_no\" ></td>";
           }
           else {
              $return_html=$return_html."<td><input class=\"w3-input w3-border w3-margin-bottom\" type=\"text\" value=\"".$del_prefix.$result_set['Ugovor_no'].$del_sufix."\"  style=\"width:100%;height:100%\"  name=\"Ugovor_no\" ></td>";
           }
//// Broj rata       
          $return_html=$return_html."<td><input class=\"w3-input w3-border w3-margin-bottom\" type=\"text\" value=\"".$result_set['Broj_rata']."\"  style=\"width:100%;height:100%\"  name=\"Broj_rata\" ></td>";
        
///// Datum dospijeca
        if($result_set['Ugovor_date'] != null and $result_set['Ugovor_date']!=""){
          $return_html=$return_html."<td style=\"padding: 0px;\"><input type=\"date\" class=\"w3-input w3-border w3-margin-bottom\"  value=\"".date("Y-m-d", strtotime($result_set['Ugovor_date'])).$del_sufix."\" style=\"height:100%\"  name=\"Ugovor_date\" > </td>";
        }
        else {
          $return_html=$return_html."<td style=\"padding: 0px;\"><input type=\"date\" class=\"w3-input w3-border w3-margin-bottom\" style=\"height:100%\" value=\"\"  name=\"Ugovor_date\" ></td>";
        }        
////// Duguje   
          $return_html=$return_html."<td style=\"padding: 0px;\"><input class=\"w3-input w3-border w3-margin-bottom\" type=\"text\" value=\"".$del_prefix.$result_set['Zaduzenje_iznos'].$del_sufix."\"   style=\"width:100%;height:100%\" name=\"Duguje\" ></td>";        
////// Nalog
        $return_html=$return_html."<td style=\"padding: 0px;\"><input class=\"w3-input w3-border w3-margin-bottom\" type=\"text\" value=\"".$del_prefix.$result_set['Nalog'].$del_sufix."\" style=\"width:100%;height:100%\"  name=\"Nalog\" ></td>";
////// Izvod      
        $return_html=$return_html."<td style=\"padding: 0px;\"><input class=\"w3-input w3-border w3-margin-bottom\" type=\"text\" value=\"".$del_prefix.$result_set['Bank_account'].$del_sufix."\" style=\"width:100%;height:100%\" name=\"Izvod\" > </td>";
////// Budzet
         $return_html=$return_html."<td style=\"padding: 0px;\"><input class=\"w3-input w3-border w3-margin-bottom\" type=\"text\" value=\"".$del_prefix.$result_set['Budzet'].$del_sufix."\" style=\"width:100%;height:100%\" name=\"Budzet\" > </td>";
////// Datum uplate
         if($result_set['Uplata_date'] != null and $result_set['Uplata_date']!=""){
          $return_html=$return_html."<td style=\"padding: 0px;\"><input type=\"date\" class=\"w3-input w3-border w3-margin-bottom\"  value=\"".date("Y-m-d", strtotime($result_set['Uplata_date'])).$del_sufix."\" style=\"height:100%\" name=\"datum_uplate\" ></td>";
        }
        else {
          $return_html=$return_html."<td style=\"padding: 0px;\"><input type=\"date\" class=\"w3-input w3-border w3-margin-bottom\" style=\"height:100%\" value=\"\"  name=\"datum_uplate\" ></td>";
        }
////// Potrazuje
        $storno_prefix_minus = "";
        if($result_set['Type'] == "Storno"){
          $storno_prefix_minus="-";
        }
        $return_html=$return_html."<td style=\"padding: 0px;\"><input class=\"w3-input w3-border w3-margin-bottom\" type=\"text\" value=\"".$del_prefix.$storno_prefix_minus.$result_set['Zaduzenje_uplaceno'].$del_sufix."\" style=\"width:100%;height:100%\" name=\"Potrazuje\" ></td>";
////// Saldo
        $zaduzenje_iznos = $result_set['Zaduzenje_iznos'] == null ? "0.00" : $result_set['Zaduzenje_iznos'];
        $uplata_iznos = $result_set['Zaduzenje_uplaceno'] == null ? "0.00" : $result_set['Zaduzenje_uplaceno'];
        // $uplata_iznos = $result_set['Uplata_iznos'];
         $return_html=$return_html."<td style=\"padding: 0px;\"><input class=\"w3-input w3-border w3-margin-bottom\" type=\"text\" value=\"".(0-($zaduzenje_iznos-($uplata_iznos)))."\" style=\"width:100%;height:100%\" name=\"Napomena\" ></td>";
////// Komentar
          $return_html=$return_html."<td style=\"width:350px\"><input class=\"w3-input w3-border w3-margin-bottom\" type=\"text\" value=\"".$del_prefix.$result_set['Napomena'].$del_sufix."\" style=\"width:100%\" name=\"Napomena\" ></td>";
////// Brisanje
           // if($result_set['Status'] != "Deleted") {
           // $return_html=$return_html."<form action=\"./storno.php\" method=\"post\">";
           // $return_html=$return_html."<input type=\"hidden\"  type=\"text\"  name=\"delete_order_id\" value=\"".$result_set['Id']."\"> ";   
           // $return_html=$return_html."<input type=\"hidden\" type=\"text\"  name=\"process\" value=\"Delete\"> ";
           $return_html=$return_html."<td style=\"padding: 0px;\"><input type=\"submit\" class=\"btn btn-danger\" value=\"Izmijeni\" style=\"width:100%\"></td> ";
           // $return_html=$return_html."</form>";
        // }      
        
        // $return_html=$return_html."<td>".$del_prefix.$storno_prefix.$result_set['Uplata_iznos'].$del_sufix."</td>";
        // $return_html=$return_html."<td>".$del_prefix.$result_set['Napomena'].$del_sufix."</td>";
        // $return_html=$return_html."<td>".$del_prefix.$result_set['Komentar'].$del_sufix."</td>";
        // $return_html=$return_html."<td style=\"border-left: 2px solid black;border-right: 2px solid black;font-size:6px\">";
       
        $return_html=$return_html."</td>";
        $return_html=$return_html."</tr>";       
        $return_html=$return_html."</form>";       
      }     
      echo $return_html;
      // return true;
    }
    else {      
      echo "Nema rezultata...";
      // return false;// "No results..."
    }
  }

  public static function get_customer_sum_avans_duguje_for_modul($customer_id,$year,$modul){
    // echo "modul_id:".$modul;
    $dbhost=Configuration::$dbInfo['dbhost'];
    $dbuser=Configuration::$dbInfo['dbuser'];
    $dbpass=Configuration::$dbInfo['dbpass'];
    $dbname=Configuration::$dbInfo['dbname'];
    $connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
    mysqli_set_charset($connection,"utf8");
    $zaduzenje_suma="";
    $sql_query = "select sum(Zaduzenje_iznos) sum_zaduzenje from orders where Customer_id=".$customer_id." and Type in ('Avans','Pocetno_Avans') and '".($year)."' = YEAR(Created_date) and Modul_id=".$modul; 
    // echo $sql_query;     
    $result_data = mysqli_query($connection, $sql_query) or die("database error:". mysqli_error($connection));
    
    if(mysqli_num_rows($result_data)) { 
      while($result_set = mysqli_fetch_assoc($result_data)) {            
        $zaduzenje_suma=  $result_set['sum_zaduzenje'];        
      }     
      // echo $zaduzenje_suma;
      return $zaduzenje_suma;
    }
    else { 
      // echo "0.00";
      return "0.00";// 
    }
  }

  public static function get_customer_sum_avans_uplate_for_modul($customer_id,$year,$modul_id){

    $dbhost=Configuration::$dbInfo['dbhost'];
    $dbuser=Configuration::$dbInfo['dbuser'];
    $dbpass=Configuration::$dbInfo['dbpass'];
    $dbname=Configuration::$dbInfo['dbname'];
    $connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
    mysqli_set_charset($connection,"utf8");
    $zaduzenje_suma="";
    $sql_query = "select sum(Zaduzenje_uplaceno) sum_uplata from orders where Customer_id=".$customer_id." and type in ('Avans','Pocetno_Avans') and ".($year)." = YEAR(Created_date) and Modul_id = ".$modul_id;      
    $result_data = mysqli_query($connection, $sql_query) or die("database error:". mysqli_error($this->connection));
    
    if(mysqli_num_rows($result_data)) { 
      while($result_set = mysqli_fetch_assoc($result_data)) {            
        $zaduzenje_suma=  $result_set['sum_uplata'];        
      }     
      return $zaduzenje_suma;
    }
    else {      
      return "0.00";// 
    }
  }


}


class atribut{
  public $Id;
  public $Type; 
  public $Name; 
  private $connection;

  public function __construct($Id){
      $this->Id=$Id;
      $this->db_connection();
      $this->get_atribut_info($this->Id);  
  }

  public function db_connection(){
    $dbhost=Configuration::$dbInfo['dbhost'];
    $dbuser=Configuration::$dbInfo['dbuser'];
    $dbpass=Configuration::$dbInfo['dbpass'];
    $dbname=Configuration::$dbInfo['dbname'];
      $this->connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
      mysqli_set_charset($this->connection,"utf8");
      if (mysqli_connect_error($this->connection)){
        die("Connection fail: ".mysqli_connect_errno($this->connection)." (".mysqli_connect_error($this->connection).")");
      }
  }

  public function get_atribut_info($Id){
    try{
      $sql= "select * from atributi  where Id = ".$Id; 
      $result_data = mysqli_query($this->connection, $sql);
      if($result_data){
        if(mysqli_num_rows($result_data)){
          while($list = mysqli_fetch_assoc($result_data)){
            $this->Type = $list['Type'];
            $this->Name = $list['Name'];
          }
        }
        else{
          throw new Exception("Potrebno je provjeriti ispravnost modula.");
        }
      }
      else{
        throw new Exception("Neuspjesno preuzimanje informacija vezanih za moduo.");
      }
    }
    catch(Exception $e){
        $_SESSION["message_type"]="error";
        $_SESSION["Message"]=$e->getMessage();
        return false;
      }
  }

  
    public static function get_all_atributi_html(){
      $dbhost=Configuration::$dbInfo['dbhost'];
      $dbuser=Configuration::$dbInfo['dbuser'];
      $dbpass=Configuration::$dbInfo['dbpass'];
      $dbname=Configuration::$dbInfo['dbname'];
      $connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
      mysqli_set_charset($connection,"utf8");
      if (mysqli_connect_error($connection)){
        throw new Exception("Problem sa konekcijom nad bazom: ".mysqli_connect_errno($connection).". Molimo kontaktirajte administratora portala.");    
      }
      $indikator=0;
      $brojac = 1;
      $return_html = "";
      $delay_count= 0;
      for ($n=0; $n<21; $n=$n+3){
        $m=$n+2;
        // $return_html=$return_html."<div id=\"about\" class=\"container-fluid\" style=\"padding-top: 20px; display: flex; justify-content: center \">";
      $sql = " select * from atributi where Id between '{$n}' and '{$m}' ";
      $results = mysqli_query($connection, $sql);
      $result='';
      
      if (is_array($results) || is_object($results)) {
        
        foreach($results as $result) {

          if( $brojac == 1){
            if($indikator == 0){
              $indikator = 1;
              $return_html=$return_html."<div class=\"animate__animated animate__bounceInRight animate__delay-".$delay_count."s\" style=\"width:98%;;margin-left: 20px;height: 150px;padding-bottom: 10px;margin-top: 100px;\">";
            }
            else {
              $return_html=$return_html."<div class=\"animate__animated animate__bounceInRight animate__delay-".$delay_count."s\" style=\"width:98%;margin-left: 20px;height: 150px;padding-bottom: 10px;margin-top: 10px;\">";
            }
            $delay_count++;
            // echo "string:".$delay_count; 
          }

          $return_html=$return_html."<div class=\"card\" style=\"width: 15%; border: 1px solid black;padding: 10px; float: left;margin-left: 1%;height: 120px;font-size: 13px;text-align: left;margin-top: 10px;box-shadow:5px 10px #888888 \" onclick=\"myFunction(".$result['Id'].")\">";
          $return_html=$return_html."<div class=\"card-body\" style=\" height:100px;\">";    
          $return_html=$return_html."<p class=\"card-text\" style=\"font-weight: bold;display: flex;justify-content : center; align-items :center; color:black; height :90px\">".$result['Name']."</p>";
          $return_html=$return_html."</div>";
          $return_html=$return_html."</div>";

          $brojac = $brojac + 1;
          // echo "Brojac: ".$brojac;
          if($brojac == 7){
            // echo "Usao u END DIV";
            $brojac = 1;
            $return_html=$return_html."</div>";
          }
          
           // $return_html=$return_html."<div id=\"about\" class=\"container-fluid\" style=\"padding-top: 20px; display: flex; justify-content: center \">";
            // $return_html=$return_html."<button style=\"font-size:22px\">".$result['Name']."<i class=\"material-icons\"  style=\"font-size:28px;color:darkslategrey\">create_new_folder</i></button>";
            // $return_html=$return_html."<div class=\"divider\"/></div>";
            // $return_html=$return_html."</div>";
        }
      }
    }
    return $return_html;
    } 

     public static function get_atributs_for_orders(){
      $dbhost=Configuration::$dbInfo['dbhost'];
      $dbuser=Configuration::$dbInfo['dbuser'];
      $dbpass=Configuration::$dbInfo['dbpass'];
      $dbname=Configuration::$dbInfo['dbname'];
      $connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
      mysqli_set_charset($connection,"utf8");
      if (mysqli_connect_error($connection)){
        throw new Exception("Problem sa konekcijom nad bazom: ".mysqli_connect_errno($connection).". Molimo kontaktirajte administratora portala.");  
      }      
      $sql = " select * from atributi order by Id asc";
      $results = mysqli_query($connection, $sql);      
      $return_html="";
      $i=1;
      if (is_array($results) || is_object($results)) {
        foreach($results as $result) {                
          $return_html=$return_html. "<tr>";
          $return_html=$return_html. "<td>".$i."</td>";
          $return_html=$return_html. "<td>".$result['Name']."</td>";              
          $return_html=$return_html. "<td><a><button type=\"button\" class=\"btn btn-primary\" onclick=\"odaberi_modul(".$result['Id'].",'".$result['Name']."');\">Odaberi</button></a></td>";
          $return_html=$return_html. "</tr>";  
          $i++;         
        }
      }
    return $return_html;
    } 

    

}

?>