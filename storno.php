<?php 
require_once('private/classes.php');

$process =  $_POST['process'] != '' ? $_POST['process'] : null; 

if($process == "storniranje"){

	$order_id =  $_POST['storno_order_id'] != '' ? $_POST['storno_order_id'] : null; 
	$storno_iznos =  $_POST['storno_iznos'] != '' ? $_POST['storno_iznos'] : null; 
	$storno_dokument =  $_POST['storno_dokument'] != '' ? $_POST['storno_dokument'] : null; 
	$order = new order($order_id);
	$customer = new customer($order->Customer_id);

	$result_id = insert_order($order->Customer_id,$storno_dokument ,"Storno",$order->Modul_id,$order->Modul_opis,$order->Ugovor_no.";Storno","null","null","null","null","null","null","null","null","null","null",$storno_iznos,$order->Id,date("Y-m-d"),"null","Storniranje iznos ".$storno_iznos,"","");
	if(!(strpos($result_id, 'ERROR') !== false)){
		$storno = new order($result_id);
		$result2 = $order->minus_uplata($storno, $customer);
		echo "Result2:".$result2;
		if($result2 == "OK"){
			$new_customer = new customer($order->Customer_id);
			$insert_id =  insert_transakcije($storno->Type,$storno->Customer_id,$storno->Id,$storno->Uplata_date,$storno->Uplata_iznos ,$customer->Zaduzenje,$customer->Uplate,$customer->Saldo,$new_customer->Zaduzenje,$new_customer->Uplate,$new_customer->Saldo);
			if(!(strpos($insert_id, 'ERROR') !== false)){
				header("Location: index5.php?order_id=".$order_id);
			}
			else {
				echo "Greska kod inserta transakcije :Pozovite administratora: ".$insert_id;
			}
		}
		else {
			echo "Greska kod updejta zaduzenja :Pozovite administratora: ".$result2;
		}
	}
	else {
		echo "Greska kod inserta ordera :Pozovite administratora: ERROR: ".$result_id;
	}
}
else if($process == "Delete"){

	$delete_order_id =  $_POST['delete_order_id'] != '' ? $_POST['delete_order_id'] : null;
	$order = new order($delete_order_id);
	if($order->Type == "Uplata"){
		$order->Uplata_iznos = 0-($order->Uplata_iznos);
	}	
	$order->Komentar = ";Delete ".$order->Komentar;
	$customer = new customer($order->Customer_id);
	$result = $order->set_new_order_state("Deleted");
	if(!(strpos($result, 'ERROR') !== false)){
		if($order->Type != "Info"){
			$zaduzenje = new order($order->Uplata_poziv_na_broj);
			$result2 = $zaduzenje->add_uplata($order, $customer);
			if($result2 == "OK"){
				$new_customer = new customer($order->Customer_id);
				$insert_id =  insert_transakcije("Deleted",$order->Customer_id,$order->Id,$order->Uplata_date,$order->Uplata_iznos ,$customer->Zaduzenje,$customer->Uplate,$customer->Saldo,$new_customer->Zaduzenje,$new_customer->Uplate,$new_customer->Saldo);
				if(!(strpos($insert_id, 'ERROR') !== false)){
					header("Location: index5.php?order_id=".$order->Uplata_poziv_na_broj);
				}
				else {
					echo "Greska kod inserta transakcije :Pozovite administratora: ".$insert_id;
				}
			}
			else {
				echo "Greska kod updejta zaduzenja :Pozovite administratora: ERROR: ".$result;
			}
		}
		else {
			header("Location: index5.php?order_id=".$order->Uplata_poziv_na_broj);
		}
	}	
	else {
		echo "Greska kod promjene status ordera :Pozovite administratora: ERROR: ".$result;
	}
}

?>