<?php 
  require_once("private/classes.php");
  $customer = new customer($_GET['customer_id']);

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com -->
  <title>Modifikovanje korisnickih podataka</title>
  <!-- <meta charset="utf-8"> -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="live_css/bootstrap.min.css">
  <link href="live_css/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="live_css/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="live_css/jquery.min.js"></script>
  <script src="live_css/bootstrap.min.js"></script>
  <style>
  body {
    font: 400 15px Lato, sans-serif;
    line-height: 1.8;
    color: #818181;
  }
  h2 {
    font-size: 24px;
    text-transform: uppercase;
    color: #303030;
    font-weight: 600;
    margin-bottom: 30px;
  }
  h4 {
    font-size: 19px;
    line-height: 1.375em;
    color: #303030;
    font-weight: 400;
    margin-bottom: 30px;
  }  
  .jumbotron {
    background-color: #f4511e;
    color: #fff;
    padding: 100px 25px;
    font-family: Montserrat, sans-serif;
  }
  .container-fluid {
    padding: 60px 50px;
  }
  .bg-grey {
    background-color: #f6f6f6;
  }
  .logo-small {
    color: #f4511e;
    font-size: 50px;
  }
  .logo {
    color: #f4511e;
    font-size: 200px;
  }
  .thumbnail {
    padding: 0 0 15px 0;
    border: none;
    border-radius: 0;
  }
  .thumbnail img {
    width: 100%;
    height: 100%;
    margin-bottom: 10px;
  }
  .carousel-control.right, .carousel-control.left {
    background-image: none;
    color: #f4511e;
  }
  .carousel-indicators li {
    border-color: #f4511e;
  }
  .carousel-indicators li.active {
    background-color: #f4511e;
  }
  .item h4 {
    font-size: 19px;
    line-height: 1.375em;
    font-weight: 400;
    font-style: italic;
    margin: 70px 0;
  }
  .item span {
    font-style: normal;
  }
  .panel {
    border: 1px solid #f4511e; 
    border-radius:0 !important;
    transition: box-shadow 0.5s;
  }
  .panel:hover {
    box-shadow: 5px 0px 40px rgba(0,0,0, .2);
  }
  .panel-footer .btn:hover {
    border: 1px solid #f4511e;
    background-color: #fff !important;
    color: #f4511e;
  }
  .panel-heading {
    color: #fff !important;
    background-color: #f4511e !important;
    padding: 25px;
    border-bottom: 1px solid transparent;
    border-top-left-radius: 0px;
    border-top-right-radius: 0px;
    border-bottom-left-radius: 0px;
    border-bottom-right-radius: 0px;
  }
  .panel-footer {
    background-color: white !important;
  }
  .panel-footer h3 {
    font-size: 32px;
  }
  .panel-footer h4 {
    color: #aaa;
    font-size: 14px;
  }
  .panel-footer .btn {
    margin: 15px 0;
    background-color: #f4511e;
    color: #fff;
  }
  .navbar {
    margin-bottom: 0;
    background-color: #f4511e;
    z-index: 9999;
    border: 0;
    font-size: 12px !important;
    line-height: 1.42857143 !important;
    letter-spacing: 4px;
    border-radius: 0;
    font-family: Montserrat, sans-serif;
  }
  .navbar li a, .navbar .navbar-brand {
    color: #fff !important;
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
    color: #f4511e !important;
    background-color: #fff !important;
  }
  .navbar-default .navbar-toggle {
    border-color: transparent;
    color: #fff !important;
  }
  footer .glyphicon {
    font-size: 20px;
    margin-bottom: 20px;
    color: #f4511e;
  }
  .slideanim {visibility:hidden;}
  .slide {
    animation-name: slide;
    -webkit-animation-name: slide;
    animation-duration: 1s;
    -webkit-animation-duration: 1s;
    visibility: visible;
  }
  @keyframes slide {
    0% {
      opacity: 0;
      transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      transform: translateY(0%);
    }
  }
  @-webkit-keyframes slide {
    0% {
      opacity: 0;
      -webkit-transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      -webkit-transform: translateY(0%);
    }
  }
  @media screen and (max-width: 768px) {
    .col-sm-4 {
      text-align: center;
      margin: 25px 0;
    }
    .btn-lg {
      width: 100%;
      margin-bottom: 35px;
    }
  }
  @media screen and (max-width: 480px) {
    .logo {
      font-size: 150px;
    }
  }
  td ,th ,tr {
    text-align: center;
    font-weight: bold;
    font-size: 15px;
    height: 12px;
  }
  </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60" style="background-color: grey">

<nav class="navbar navbar-default navbar-fixed-top" style="background-color: darkslategrey">
  <div class="container" style="width: 100%;margin-left: 0px;padding: 15px;">
    <div class="navbar-header" style="width:50%">
      <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button> -->
      
      <img src="logo.jpg" style="height:60px;float: left;">
      <div style="width:80%;float: left;margin-left: 10px;">
        <br>
        <a class="navbar-brand" style="margin-left: 10px;float: none;width: 100%;font-size: 22px;">Promjena korisnickih informacija: </a>
        
      </div>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="moduls.php">Moduli</a></li>
        <li><a href="insert_customer.php">Novi klijent</a></li>
        
        <li><a href="logout.php">LogOut</a></li>
        <!-- <li><a href="#pricing">PRICING</a></li> -->
        <!-- <li><a href="#contact">CONTACT</a></li> -->
      </ul>
    </div>
  </div>
</nav>
<div style="background-color: grey;">
  <div style="width:100%;">
  </div>
</div>

<div class="jumbotron text-center" style="background-color: grey;height: 650px;">
    <p>  <?php 
                                            if(isset($_SESSION["Message"])) {
                                                echo $_SESSION["Message"];
                                                $_SESSION["Message"]="";
                                                
                                            }
                                        ?></p>
  <div style="width:60%;height: 650px;float: left;">
    <form method="post" action="insert.php" style="width: 100%">
    <input type="hidden" id="update_customer" name="update_customer" value="update_customer" >
    <input type="hidden" id="Id" name="Id" value="<?php echo $customer->Id; ?>" >
    
    <div style="float:left; width:100%; ">

        <div style="width:60%;float: left;">
          <br><br><br>

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 14px;padding: 5px;">
                   <label style="margin-left:0px"><b>Ime korisnika: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black; font-size: 16px;">
                   <input class="w3-input w3-border w3-margin-bottom" type="text" value="<?php echo $customer->Name; ?>" name="Name" id="Name" style="width:100%">    
              </div>  
            </div>
            
            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 14px;padding: 5px;">
                   <label style="margin-left:0px"><b>Ime odgovorne osobe: </b></label>
              </div>

              <div style="width: 60%;float:left;color: black; font-size: 16px;">
                   <input class="w3-input w3-border w3-margin-bottom" type="text" value="<?php echo $customer->Responsible_person; ?>" name="Responsible_person" id="Responsible_person"  style="width:100%" >    
              </div>    
            </div>
            
            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 14px;padding: 5px;">
                   <label style="margin-left:0px"><b>Email: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black; font-size: 16px;">
                   <input class="w3-input w3-border w3-margin-bottom" type="email" value="<?php echo $customer->Email; ?>" name="Email" id="Email"  style="width:100%" > 
                   
              </div>    
            </div>

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 14px;padding: 5px;">
                   <label style="margin-left:0px"><b>Telefon: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black; font-size: 16px;">
                    <input class="w3-input w3-border w3-margin-bottom" type="text" value="<?php echo $customer->Telefon; ?>" name="Telefon" id="Telefon"  style="width:100%" >       
              </div>    
            </div>

             <div style="width:100%;border:0px solid black;float:left;">
               <div style="width: 40%;float:left;font-size: 14px;padding: 5px;">
                   <label style="margin-left:0px"><b>Maticni broj: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black;">
                 <input class="w3-input w3-border w3-margin-bottom" type="text" value="<?php echo $customer->JMBG; ?>" name="JMBG" id="JMBG"  style="width:100%" >
              </div>    
            </div>

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 14px;padding: 5px;">
                   <label style="margin-left:0px"><b>PIB: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black;">
                 <input class="w3-input w3-border w3-margin-bottom" type="text" value="<?php echo $customer->PIB; ?>" name="PIB" id="PIB" style="width:100%" >
              </div>    
            </div>

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 14px;padding: 5px;">
                   <label style="margin-left:0px"><b>PDV reg broj: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black;">
                 <input class="w3-input w3-border w3-margin-bottom" type="text" value="<?php echo $customer->PDV_broj; ?>"  name="PDV_broj" id="PDV_broj" style="width:100%" >
              </div>    
            </div>

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 14px;padding: 5px;">
                   <label style="margin-left:0px"><b>Adresa: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black;">
                 <input class="w3-input w3-border w3-margin-bottom" type="text" value="<?php echo $customer->Address; ?>" name="Address" id="Address"  style="width:100%" >
              </div>    
            </div>

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 14px;padding: 0px;">
                   <label style="margin-left:0px"><b>Status korisnika: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black; font-size: 16px;">
                   <select class="w3-input w3-border w3-margin-bottom" type="text"   name="Status" id="Status" style="width:100%; " >
                    <?php if($customer->Status == "active") { ?>
                    <option value="active">Aktivan</option>
                    <option value="suspend">Suspendovan</option>
                  <?php }
                    else { ?>
                      <option value="suspend">Suspendovan</option>
                      <option value="active">Aktivan</option>
                    <?php }  ?>
                  </select>    
              </div>    
            </div>


        </div>

        <div style="width:40%;float: right;">
            <br><br><br>
          <div style="width:100%;float:right;">

            <div style="width:100%;border:0px solid black;float:right;">
              <div style="width: 40%;float:left;font-size: 14px;padding: 0px;">
                   <label style="margin-left:0px"><b>Zaduzenje: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black; font-size: 16px;">
                   <input class="w3-input w3-border w3-margin-bottom" type="number" step="0.01" value="<?php echo $customer->Zaduzenje; ?>" id="Zaduzenje" name="Zaduzenje" style="width:100%" >    
              </div>    
            </div>

            <div style="width:100%;border:0px solid black;float:right;">
                <div style="width: 40%;float:left;font-size: 14px;padding: 0px;">
                   <label style="margin-left:0px"><b>Uplate: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black; font-size: 16px;">
                   <input class="w3-input w3-border w3-margin-bottom" type="number" step="0.01" value="<?php echo $customer->Uplate; ?>" id="Uplate" name="Uplate" style="width:100%" >    
              </div>   
            </div>

            <div style="width:100%;border:0px solid black;float:right;">
             <div style="width: 40%;float:left;font-size: 14px;padding: 0px;">
                   <label style="margin-left:0px"><b>Saldo: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black; font-size: 16px;">
                   <input class="w3-input w3-border w3-margin-bottom" type="number" step="0.01" value="<?php echo $customer->Saldo; ?>" id="Saldo" name="Saldo" style="width:100%" >    
              </div>     
            </div>

            <div style="width:100%;border:0px solid black;float:right;margin-top: 0px; height: 60px;">              
              <div style="width:100%;">
                <button type="submit" value="submit" id="submit" name="submit" class="button" style="width:100%;background-color:blue; height: 55px;"> Update korisnika</button>
              </div>  
            </div>

           

          </div>

           


 

        </div>
        </div>

      </form>
  

      </div>



  </div>

</div>






</body>
</html>
