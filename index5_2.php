<?php 
  require_once("private/classes.php");

  // $order =  isset($_GET['customer_id']) ? (new customer($_GET['customer_id'])) : null;
  $customer = isset($_GET['customer_id']) ? (new customer($_GET['customer_id'])) : null;
  $modul = new atribut($_GET['modul_id']);
  $year_selected = isset($_GET['year']) ? $_GET['year'] : null;
  $year = date("Y");
  if($year_selected != null){
    $year=$year_selected;
  }

  // echo "id:".$modul->Id;
  // echo "Name:".$modul->Name;
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com -->
  <title>Uplate </title>
  <!-- <link rel="stylesheet" href="live_css/w3.css"> -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="live_css/bootstrap.min.css">
  <link href="live_css/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="live_css/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="live_css/jquery.min.js"></script>
  <script src="live_css/bootstrap.min.js"></script>

  
  <style>
  body {
    font: 400 15px Lato, sans-serif;
    line-height: 1.8;
    color: #818181;
  }
  h2 {
    font-size: 24px;
    text-transform: uppercase;
    color: #303030;
    font-weight: 600;
    margin-bottom: 30px;
  }
  h4 {
    font-size: 19px;
    line-height: 1.375em;
    color: #303030;
    font-weight: 400;
    margin-bottom: 30px;
  }  
  .jumbotron {
    background-color: #f4511e;
    color: #fff;
    padding: 100px 25px;
    font-family: Montserrat, sans-serif;
  }
  .container-fluid {
    padding: 60px 50px;
  }
  .bg-grey {
    background-color: #f6f6f6;
  }
  .logo-small {
    color: #f4511e;
    font-size: 50px;
  }
  .logo {
    color: #f4511e;
    font-size: 200px;
  }
  .thumbnail {
    padding: 0 0 15px 0;
    border: none;
    border-radius: 0;
  }
  .thumbnail img {
    width: 100%;
    height: 100%;
    margin-bottom: 10px;
  }
  .carousel-control.right, .carousel-control.left {
    background-image: none;
    color: #f4511e;
  }
  .carousel-indicators li {
    border-color: #f4511e;
  }
  .carousel-indicators li.active {
    background-color: #f4511e;
  }
  .item h4 {
    font-size: 19px;
    line-height: 1.375em;
    font-weight: 400;
    font-style: italic;
    margin: 70px 0;
  }
  .item span {
    font-style: normal;
  }
  .panel {
    border: 1px solid #f4511e; 
    border-radius:0 !important;
    transition: box-shadow 0.5s;
  }
  .panel:hover {
    box-shadow: 5px 0px 40px rgba(0,0,0, .2);
  }
  .panel-footer .btn:hover {
    border: 1px solid #f4511e;
    background-color: #fff !important;
    color: #f4511e;
  }
  .panel-heading {
    color: #fff !important;
    background-color: #f4511e !important;
    padding: 25px;
    border-bottom: 1px solid transparent;
    border-top-left-radius: 0px;
    border-top-right-radius: 0px;
    border-bottom-left-radius: 0px;
    border-bottom-right-radius: 0px;
  }
  .panel-footer {
    background-color: white !important;
  }
  .panel-footer h3 {
    font-size: 32px;
  }
  .panel-footer h4 {
    color: #aaa;
    font-size: 14px;
  }
  .panel-footer .btn {
    margin: 15px 0;
    background-color: #f4511e;
    color: #fff;
  }
  .navbar {
    margin-bottom: 0;
    background-color: #f4511e;
    z-index: 9999;
    border: 0;
    font-size: 12px !important;
    line-height: 1.42857143 !important;
    letter-spacing: 4px;
    border-radius: 0;
    font-family: Montserrat, sans-serif;
  }
  .navbar li a, .navbar .navbar-brand {
    color: #fff !important;
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
    color: #f4511e !important;
    background-color: #fff !important;
  }
  .navbar-default .navbar-toggle {
    border-color: transparent;
    color: #fff !important;
  }
  footer .glyphicon {
    font-size: 20px;
    margin-bottom: 20px;
    color: #f4511e;
  }
  .slideanim {visibility:hidden;}
  .slide {
    animation-name: slide;
    -webkit-animation-name: slide;
    animation-duration: 1s;
    -webkit-animation-duration: 1s;
    visibility: visible;
  }
  @keyframes slide {
    0% {
      opacity: 0;
      transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      transform: translateY(0%);
    }
  }
  @-webkit-keyframes slide {
    0% {
      opacity: 0;
      -webkit-transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      -webkit-transform: translateY(0%);
    }
  }
  @media screen and (max-width: 768px) {
    .col-sm-4 {
      text-align: center;
      margin: 25px 0;
    }
    .btn-lg {
      width: 100%;
      margin-bottom: 35px;
    }
  }
  @media screen and (max-width: 480px) {
    .logo {
      font-size: 150px;
    }
  }
  td ,th ,tr {
    text-align: left;
    font-weight: bold;
    font-size: 15px;
    height: 12px;
    width: 50px;
  }
  td {
    padding: 0px;
  }

  #id02 #id01 {
    @include 'live_css/w3.css';
  }

/* Dropdown Button */
.dropbtn {
  background-color: #04AA6D;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
  cursor: pointer;
}

/* Dropdown button on hover & focus */
.dropbtn:hover, .dropbtn:focus {
  background-color: #3e8e41;
}

/* The search field */
#myInput {
  box-sizing: border-box;
  /*background-image: url('searchicon.png');*/
  background-position: 14px 12px;
  background-repeat: no-repeat;
  font-size: 16px;
  padding: 14px 20px 12px 45px;
  border: none;
  border-bottom: 1px solid #ddd;
}

/* The search field when it gets focus/clicked on */
#myInput:focus {outline: 3px solid #ddd;}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  position: relative;
  display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f6f6f6;
  min-width: 230px;
  border: 1px solid #ddd;
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #f1f1f1}

/* Show the dropdown menu (use JS to add this class to the .dropdown-content container when the user clicks on the dropdown button) */
.show {display:block;}

  </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<nav class="navbar navbar-default navbar-fixed-top" style="background-color: darkslategrey">
  <div class="container" style="width: 100%;margin-left: 0px;padding: 15px;">
    <div class="navbar-header" style="width:50%">
      <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button> -->
      
      <img src="logo.jpg" style="height:60px;float: left;">
      <div style="width:80%;float: left;margin-left: 10px;">
        <a class="navbar-brand" style="margin-left: 10px;float: none;width: 100%;font-size: 13px;">Modul: <?php echo $modul->Name; ?> </a>
        <input id="modul_id" type="hidden" value="<?php echo $modul->Id;  ?>" > 
      </div>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="index.php?modul_id=<?php echo $modul->Id; ?>">Nazad</a></li>       
        <li><a href="reports.php">Izvjestaji</a></li>
        <li><a href="logout.php">LogOut</a></li>
        <!-- <li><a href="#pricing">PRICING</a></li> -->
        <!-- <li><a href="#contact">CONTACT</a></li> -->
      </ul>
    </div>
  </div>
</nav>

<div class="jumbotron text-center" style="background-color: grey;padding-bottom: 10px;">
  <div style="width:100%;height: 160px;">

    <div class="dropdown" style="width:30%;float:right;">
        <button onclick="myFunction()" class="dropbtn" style="width: 100%">Pronadji klijenta</button>
        <div id="myDropdown" class="dropdown-content" style = "width:100%" >
          <input type="text" placeholder="..." id="myInput" onkeyup="filterFunction()" style="color:black;width: 100%;">
          <?php  echo customer::get_all_list_of_customers($modul->Id,$year); ?>                  
        </div>
        <div style="width:100%;">
         <button style="width:100%;height:50px;margin-top: 20px;color: black" <?php if($customer != null) { echo "onClick=\"redirect_to_print();\""; } ?> >Stampaj</button>
       </div>

        <div style="width:100%;border:0px solid black;float:left;">            
            <div style="width: 40%;float:right;color: black;">
                 <input class="w3-input w3-border w3-margin-bottom" type="text" value="" id="broj_ugovora_print"  name="broj_ugovora_print" style="width:100%" >    
            </div>    
            <div style="width: 20%;float:right;font-size: 10px;padding: 5px;">
                 <label style="margin-left:0px"><b>Broj ugovora: </b></label>
            </div>
            <select class="w3-input w3-border w3-margin-bottom"  style="width:20%;color:black;float: left;" id="yearSelect" onchange="selectYear()" >
                   <?php  echo order::get_years_of_orders($year,"avans"); ?>
            </select>  
        </div>
    </div>

      

      <div style="width: 30%;float: left;">     
              
          <script type="text/javascript">

            function selectYear() {
              var year = document.getElementById("yearSelect").value;
              var customer_text="";
              <?php if($customer!=null) { echo "customer_text='&customer_id=".$customer->Id."';"; } ?>
              // alert(year);
              window.open('./index5_2.php?modul_id=<?php echo $modul->Id; ?>&year='+year+customer_text,'_blank');
            }

            function redirect_to_print(){
              
              temp_text= '<?php if($customer != null) { echo  "customer_id=".$customer->Id."&modul_id=".$modul->Id."&year=".$year; } ?>';
              text_ugovor=document.getElementById('broj_ugovora_print').value;
              if(text_ugovor != ""){
                temp_text=temp_text+'&ugovor_id='+text_ugovor;
              }
              // alert("text:"+temp_text);
              window.open('./test/kartica2.php?'+temp_text,'_blank');
            }

            /* When the user clicks on the button,
              toggle between hiding and showing the dropdown content */
              function myFunction() {
                document.getElementById("myDropdown").classList.toggle("show");
              }

              function filterFunction() {
                var input, filter, ul, li, a, i;
                input = document.getElementById("myInput");
                filter = input.value.toUpperCase();
                div = document.getElementById("myDropdown");
                a = div.getElementsByTagName("a");
                for (i = 0; i < a.length; i++) {
                  txtValue = a[i].textContent || a[i].innerText;
                  if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    a[i].style.display = "";
                  } else {
                    a[i].style.display = "none";
                  }
                }
              }
            
            function redirect_to_order(order_id){
              alert('klinio'+order_id);
              window.location.replace("./index5.php?order_id="+order_id);
            }

          </script>

          <div style="width:100%;">
            <h1 style="font-size: 40px;float: left;font-style: italic;margin-top: 0px;"><?php if($customer != null) { echo $customer->Name; }?></h1> 
          </div>

          <p style="font-size: 12px;float: left;width: 100%;text-align: left;margin-bottom: 0px;">PIB/PDV_broj:<?php if($customer != null) { echo $customer->PIB;} ?> &nbsp; <?php if($customer != null) { echo $customer->PDV_broj;} ?></p></br>
          <p style="font-size: 12px;float: left;width: 100%;text-align: left;margin-bottom: 0px">JMBG: &nbsp;<?php if($customer != null) { echo $customer->JMBG; } ?> &nbsp;</p></br>
          <p style="font-size: 12px;float: left;width: 100%;text-align: left;margin-bottom: 0px">Adresa:&nbsp;<?php if($customer != null) { echo $customer->Address; } ?> &nbsp;</p> </br>
          <p style="font-size: 12px;float: left;width: 100%;text-align: left;margin-bottom: 0px">Email:&nbsp;<?php if($customer != null) { echo $customer->Email; } ?>  &nbsp;</p> </br>
          <p style="font-size: 12px;float: left;width: 100%;text-align: left;margin-bottom: 0px">Tel:&nbsp;<?php if($customer != null) { echo $customer->Telefon; } ?>  &nbsp;</p> </br>

        </div>
     
  </div>

  <!-- <form> -->
    <!-- <form> -->
    <div id="id01" class="w3-modal" style="display:none">
      <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:1000px">

        <div class="w3-center"><br>
          <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal" style="background-color:red">&times;</span>
          <!-- <img src="img_avatar4.png" alt="Avatar" style="width:30%" class="w3-circle w3-margin-top"> -->
        </div>

        
          <div class="input-group">

            <div class="dropdown" style="margin-left: 5%;">
              <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown" style="float: left;">Filtar
              <span class="caret"></span></button>
              <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                <li role="presentation"><a role="menuitem" tabindex="-1" onclick="filtar_set('PIB');">PIB</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" onclick="filtar_set('PDV_broj');">PDV broj</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" onclick="filtar_set('Name');">Naziv klijenta</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" onclick="filtar_set('JMBG');">JMBG</a></li>
                <!-- <li role="presentation" class="divider"></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ostalo...</a></li> -->
              </ul>
            </div>
            <script type="text/javascript">

              var filtar = "";
              function filtar_set(new_filtar){
                filtar = new_filtar;
              }

              function get_customers_filtared_list(){
                var filtar_value = document.getElementById('search_customer_input').value;
                var xhttp; 
                xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                  if (this.readyState == 4 && this.status == 200) {
                    document.getElementById("customers_filtared_list").innerHTML = this.responseText;
                  }          
                };             
                xhttp.open("GET", "get_orders_configuration_AJAX.php?filtar="+filtar+"&filtar_value=" + filtar_value +"&process=customers", true);
                xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xhttp.send( );
              }

            </script>

            <input id="search_customer_input" type="email" class="form-control" size="50" placeholder="Unesite željenu riječ ..." required style="width: 50%;margin-left: 20xp;">
            <div class="input-group-btn" style="float: left;">
              <button type="button" class="btn btn-danger" onclick="get_customers_filtared_list();">Pretraži</button>
            </div>
          </div>
  <!-- </form> -->
        <div id="about" class="container-fluid" style="padding-top: 0px;">
        <table class="table table-bordered" style="color:black">
          <thead>
            <tr>
              <th>#</th>
              <th>Kompanija:</th>
              <th>Zaduzenje:</th>
              <th>Uplate:</th>
              <th>Saldo:</th>
              <th style="width:50px"></th>
              <!-- <th>Dugovanje u valuti:</th> -->
            </tr>
          </thead>
          <tbody id="customers_filtared_list">
            
          </tbody>
        </table>
      </div>

      <script type="text/javascript">

             

      </script>

      </div>
    </div>

    <div id="id02" class="w3-modal" style="display:none">
      <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="width:1200px">

        <div class="w3-center"><br>
          <span onclick="document.getElementById('id02').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal" style="background-color:red">&times;</span>
        </div>        
        <div id="about" class="container-fluid" style="padding-top: 20px;">
        <table class="table table-bordered" style="color:black">
          <thead>
            <tr>
              <th>#</th>
              <th>Broj ugovora:</th>
              <th>Rata:</th>
              <th>Modul:</th>
              <th>Zaduzenje:</th>
              <th>Saldo:</th>
              <th>Komentar:</th>
              <th>Status:</th>
              <th>Datum valute:</th>
              <th style="width:50px"></th>
              <!-- <th>Dugovanje u valuti:</th> -->
            </tr>
          </thead>
          <tbody id="lista_zaduzenja">
                                            
          </tbody>
        </table>        

      </div>
      </div>
    </div>
    
  <!-- </form> -->

  <script type="text/javascript">
    
   

  </script>

  
</div>

<!-- Container (About Section) -->
<div id="about" class="container-fluid" style="padding-top: 0px;">
 

  <table class="table table-bordered">
    <thead>
      <tr style="border: 3px solid black;">

        <th style="border-right: 2px solid black">#</th>
        <th>Broj rijesenja</th>
        <th>Br. rata</th>
        <th style="width:50px">Datum prispeća</th>        
        <th style="width:100px">Duguje</th>
        <th>Nalog Br.</th>
        <th>Izvod Br.</th>  
        <th>Budzet</th>
        
        <th>Datum uplate</th>
        <th>Potrazuje</th>
        <th >Saldo</th>
        <th >Napomena</th>
        <th></th>        
        <!-- <th>Brisanje</th>         -->

      </tr>
    </thead>
    <tbody>
      <?php  if($customer != null) { echo $customer->get_avans_uplate_from_customer($year,$modul->Id); } ?>

<form action="./uplata_avans.php" method="post">
      <tr>
        <input type="hidden"  name="process"  value="uplata_insert">
        <input type="hidden"  name="customer_id"  value="<?php if($customer !=null) { echo $customer->Id; } ?>" >
        <input type="hidden"  name="modul_id"  value="<?php if($_GET['modul_id'] !=null) { echo $_GET['modul_id']; } ?>" >
        <td style="border-left: 2px solid black;border-right: 2px solid black"></td>
        <td>
           <input class="w3-input w3-border w3-margin-bottom" type="text" value=""  style="width:100%;height:100%"  name="Ugovor_no" >
        </td>
        <td>
          <input class="w3-input w3-border w3-margin-bottom" type="text" value="" style="width:100%;height:100%"  name="Broj_rata" >
        </td>
        <td style="padding: 0px;">
          <input type="date" class="w3-input w3-border w3-margin-bottom"  style="height:100%" name="Ugovor_date" > 
        </td>
        <td style="padding: 0px;">
          <input class="w3-input w3-border w3-margin-bottom" type="text"  style="width:100%;height:100%" name="Duguje">
        </td>
        <td style="padding: 0px;">
          <input class="w3-input w3-border w3-margin-bottom" type="text" style="width:100%;height:100%" name="Nalog">
        </td>
        <td style="padding: 0px;">
          <input class="w3-input w3-border w3-margin-bottom" type="text"  style="width:100%;height:100%" name="Izvod"> 
        </td>
        <td style="padding: 0px;">
          <input class="w3-input w3-border w3-margin-bottom" type="text"  style="width:100%;height:100%" name="Budzet">
        </td>
        <td style="padding: 0px;">
          <input type="date" class="w3-input w3-border w3-margin-bottom"  style="height:100%" name="datum_uplate">
        </td>
        <td style="padding: 0px;">
          <input class="w3-input w3-border w3-margin-bottom" type="text" value="0.00" style="width:100%;height:100%" name="Potrazuje">
        </td>
        <td style="padding: 0px;">
          <input class="w3-input w3-border w3-margin-bottom" type="text"  style="width:100%;height:100%" readonly>
        </td>
        <td style="padding: 0px;">
          <input class="w3-input w3-border w3-margin-bottom" type="text"  style="width:100%;height:100%" name="Napomena">
        </td>
        <td style="padding: 0px;">
          <input type="submit" class="btn btn-danger" value="Unesi">
        </td>
     </tr>
 </form>
      
    </tbody>
  </table>
  <div style="width:100%;border: 1px solid black;">

    <div style="width:100%;padding: 15px;border: 1px solid black;height: 70px;">
      <div style="width:30%;border:1px solid black;float: left; text-align: center;">
         <label><b>Zaduzenje:</b></label>
         <input style="color: blue;" class="w3-input w3-border w3-margin-bottom" type="text"  name="Zaduzenje" value="<?php if($customer != null) { echo customer::get_customer_sum_avans_duguje_for_modul($customer->Id,$year,$_GET['modul_id']); }  ?>" >        
      </div>

      <div style="width:30%;border:1px solid black;float:left;margin-left: 5%; text-align: center;">
         <label><b>Uplate:</b></label>
        <input style="color: green;" class="w3-input w3-border w3-margin-bottom" type="text"  name="Uplate" value="<?php if($customer != null) { echo customer::get_customer_sum_avans_uplate_for_modul($customer->Id,$year,$_GET['modul_id']); }  ?>">        
      </div>

      <div style="width:30%;border:1px solid black;float:right; text-align: center;">
         <label><b>Saldo:</b></label>
         <input style="color: red;" class="w3-input w3-border w3-margin-bottom" type="text"  name="Saldo" value="<?php if($customer != null) { echo 
          (0- 
          ((customer::get_customer_sum_avans_duguje_for_modul($customer->Id,$year,$_GET['modul_id']) - 
          (customer::get_customer_sum_avans_uplate_for_modul($customer->Id,$year,$_GET['modul_id'])
          )))); }  ?>" >        
      </div>
    </div>

  </div>

</div>



</body>
</html>
