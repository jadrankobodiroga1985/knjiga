<?php 
//require_once('private/functions.php');
require_once('private/classes.php');
// check_session();
// var_dump($_POST)	;
try{
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
    	if(isset($_POST["insert_customer"])){
    		//echo "Usao u insert_customer";
			$Name = $_POST['Name'];
			$Responsible_person = $_POST['Responsible_person'];
			$JMBG = $_POST['JMBG'];
			$PIB = $_POST['PIB'];
			$PDV_broj = $_POST['PDV_broj'];
			$Address = $_POST['Address'];
			$Status = $_POST['Status'];
			$Zaduzenje = $_POST['Zaduzenje'];
			$Uplate = $_POST['Uplate'];
			$Saldo = $_POST['Saldo'];
			$Email = $_POST['Email'];
			$Telefon = $_POST['Telefon'];
			$result = insert_customer($Name,$Responsible_person,$JMBG,$PIB,$PDV_broj,$Address,$Status,$Zaduzenje,$Uplate,$Saldo,$Email,$Telefon);
			if($result){
				$_SESSION["Message"] = "Uspjesno unesene korisnicke informacije";
				header("Location: insert_customer.php");
			}
			else {
				$_SESSION["Message"] = "Unos korisnickih informacije nije uspijesan.";
				header("Location: insert_customer.php");
			}
    	}
    	if(isset($_POST["update_customer"])){
    		$Id = $_POST['Id'];
			$Name = $_POST['Name'];
			$Responsible_person = $_POST['Responsible_person'];
			$JMBG = $_POST['JMBG'];
			$PIB = $_POST['PIB'];
			$PDV_broj = $_POST['PDV_broj'];
			$Address = $_POST['Address'];
			$Status = $_POST['Status'];
			$Zaduzenje = $_POST['Zaduzenje'];
			$Uplate = $_POST['Uplate'];
			$Saldo = $_POST['Saldo'];
			$Email = $_POST['Email'];
			$Telefon = $_POST['Telefon'];
			$result = customer::update_customer($Id,$Name,$Responsible_person,$JMBG,$PIB,$PDV_broj,$Address,$Status,$Zaduzenje,$Uplate,$Saldo,$Email,$Telefon);
			if($result){
				$_SESSION["Message"] = "Uspjesno unesene korisnicke informacije";
				header("Location: update_customer.php?customer_id=".$Id."");
			}
			else {
				$_SESSION["Message"] = "Unos korisnickih informacije nije uspijesan.";
				header("Location: update_customer.php?customer_id=".$Id."");
			}
    	}
    	if(isset($_POST["update_order"])=="update_order"){
    		echo "Usao u update_order";
    		$Id = $_POST['order_id'];
			$Type = $_POST['tip'];
			$Ugovor_no = $_POST['broj_ugovora'];
			$Ugovor_date = $_POST['datum_ugovora'];
			$Ugovor_valuta_date = $_POST['datum_kraja_ugovora'];
			$Zaduzenje_iznos = $_POST['iznos_zaduzenje'];
			$Broj_rata = $_POST['broj_rata'];
			$Iznos_jedne_rate = $_POST['iznos_jedne_rate'];
			$Pocetak_redovnih_rata = $_POST['datum_pocetka_rata'];
			$Prva_rata_iznos = $_POST['iznos_prve_rate'];
			$Prva_rata_valuta_date = $_POST['datum_prve_rate'];
			$Zaduzenje_uplaceno = $_POST['uplaceno'];
			$Broj_rata_uplaceno = $_POST['broj_rata_uplacen'];
			$Status = $_POST['status'];
			$Napomena = $_POST['Napomena'];
			$Nalog = $_POST['Nalog'];

			$order = new order($Id);
			$customer = new customer($order->Customer_id);
			if($order->Zaduzenje_iznos != $Zaduzenje_iznos) {
				$customer->set_zaduzenje($order->Zaduzenje_iznos - $Zaduzenje_iznos);
			}
			if($order->Zaduzenje_uplaceno != $Zaduzenje_uplaceno) {
				$customer->set_uplata($order->Zaduzenje_uplaceno - $Zaduzenje_uplaceno);
			}

			$result = update_order($Id,$Type,$Ugovor_no,$Ugovor_date,$Ugovor_valuta_date,$Zaduzenje_iznos,$Broj_rata,$Iznos_jedne_rate,$Pocetak_redovnih_rata,$Prva_rata_iznos,$Prva_rata_valuta_date,$Zaduzenje_uplaceno,$Broj_rata_uplaceno,$Status,$Napomena,$Nalog);
			if($result){
				$_SESSION["message"] = "Uspjesno modifikovane infomacije o modulu.";
				header("Location: index5.php?order_id=".$_POST['order_id']); // Ovdje cemo morati dodati da se vrati na stranicu gdje je to zaduzenje u pitanju...
			}
			else {
				$_SESSION["message"] = "Neuspijesan update informacija o modulu.";
				header("Location: index5.php?order_id=".$_POST['order_id']);
			}
    	} 		 	
	}
}
catch(Exception $e){
		$_SESSION["message_type"]="error";
		$_SESSION["Message"]=$e->getMessage();
		header("Location: index5.php?order_id=".$_POST['order_id']);
}

// var_dump($_SESSION);

?>