<?Php
/////////////////////////////////////////// Spisak svih obveznika //////////////////////////////////

// Opis:
//Izvještaj koji bi sadržao sve obveznike u bazi (po određenom modulu) sa kolonama Duguje Potražuje Saldo.
//Ova vrsta izvještaj se koristi kako bismo mogli da dobijemo podatak npr. Sa stanjem na dan 25.08. spisak
//obveznika sa zaduženjima i uplatama za lokalnu komunalnu taksu je takav i takav. Na kraju i red u kome bi se vidjelo i zbirno stanje.

// http://localhost:3272/Knjiga/test/izvjestaj2.php?modul_id=1&datum_do=2021-08-01&datum_od=2020-01-01&customer_id=2

require('tfpdf.php');
require_once('../private/classes.php');

$modul_id = isset($_GET['modul_id']) ? $_GET['modul_id'] : null;
$datum_od = isset($_GET['datum_od']) ? $_GET['datum_od'] : null;
$datum_do = isset($_GET['datum_do']) ? $_GET['datum_do'] : null;
$customer_id = isset($_GET['customer_id']) ? $_GET['customer_id'] : null;

$modul_id_text = " ";
if($modul_id != null){
	$modul_id_text = " and Modul_id=".$modul_id." ";
}

$customer_id_text = " ";
if($customer_id != null){
	$customer_id_text = " and Customer_id=".$customer_id." ";
}

$target_year = date("Y");

$datum_do_text = "";
if($datum_do != null && $datum_do !=""){
	$datum_do_text = " and ('".$datum_do."' >= Ugovor_date or '".$datum_do."' >= Uplata_date )" ;
	$datum_do_year = DateTime::createFromFormat("Y-m-d", $datum_do);
	if( $datum_do_year->format("Y") != $target_year ) {
		$target_year = $datum_do_year->format("Y");
	}
}

$datum_od_text = "";
if($datum_od != null && $datum_od !=""){
	$datum_od_text = " and ('".$datum_od."' <= Ugovor_date or '".$datum_od."' <= Uplata_date )";
	$datum_od_year = DateTime::createFromFormat("Y-m-d", $datum_od);
	if($datum_do != null && $datum_do !=""){
		$datum_do_year = DateTime::createFromFormat("Y-m-d", $datum_do);
		// var_dump($datum_od_year->format("Y"));
		// var_dump($datum_do_year->format("Y"));
		if( $datum_od_year->format("Y") != $datum_do_year->format("Y")) {
			$datum_od_text = " and '".$datum_do_year->format('Y')."-01-01' <= Ugovor_date ";
		}
	}
	else {
		if( $datum_od_year->format("Y") != $target_year ) {
			$target_year = $datum_od_year->format("Y");
		}
	}	
}

$pdf = new tFPDF(); 
$pdf->AddPage();
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu-Bold','','DejaVuSansCondensed-Bold.ttf',true);
$pdf->SetFont('Arial','B',12);
$width=$pdf->GetPageWidth(); // Width of Current Page
$height=$pdf->GetPageHeight(); // Height of Current Page
$width_cell=array(10,20,30,20,20,20,20,20);

$pdf->Image('logo.png',10,6,30);
// Arial bold 15
$pdf->SetFont('Arial','B',8);
// Move to the right
$pdf->Cell(50);
// Title
$modul_title_text = "za sve module ";
if($modul_id != null and $modul_id!=""){
	$modul = new atribut($modul_id);
	$modul_title_text = "samo za modul:".$modul->Name." ";
}

$customer_title_text = "za sve klijente ";
if($customer_id != null and $customer_id!=""){
	$cust = new customer($customer_id);
	$customer_title_text = "samo za klijenta:".$cust->Name." ";
}

$Datum_Od_title_text = " ";
if($datum_od != null and $datum_od!=""){	
	$Datum_Od_title_text = "Datum Od :".$datum_od." ";
}

$Datum_Do_title_text = " ";
if($datum_do != null and $datum_do!=""){	
	$Datum_Do_title_text = "Datum Do :".$datum_do." ";
}

$title ='Izvjestaj svih klijenata o zaduzenjima i uplatama '.$modul_title_text. $customer_title_text.$Datum_Od_title_text.$Datum_Do_title_text; 
// $title = iconv('UTF-8', 'CP1250//TRANSLIT', $title);
// $text = preg_replace("#[^a-zA-z]#", "", $text);
$pdf->MultiCell(100,5,$title);
// $pdf->Cell(100,10,'Izvještaj: Naziv Izvještaja',1,0,'C');
// Line break
$pdf->Ln(10);
$y = $pdf->GetY();
$pdf -> Line(20, $y , $width-20, $y); 

$temp_sum_duguje = "0.00";
$temp_sum_potrazuje = "0.00";
$temp_sum_saldo = "0.00";
$dbhost=Configuration::$dbInfo['dbhost'];
$dbuser=Configuration::$dbInfo['dbuser'];
$dbpass=Configuration::$dbInfo['dbpass'];
$dbname=Configuration::$dbInfo['dbname'];
$connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);

// $output="";  
mysqli_set_charset($connection,"utf8");
// mysqli_set_charset($connection2,"utf8");
if (mysqli_connect_error($connection)){
throw new Exception("Problem sa konekcijom nad bazom: ".mysqli_connect_errno($connection).". Molimo kontaktirajte administratora portala.");  
}      
$sql = " select Customer_id,(select Name from bar.customers where Id=Customer_id) Name,
 		coalesce(sum(Zaduzenje_iznos),0) Duguje, coalesce(sum(Zaduzenje_uplaceno),0) Potrazuje,
 		0-(coalesce(sum(Zaduzenje_iznos),0) - coalesce(sum(Zaduzenje_uplaceno),0)) Saldo  from bar.orders where Type in ( 'Zaduzenje','Pocetno_stanje')  and (".$target_year." = YEAR(Ugovor_date) or ".$target_year." = YEAR(Uplata_date) ) ". $modul_id_text . $datum_do_text . $datum_od_text . $customer_id_text . " group by Customer_id;";


// echo "string:".$sql;
// die();
$results = mysqli_query($connection, $sql); 
if(mysqli_num_rows($results)) {


		$y = $pdf->GetY();
  	$pdf -> Line(20, $y , $width-20, $y);  
  	$pdf->Ln(1);
  	$y = $pdf->GetY();
  	$pdf -> Line(20, $y , $width-20, $y);  
  	$pdf->Ln(2);

		$pdf -> SetX(20);
		$pdf->SetFillColor(255,255,255);
		// $pdf->SetFont('Arial','B',5);
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(10,3,'',0,0,'C',true); // First header column 
		$pdf -> SetX(30);
		$pdf->Cell(100,3,'Korisnik lokacije:',0,0,'L',true); // First header column 
		$pdf -> SetX(130);
		$pdf->Cell(20,3,'Duguje',0,0,'R',true); // Second header column
		$pdf -> SetX(150);
		$pdf->Cell(20,3,'Potrazuje',0,0,'R',true); // Second header column
		$pdf -> SetX(170);
		$pdf->Cell(20,3,'Saldo',0,0,'R',true); // Second header column
		$pdf->Ln(4);
		$y = $pdf->GetY();
  	$pdf -> Line(20, $y , $width-20, $y);  
  	$pdf->Ln(2);
  	$i=0;
      while($list = mysqli_fetch_assoc($results)) {    
      	$i++;
      	$pdf -> SetX(20);
		$pdf->SetFillColor(255,255,255);
		// $pdf->SetFont('Arial','B',5);
		// $pdf->SetFont('Arial','B',8);
		// $pdf -> SetX(25);
		$pdf->Cell(10,3,$i.'.',0,0,'C',true); // First header column 
		$pdf -> SetX(30);
		$pdf->SetFont('DejaVu-Bold','',8);
		$pdf->Cell(100,3,$list['Name'],0,0,'L',true); // First header column 
		$pdf -> SetX(130);

		$sql2 = " select coalesce(sum(
	    case 
	      when type in ('Avans','Pocetno_Avans') then (
	                                                  (CASE
	                                                      WHEN Zaduzenje_uplaceno IS NULL THEN 0        
	                                                      ELSE Zaduzenje_uplaceno
	                                                  END) -
	                                                  (CASE
	                                                      WHEN Zaduzenje_iznos IS NULL THEN 0        
	                                                      ELSE Zaduzenje_iznos
	                                                  END)
	                                                  )
	      else Zaduzenje_uplaceno 
	      end    
	    ),0) sum_zaduzenje_uplaceno  
			from bar.orders where Type in ('Avans','Pocetno_Avans')  and (".$target_year." = YEAR(Ugovor_date) or ".$target_year." = YEAR(Uplata_date) ) and Customer_id =  ".$list['Customer_id']. $modul_id_text . $datum_do_text . $datum_od_text  . " group by Customer_id;";
		$avans_uplata = 0;

		$connection2=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
		$results2 = mysqli_query($connection2, $sql2); 
		if(mysqli_num_rows($results2)) {
			while($list2 = mysqli_fetch_assoc($results2)) {  
				$avans_uplata= $list2['sum_zaduzenje_uplaceno'];  
			}
			if($avans_uplata < 0){
				$avans_uplata = 0;
			}
		}
		$connection2->close();

		$duguje_iznos = $list['Duguje'] == null ? "0.00" : $list['Duguje'];
		$temp_sum_duguje = $temp_sum_duguje + ($duguje_iznos);

		$potrazuje_iznos = $list['Potrazuje'] == null ? "0.00" : ($list['Potrazuje'] + ($avans_uplata)) ;
		$temp_sum_potrazuje = $temp_sum_potrazuje + ($potrazuje_iznos);

		$saldo_iznos = ($potrazuje_iznos) - ($duguje_iznos);
		$temp_sum_saldo = $temp_sum_saldo + ($saldo_iznos);

		$pdf->Cell(20,3,number_format($duguje_iznos, 2, '.', ','),0,0,'R',true); // Second header column
		$pdf -> SetX(150);
		$pdf->Cell(20,3,number_format($potrazuje_iznos, 2, '.', ','),0,0,'R',true); // Second header column
		$pdf -> SetX(170);
		$pdf->Cell(20,3,number_format($saldo_iznos, 2, '.', ','),0,0,'R',true); // Second header column

		

		$pdf->Ln(4);
		$y = $pdf->GetY();
	  	$pdf -> Line(20, $y , $width-20, $y);  
	  	$pdf->Ln(2);
	  }	

    	// $pdf->Ln(1);
	  	$y = $pdf->GetY();
	  	$pdf -> Line(20, $y , $width-20, $y);  
	  	$pdf->Ln(2);	
	  	$pdf -> SetX(115);
		$pdf->Cell(10,3,"Ukupno:",0,0,'L',true); // First header column 
	  	$pdf -> SetX(130);
		$pdf->Cell(20,3,number_format($temp_sum_duguje, 2, '.', ','),0,0,'R',true); // Second header column
		// $pdf->Cell(20,3,$temp_sum_duguje,0,0,'R',true); // Second header column
		$pdf -> SetX(150);
		$pdf->Cell(20,3,number_format($temp_sum_potrazuje, 2, '.', ','),0,0,'R',true); // Second header column
		// $pdf->Cell(20,3,$temp_sum_potrazuje,0,0,'R',true); // Second header column
		$pdf -> SetX(170);
		$pdf->Cell(20,3,number_format($temp_sum_saldo, 2, '.', ','),0,0,'R',true); // Second header column
		//number_format($number, 2, '.', '')

	}

/////////////////////////////////////////////////////////////////////////////////////

$pdf->Output();

?>