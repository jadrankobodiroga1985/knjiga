<?Php
/////////////////////////////////////////// Finansijsku karticu //////////////////////////////////

// Opis:
// Napomena: Potrebno je omoguditi pojedinačan report, tj. Finansijsku karticu koju vidite u nastavku,
// konkretno pri izboru određenog obveznika(customera) da postoji mogudnost dobijanja te Finansijske
// kartice za svaki modul pojedinačno (ako nas interesuju recimo samo lokalne takse) i druga opcija da može
// za obveznika da se dobija Finansijska kartica sa zaduženjima i uplatama po osnovu svih modula. U prilogu
// sam Vam slikala kako trenutno izgleda kartica za modul koji nema uplate iz nekoliko rata i slično, i kako
// izgleda ta kartica za modul Komunalije koje se razlikuju zbog mogudnosti pladanja u ratama.

// http://localhost:3272/Knjiga/test/izvjestaj5.php?customer_id=2&modul_id=2

require('tfpdf.php');
require_once('../private/classes.php');

$modul_id = isset($_GET['modul_id']) ? $_GET['modul_id'] : null;
$datum_od = isset($_GET['datum_od']) ? $_GET['datum_od'] : null;
$datum_do = isset($_GET['datum_do']) ? $_GET['datum_do'] : null;
$customer_id = isset($_GET['customer_id']) ? $_GET['customer_id'] : null; 

// if($customer_id == null){
// 	echo "Potrebno je da unesete klijenta da bi dobili analiticku karticu";
// 	die();
// }

$modul_id_text = " ";
if($modul_id != null){
	$modul_id_text = " and Modul_id=".$modul_id." ";
}

$customer_id_text = " ";
$poziv_na_broj_text = " ";
if($customer_id != null){
	$customer_id_text = " and Customer_id=".$customer_id." ";
	$poziv_na_broj_text = " and Uplata_poziv_na_broj=".$customer_id." ";
}

$target_year = date("Y");
//get_customer_sum_uplate_for_modul

$datum_do_text = "";
if($datum_do != null && $datum_do !=""){
	$datum_do_text = " and '".$datum_do."' >= Ugovor_date ";
	$datum_do_year = DateTime::createFromFormat("Y-m-d", $datum_do);
	if( $datum_do_year->format("Y") != $target_year ) {
		$target_year = $datum_do_year->format("Y");
	}
}

$datum_od_text = "";
if($datum_od != null && $datum_od !=""){
	$datum_od_text = " and '".$datum_od."' <= Ugovor_date ";
	$datum_od_year = DateTime::createFromFormat("Y-m-d", $datum_od);
	if($datum_do != null && $datum_do !=""){
		$datum_do_year = DateTime::createFromFormat("Y-m-d", $datum_do);
		// var_dump($datum_od_year->format("Y"));
		// var_dump($datum_do_year->format("Y"));
		if( $datum_od_year->format("Y") != $datum_do_year->format("Y")) {
			$datum_od_text = " and '".$datum_do_year->format('Y')."-01-01' <= Ugovor_date ";
		}
	}
	else {
		if( $datum_od_year->format("Y") != $target_year ) {
			$target_year = $datum_od_year->format("Y");
		}
	}	
}

$pdf = new tFPDF(); 
$pdf->AddPage();
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu-Bold','','DejaVuSansCondensed-Bold.ttf',true);
$pdf->SetFont('Arial','B',12);
$width=$pdf->GetPageWidth(); // Width of Current Page
$height=$pdf->GetPageHeight(); // Height of Current Page
$width_cell=array(40,20,30,20,20,20,20,20);

$pdf->Image('logo.png',10,6,30);
$pdf->SetFont('Arial','B',15);
$pdf->Cell(50);

$title ='Opstina BAR Sekretarijat za finansije '; 
$pdf->MultiCell(100,10,$title);
$y = $pdf->GetY();
$pdf -> Line(20, $y+8 , $width-20, $y+8);  
$pdf->Ln(4);
$pdf->Ln(6);

$pdf->SetFillColor(255,255,255);
$pdf->SetFont('Arial','B',10);		
$pdf -> SetX(75);
$pdf->Cell(50,10,'FINANSIJSKA KARTICA',0,0,'C',true); 
$pdf->SetFillColor(255,255,255);
$pdf->SetFont('Arial','B',10);		
$pdf -> SetX(170);
$pdf->Cell(20,10,date('Y-m-d'),0,0,'C',true); 
$pdf->Ln(4);
$pdf->Ln(4);

$pdf->SetFillColor(255,255,255);
$pdf->SetFont('Arial','',8);		
$pdf -> SetX(75);
$modul_text_title = "";
if($modul_id != null && $modul_id != ""){
	$modul_object = new atribut($modul_id);
	$modul_text_title = "Modul: ".$modul_object->Name;
}
$pdf->MultiCell(70,5,$modul_text_title,'','L');

$dbhost=Configuration::$dbInfo['dbhost'];
$dbuser=Configuration::$dbInfo['dbuser'];
$dbpass=Configuration::$dbInfo['dbpass'];
$dbname=Configuration::$dbInfo['dbname'];
$connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
$connection2=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
// $output="";  
mysqli_set_charset($connection,"utf8");
mysqli_set_charset($connection2,"utf8");
if (mysqli_connect_error($connection)){
throw new Exception("Problem sa konekcijom nad bazom: ".mysqli_connect_errno($connection).". Molimo kontaktirajte administratora portala.");  
}      
$sql = " select * from bar.orders where Type in ('Zaduzenje','Pocetno_stanje') and  ".$target_year." = YEAR(Ugovor_date) ". $modul_id_text . $datum_do_text . $datum_od_text . $customer_id_text . " order by Customer_id asc";
$results = mysqli_query($connection, $sql);
if(mysqli_num_rows($results)) {	
	$ind = 0;
      while($list = mysqli_fetch_assoc($results)) {  

      	if($ind == 0 || $customer_id == null){      		
	        $customer = new customer($list['Customer_id']);
	        $pdf -> SetX(20);
			$pdf->SetFillColor(255,255,255);
			$pdf->SetFont('DejaVu-Bold','',8);			
			$pdf->Cell(100,8,$customer->Name,0,0,'L',true); // First header column 
			$pdf->SetFont('Arial','B',8);
			$pdf->Ln(4); 
			$pdf->Ln(4);  	
			$ind = 1;
		}
		$y = $pdf->GetY();
      	$pdf -> Line(20, $y , $width-20, $y);   	        		
		// $pdf->Ln(4);  	
		$pdf -> Line(20, $y +2, $width-20, $y +2);   	        		
		$pdf->Ln(8);  

		if($list['Broj_rata'] == null ) {
	    		
				$pdf->SetFillColor(193,229,252);
				$pdf->SetFont('Arial','B',5);
				$pdf -> SetX(20);
				$pdf->Cell($width_cell[0],5,'Broj rijesenja',1,0,'C',true); // First header column 
				$pdf->Cell($width_cell[1],5,'Datum rijesenja',1,0,'C',true); // First header column 
				$pdf->Cell(10,5,'Nalog',1,0,'C',true); // First header column 
				$pdf->Cell(10,5,'Izvod',1,0,'C',true); // First header column 
				$pdf->Cell($width_cell[3],5,'Datum uplate',1,0,'C',true); // Second header column
				$pdf->Cell($width_cell[4],5,'Duguje',1,0,'C',true); // Third header column 
				$pdf->Cell($width_cell[5],5,'Potrazuje',1,0,'C',true); // Third header column 
				$pdf->Cell($width_cell[6],5,'Saldo',1,1,'C',true); // Fourth header column		

				$pdf -> SetX(20);
				$pdf->SetFont('Arial','B',5);
				// Rows of data 
				$pdf->Cell($width_cell[0],5,$list['Ugovor_no'],0,0,'C',false); // First column of row 1 

				$temp_Ugovor_date = $list['Ugovor_date'];
				if($temp_Ugovor_date != null and $temp_Ugovor_date != ""){
		         $temp_Ugovor_date = date("Y-m-d", strtotime($list['Ugovor_date']));
			    } 
				$pdf->Cell($width_cell[1],5,$temp_Ugovor_date,0,0,'C',false); // First column of row 1 

				$pdf->Cell(10,5,$list['Nalog'],0,0,'C',false); // First column of row 1 
				$pdf->Cell(10,5,$list['Bank_account'],0,0,'C',false); // First column of row 1 

				$temp_Ugovor_valuta_date = $list['Ugovor_valuta_date'];
				if($temp_Ugovor_valuta_date != null and $temp_Ugovor_valuta_date != ""){
		         $temp_Ugovor_valuta_date = date("Y-m-d", strtotime($list['Ugovor_valuta_date']));
			    } 
				$pdf->Cell($width_cell[3],5,$temp_Ugovor_valuta_date,0,0,'C',false); // Second column of row 1

				$pdf->Cell($width_cell[4],5,$list['Zaduzenje_iznos'],0,0,'C',false); // Third column of row 1 
				$pdf->Cell($width_cell[5],5,"0.00",0,0,'C',false); // Fourth column of row 1 
				$pdf->Cell($width_cell[6],5,$list['Zaduzenje_iznos'],0,1,'C',false);
		}
		else {

		        $pdf -> SetX(20);
				$pdf->SetFillColor(255,255,255);
				$pdf->SetFont('Arial','B',8);
				$pdf->Cell(100,8,'Broj ugovora: '.$list['Ugovor_no'],0,0,'L',true); // First header column 

				$temp_Ugovor_date = $list['Ugovor_date'];
				if($temp_Ugovor_date != null and $temp_Ugovor_date != ""){
		         $temp_Ugovor_date = date("Y-m-d", strtotime($list['Ugovor_date']));
			    }  
				$pdf->Cell(150,8,'Datum sklapanja ugovora: '.$temp_Ugovor_date,0,0,'L',true); // First header

				$pdf->Ln(4); 
				$pdf->Ln(4);  
				$pdf -> SetX(20);
				$pdf->Cell(100,8,'Ukupno zaduzenje: '.$list['Zaduzenje_iznos'],0,0,'L',true); // First header column 
				$pdf->Cell(150,8,'Broj rata: '.$list['Broj_rata'],0,0,'L',true); // First header 
				$pdf->Ln(4); 
				$pdf->Ln(4); 
				$pdf -> SetX(20);
				$pdf->Cell(100,8,'Napomena: '.$list['Napomena'],0,0,'L',true); // First header column 
				$pdf->Ln(4);
				$pdf->Ln(4);

				$pdf->SetFillColor(193,229,252);
				$pdf->SetFont('Arial','B',5);
				$pdf -> SetX(20);
				$pdf->Cell(10,5,'Rata broj',1,0,'C',true); // First header column 
				$pdf->Cell($width_cell[1],5,'Datum prispijec',1,0,'C',true); // First header column 
				$pdf->Cell(10,5,'Nalog',1,0,'C',true); // First header column 
				$pdf->Cell(10,5,'Izvod',1,0,'C',true); // First header column 
				$pdf->Cell($width_cell[3],5,'Datum uplate',1,0,'C',true); // First header column 
				$pdf->Cell($width_cell[4],5,'Duguje',1,0,'C',true); // Second header column
				$pdf->Cell($width_cell[5],5,'Potrazuje',1,0,'C',true); // Third header column 
				$pdf->Cell($width_cell[6],5,'Saldo',1,0,'C',true); // Third header column 
				$pdf->Cell(40,5,'Napomena',1,1,'C',true); // Fourth header column	
				$pdf->Ln(2);	

				$pdf -> SetX(20);
				$pdf->SetFont('Arial','B',5);
				$pdf->Cell(10,3,'0',0,0,'C',false); // First column of row 1 

				$temp_Ugovor_valuta_date = $list['Ugovor_valuta_date'];
				if($temp_Ugovor_valuta_date != null and $temp_Ugovor_valuta_date != ""){
		         $temp_Ugovor_valuta_date = date("Y-m-d", strtotime($list['Ugovor_valuta_date']));
			    }  
				$pdf->Cell($width_cell[1],3,$temp_Ugovor_valuta_date,0,0,'C',false); // First column of row 1 

				$pdf->Cell(10,3,$list['Nalog'],0,0,'C',false); // First column of row 1 
				$pdf->Cell(10,3,'',0,0,'C',false); // First column of row 1 
				$pdf->Cell($width_cell[3],3,'',0,0,'C',false); // Second column of row 1 
				$pdf->Cell($width_cell[4],3,$list['Zaduzenje_iznos'],0,0,'C',false); // Third column of row 1 
				$pdf->Cell($width_cell[5],3,'0.00',0,0,'C',false); // Fourth column of row 1 
				$pdf->Cell($width_cell[6],3,$list['Zaduzenje_iznos'],0,0,'C',false);
				$pdf->Cell(40,3,$list['Napomena'],0,1,'C',false);
			}
    	
		// $y = $pdf->GetY();
  //     	$pdf -> Line(20, $y , $width-20, $y);   	        		
		// // $pdf->Ln(4);  	
		// $pdf -> Line(20, $y +2, $width-20, $y +2);   	        		
		// $pdf->Ln(4);  	

		$sql2 = " select * from bar.orders where Type in ('Uplata','Storno') and Status!='Deleted'  and  ".$target_year." = YEAR(Uplata_date) and Uplata_poziv_na_broj = ". $list['Id'] ;
        $results2 = mysqli_query($connection2, $sql2);
        if(mysqli_num_rows($results2)) {        	
        	
    		if($list['Broj_rata'] == null ) {   						

				while($list2 = mysqli_fetch_assoc($results2)) {
					$znak1 = "";
					$znak2 = "-";
					if($list2['Type'] == "Storno"){
						$znak1 = "-";
						$znak2 = "";
					}

		            $pdf -> SetX(20);
					$pdf->SetFont('Arial','',5);
					// Rows of data 
					$pdf->Cell($width_cell[0],3,'',0,0,'C',false); // First column of row 1 
					$pdf->Cell($width_cell[1],3,'',0,0,'C',false); // First column of row 1 
					$pdf->Cell(10,3,$list2['Nalog'],0,0,'C',false); // First column of row 1 
					$pdf->Cell(10,3,$list2['Bank_account'],0,0,'C',false); // First column of row 1 

					$temp_Uplata_date = $list2['Uplata_date'];
					if($temp_Uplata_date != null and $temp_Uplata_date != ""){
			         $temp_Uplata_date = date("Y-m-d", strtotime($list2['Uplata_date']));
				    } 
					$pdf->Cell($width_cell[3],3,$temp_Uplata_date,0,0,'C',false); // Second column of row 1

					$pdf->Cell($width_cell[4],3,'0.00',0,0,'C',false); // Third column of row 1 
					$pdf->Cell($width_cell[5],3,$znak1.$list2['Uplata_iznos'],0,0,'C',false); // Fourth column of row 1 
					$pdf->Cell($width_cell[6],3,$znak2.$list2['Uplata_iznos'],0,1,'C',false); // Fourth column of row 1 
					// $pdf->Cell($width_cell[7],5,$list2['Uplata_iznos'],0,1,'C',false); // Fourth column of row 1
		        }	         
	        }
	        else {
		 
				$i = 0;
				while($list2 = mysqli_fetch_assoc($results2)) {
					$znak1 = "-";
					$znak2 = "";
					$broj_rate = 0;
					if($list2['Type'] == "Uplata"){
						$i++;
						$broj_rate = $i;
						$znak1 = "";
						$znak2 = "-";	
					}					
		            $pdf -> SetX(20);
					$pdf->SetFont('Arial','',5);
					$pdf->Cell(10,5,$broj_rate,0,0,'C',false); // First column of row 1 

					$temp_Uplata_rata_date = $list2['Uplata_rata_date'];
					if($temp_Uplata_rata_date != null and $temp_Uplata_rata_date != ""){
			         $temp_Uplata_rata_date = date("Y-m-d", strtotime($list2['Uplata_rata_date']));
				    } 
					$pdf->Cell($width_cell[1],3,$temp_Uplata_rata_date,0,0,'C',false); // First column of row 1

					$pdf->Cell(10,3,$list2['Nalog'],0,0,'C',false); // First column of row 1 
					$pdf->Cell(10,3,$list2['Bank_account'],0,0,'C',false); // First column of row 1 

					$temp_Uplata_date = $list2['Uplata_date'];
					if($temp_Uplata_date != null and $temp_Uplata_date != ""){
			         $temp_Uplata_date = date("Y-m-d", strtotime($list2['Uplata_date']));
				    } 
					$pdf->Cell($width_cell[3],3,$temp_Uplata_date,0,0,'C',false); // Second column of row 1 

					$pdf->Cell($width_cell[4],3,'0.00',0,0,'C',false); // Third column of row 1 
					$pdf->Cell($width_cell[5],3,$znak1.$list2['Uplata_iznos'],0,0,'C',false); // Fourth column of row 1 
					$pdf->Cell($width_cell[6],3,$znak2.$list2['Uplata_iznos'],0,0,'C',false);
					$pdf->Cell($width_cell[7],3,$list2['Napomena'],0,1,'C',false);
		        }

	        }
        } 
        if($list['Broj_rata'] == null ) { 
	        $pdf->Ln(2);     
	        $pdf -> SetX(100);
			$pdf->Cell(20,4,'UKUPNO:',1,0,'C',true); // Second header column
			$pdf -> SetX(122);
			$pdf->Cell(15,4,$list['Zaduzenje_iznos'],0,0,'C',true); // Second header column
			$pdf -> SetX(142);
			$pdf->Cell(15,4,$list['Zaduzenje_uplaceno'],0,0,'C',true); // Second header column
			$pdf -> SetX(162);
			$pdf->Cell(15,4,$list['Zaduzenje_iznos'] - $list['Zaduzenje_uplaceno'],0,0,'C',true); // Second header column
	        $pdf->Ln(4);
	        $pdf->Ln(4);
	    }
        else {
        	$pdf->Ln(2);     
	        $pdf -> SetX(70);
			$pdf->Cell(20,4,'UKUPNO:',1,0,'C',true); // Second header column
			$pdf -> SetX(92);
			$pdf->Cell(15,4,$list['Zaduzenje_iznos'],0,0,'C',true); // Second header column
			$pdf -> SetX(112);
			$pdf->Cell(15,4,$list['Zaduzenje_uplaceno'],0,0,'C',true); // Second header column
			$pdf -> SetX(132);
			$pdf->Cell(15,4,$list['Zaduzenje_iznos'] - $list['Zaduzenje_uplaceno'],0,0,'C',true); // Second header column
	        $pdf->Ln(4);
	        $pdf->Ln(4);

        }
      }
}




// $pdf -> Line(20, 10, $width-20, 10);
// $pdf->Ln(4);   
// $pdf -> SetX(20);
// $pdf->SetFillColor(193,229,252);
// $pdf->SetFont('Arial','',5);
// $pdf->Cell(50,5,'Korisnik:',1,0,'C',true); // First header column 
// $pdf -> SetX(100);
// $pdf->Cell(50,5,'Ukupno zaduzenje',1,0,'C',true); // Second header column
// $pdf -> SetX(160);
// $pdf->Cell(30,5,'Broj rata',1,0,'C',true); // Second header column

// $pdf->Ln(4);   
// $pdf -> SetX(20);
// $pdf->SetFillColor(193,229,252);
// $pdf->SetFont('Arial','',5);
// $pdf->Cell(50,5,'Jadranko Bodiroga:',1,0,'C',true); // First header column 
// $pdf -> SetX(100);
// $pdf->Cell(50,5,'100.99',1,0,'C',true); // Second header column
// $pdf -> SetX(160);
// $pdf->Cell(30,5,'60',1,0,'C',true); // Second header column
// $pdf->Ln(4);   

// $pdf->Ln(4);   
// $pdf -> SetX(20);
// $pdf->SetFillColor(193,229,252);
// $pdf -> SetX(100);
// $pdf->Cell(50,5,'Datum dospijeca:',1,0,'C',true); // Second header column
// $pdf -> SetX(160);
// $pdf->Cell(30,5,'2021-06-01',1,0,'C',true); // Second header column

// $pdf -> SetY(35);
// $pdf->SetFillColor(193,229,252);

// $pdf->SetFont('Arial','',5);
// $pdf -> SetX(20);
// $pdf->Cell($width_cell[0],10,'#',1,0,'C',true); // First header column 
// $pdf->Cell($width_cell[1],10,'Tip',1,0,'C',true); // First header column 
// $pdf->Cell($width_cell[2],10,'Rata broj',1,0,'C',true); // First header column 
// $pdf->Cell($width_cell[3],10,'Datum rate',1,0,'C',true); // Second header column
// $pdf->Cell($width_cell[4],10,'Iznos rate',1,0,'C',true); // Third header column 
// $pdf->Cell($width_cell[5],10,'Izvod',1,0,'C',true); // Third header column 
// $pdf->Cell($width_cell[6],10,'Datum_uplate',1,0,'C',true); // Fourth header column
// $pdf->Cell($width_cell[7],10,'Uplaceno',1,1,'C',true); // Fourth header column
// //// header is over ///////

// $pdf -> SetX(20);
// $pdf->SetFont('Arial','',5);
// // First row of data 
// $pdf->Cell($width_cell[0],10,'1',1,0,'C',false); // First column of row 1 
// $pdf->Cell($width_cell[1],10,'Uplata',1,0,'C',false); // First column of row 1 
// $pdf->Cell($width_cell[2],10,'Rata broj 3',1,0,'C',false); // First column of row 1 
// $pdf->Cell($width_cell[3],10,'2021-01-01',1,0,'C',false); // Second column of row 1 
// $pdf->Cell($width_cell[4],10,'50.00',1,0,'C',false); // Third column of row 1 
// $pdf->Cell($width_cell[5],10,'75',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[6],10,'2021-01-01',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[7],10,'50.00',1,1,'C',false); // Fourth column of row 1 
// //  First row of data is over 
// //  Second row of data 
// $pdf -> SetX(20);
// $pdf->Cell($width_cell[0],10,'2',1,0,'C',false); // First column of row 2 
// $pdf->Cell($width_cell[1],10,'Uplata',1,0,'C',false); // First column of row 2 
// $pdf->Cell($width_cell[2],10,'Rata broj 2',1,0,'C',false); // First column of row 2 
// $pdf->Cell($width_cell[3],10,'2021-01-01',1,0,'C',false); // Second column of row 1 
// $pdf->Cell($width_cell[4],10,'50.00',1,0,'C',false); // Third column of row 1 
// $pdf->Cell($width_cell[5],10,'75',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[6],10,'2021-01-01',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[7],10,'50.00',1,1,'C',false); // Fourth column of row 1 
// //   Sedond row is over 
// //  Third row of data
// $pdf -> SetX(20);
// $pdf->Cell($width_cell[0],10,'3',1,0,'C',false); // First column of row 3
// $pdf->Cell($width_cell[1],10,'Uplata',1,0,'C',false); // First column of row 3
// $pdf->Cell($width_cell[2],10,'Rata broj 1',1,0,'C',false); // First column of row 3
// $pdf->Cell($width_cell[3],10,'2021-01-01',1,0,'C',false); // Second column of row 1 
// $pdf->Cell($width_cell[4],10,'50.00',1,0,'C',false); // Third column of row 1 
// $pdf->Cell($width_cell[5],10,'75',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[6],10,'2021-01-01',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[7],10,'50.00',1,1,'C',false); // Fourth column of row 1 
   
// // $pdf -> Line(20, 80, $width-20, 80); 
// $pdf->Ln(4); //Break
//////////////////////////////////////////////Korisnik/////////////////////////////////////

// $pdf -> Line(20, 90, $width-20, 90);
// $pdf->Ln(4);   
// $pdf -> SetX(20);
// $pdf->SetFillColor(193,229,252);
// $pdf->SetFont('Arial','',5);
// $pdf->Cell(50,5,'Korisnik:',1,0,'C',true); // First header column 
// $pdf -> SetX(100);
// $pdf->Cell(50,5,'Ukupno zaduzenje',1,0,'C',true); // Second header column
// $pdf -> SetX(160);
// $pdf->Cell(30,5,'Broj rata',1,0,'C',true); // Second header column

// $pdf->Ln(4);   
// $pdf -> SetX(20);
// $pdf->SetFillColor(193,229,252);
// $pdf->SetFont('Arial','',5);
// $pdf->Cell(50,5,'Dubravko Bodiroga:',1,0,'C',true); // First header column 
// $pdf -> SetX(100);
// $pdf->Cell(50,5,'1110.99',1,0,'C',true); // Second header column
// $pdf -> SetX(160);
// $pdf->Cell(30,5,'6',1,0,'C',true); // Second header column
// $pdf->Ln(4);   

// $pdf->Ln(4);   
// $pdf -> SetX(20);
// $pdf->SetFillColor(193,229,252);
// $pdf -> SetX(100);
// $pdf->Cell(50,5,'Datum dospijeca:',1,0,'C',true); // Second header column
// $pdf -> SetX(160);
// $pdf->Cell(30,5,'2021-06-01',1,0,'C',true); // Second header column

// $pdf -> SetY(105);
// $pdf -> SetX(20);
// $pdf->SetFillColor(193,229,252);
// $pdf->SetFont('Arial','',5);
// $pdf->Cell($width_cell[0],10,'Broj',1,0,'C',true); // First header column 
// $pdf->Cell($width_cell[1],10,'Datum rate',1,0,'C',true); // Second header column
// $pdf->Cell($width_cell[2],10,'Iznos rate',1,0,'C',true); // Third header column 
// $pdf->Cell($width_cell[3],10,'Izvod',1,0,'C',true); // Third header column 
// $pdf->Cell($width_cell[4],10,'Datum_uplate',1,0,'C',true); // Fourth header column
// $pdf->Cell($width_cell[5],10,'Uplaceno',1,1,'C',true); // Fourth header column
// //// header is over ///////
// $pdf -> SetX(20);
// $pdf->SetFont('Arial','',5);
// // First row of data 
// $pdf->Cell($width_cell[0],10,'1',1,0,'C',false); // First column of row 1 
// $pdf->Cell($width_cell[1],10,'2021-01-01',1,0,'C',false); // Second column of row 1 
// $pdf->Cell($width_cell[2],10,'50.00',1,0,'C',false); // Third column of row 1 
// $pdf->Cell($width_cell[3],10,'75',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[4],10,'2021-01-01',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[5],10,'50.00',1,1,'C',false); // Fourth column of row 1 
// //  First row of data is over 
// //  Second row of data 
// $pdf -> SetX(20);
// $pdf->Cell($width_cell[0],10,'2',1,0,'C',false); // First column of row 2 
// $pdf->Cell($width_cell[1],10,'2021-01-01',1,0,'C',false); // Second column of row 1 
// $pdf->Cell($width_cell[2],10,'50.00',1,0,'C',false); // Third column of row 1 
// $pdf->Cell($width_cell[3],10,'75',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[4],10,'2021-01-01',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[5],10,'50.00',1,1,'C',false); // Fourth column of row 1 
// //   Sedond row is over 
// //  Third row of data
// $pdf -> SetX(20);
// $pdf->Cell($width_cell[0],10,'3',1,0,'C',false); // First column of row 3
// $pdf->Cell($width_cell[1],10,'2021-01-01',1,0,'C',false); // Second column of row 1 
// $pdf->Cell($width_cell[2],10,'50.00',1,0,'C',false); // Third column of row 1 
// $pdf->Cell($width_cell[3],10,'75',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[4],10,'2021-01-01',1,0,'C',false); // Fourth column of row 1 
// $pdf->Cell($width_cell[5],10,'50.00',1,1,'C',false); // Fourth column of row 1 
   
// $pdf -> Line(20, 150, $width-20, 150); 
// $pdf->Ln(4); //Break

/////////////////////////////////////////////////////////////////////////////////////

$pdf->Output();

?>