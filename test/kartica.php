
<?php
///////////////////////////////////////////  kartica //////////////////////////////////
require('tfpdf.php');
require_once('../private/classes.php');


$order_id = isset($_GET['order_id']) ? $_GET['order_id'] : null; 
$order = new order($order_id);
$customer = new customer($order->Customer_id);
$modul = new atribut($order->Modul_id);

$pdf = new tFPDF(); 
$pdf->AddPage();
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu-Bold','','DejaVuSansCondensed-Bold.ttf',true);
$pdf->SetFont('Arial','B',12);
$width=$pdf->GetPageWidth(); // Width of Current Page
$height=$pdf->GetPageHeight(); // Height of Current Page
$width_cell=array(40,20,30,15,15,15,15,20);

$pdf->Image('logo.png',10,6,30);
$pdf->SetFont('Arial','B',15);
$pdf->Cell(50);

$title ='Opstina BAR Sekretarijat za finansije '; 
$pdf->MultiCell(100,10,$title);
$y = $pdf->GetY();
$pdf -> Line(20, $y+8 , $width-20, $y+8);  
$pdf->Ln(4);
$pdf->Ln(6);

$pdf->SetFillColor(255,255,255);
$pdf->SetFont('Arial','B',10);		
$pdf -> SetX(75);
$pdf->Cell(50,10,'FINANSIJSKA KARTICA',0,0,'C',true); 
$pdf->SetFillColor(255,255,255);
$pdf->SetFont('Arial','B',10);		
$pdf -> SetX(170);
$pdf->Cell(20,10,date('Y-m-d'),0,0,'C',true); 
$pdf->Ln(4);
$pdf->Ln(4);

$pdf->SetFillColor(255,255,255);
$pdf->SetFont('DejaVu','',10);		
$pdf -> SetX(75);

$pdf->MultiCell(70,5,$modul->Name,'','L');
// die();
$dbhost=Configuration::$dbInfo['dbhost'];
$dbuser=Configuration::$dbInfo['dbuser'];
$dbpass=Configuration::$dbInfo['dbpass'];
$dbname=Configuration::$dbInfo['dbname'];
$connection=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
$connection2=mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
// $output="";  
mysqli_set_charset($connection,"utf8");
mysqli_set_charset($connection2,"utf8");
if (mysqli_connect_error($connection)){
	throw new Exception("Problem sa konekcijom nad bazom: ".mysqli_connect_errno($connection).". Molimo kontaktirajte administratora portala.");  
}      
$sql = " select * from bar.orders where Id=".$order->Id;
$results = mysqli_query($connection, $sql);
if(mysqli_num_rows($results)) {	
	$ind = 0;
      while($list = mysqli_fetch_assoc($results)) {  

    $pdf -> SetX(20);
		$pdf->SetFillColor(255,255,255);
		$pdf->SetFont('DejaVu-Bold','',8);
		$pdf->Cell(100,8,"Korisnik: ".$customer->Name,0,0,'L',true); // First header column 
		// $pdf->Cell(100,8,"Korisnik: ".(iconv('UTF-8', 'iso-8859-2', $customer->Name)),0,0,'L',true); // First header column 
		$pdf->Ln(4); 
		$pdf->Ln(4);  	
		$ind = 1;
		// }
		$y = $pdf->GetY();
      	$pdf -> Line(20, $y , $width-20, $y);   	        		
		// $pdf->Ln(4);  	
		$pdf -> Line(20, $y +2, $width-20, $y +2);   	        		
		$pdf->Ln(4);		

        $pdf -> SetX(20);
		$pdf->SetFillColor(255,255,255);
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(100,8,'Broj ugovora: '.$list['Ugovor_no'],0,0,'L',true); // First header column 

		$temp_Ugovor_date = $list['Ugovor_date'];
		if($temp_Ugovor_date != null and $temp_Ugovor_date != ""){
         $temp_Ugovor_date = date("Y-m-d", strtotime($list['Ugovor_date']));
	    }  
		$pdf->Cell(150,8,'Datum sklapanja ugovora: '.$temp_Ugovor_date,0,0,'L',true); // First header

		$pdf->Ln(4); 
		$pdf->Ln(4);  
		$pdf -> SetX(20);
		$pdf->Cell(100,8,'Ukupno zaduzenje: '.$list['Zaduzenje_iznos'],0,0,'L',true); // First header column 
		$pdf->Cell(150,8,'Broj dozvoljenih rata: '.$list['Broj_rata'],0,0,'L',true); // First header 
		$pdf->Ln(4); 
		$pdf->Ln(4); 
		$pdf -> SetX(20);
		$pdf->Cell(100,8,'Napomena: '.$list['Napomena'],0,0,'L',true); // First header column 
		$pdf->Ln(4);
		$pdf->Ln(4);

		$pdf->SetFillColor(193,229,252);
		$pdf->SetFont('Arial','B',5);
		$pdf -> SetX(20);
		$pdf->Cell(10,5,'Tip naloga',1,0,'C',true); // First header column 
		$pdf->Cell(10,5,'Rata broj',1,0,'C',true); // First header column 
		$pdf->Cell(15,5,'Dat.prisp.rate',1,0,'C',true); // First header column 
		$pdf->Cell(10,5,'Nalog',1,0,'C',true); // First header column 
		$pdf->Cell(10,5,'Izvod',1,0,'C',true); // First header column 
		$pdf->Cell(10,5,'Budzet',1,0,'C',true); // First header column 
		$pdf->Cell(15,5,'Datum uplate',1,0,'C',true); // First header column 
		$pdf->Cell($width_cell[4],5,'Duguje',1,0,'C',true); // Second header column
		$pdf->Cell($width_cell[5],5,'Potrazuje',1,0,'C',true); // Third header column 
		$pdf->Cell($width_cell[6],5,'Saldo',1,0,'C',true); // Third header column 
		$pdf->Cell(45,5,'Napomena',1,1,'C',true); // Fourth header column	
		// $pdf->Ln(2);	 	

		$sql2 = " select * from bar.orders where Type in ('Uplata','Storno','Info') and Status!='Deleted'  and Uplata_poziv_na_broj = ". $list['Id'] ;
        $results2 = mysqli_query($connection2, $sql2);
        if(mysqli_num_rows($results2)) {        	
        	    		       		 
			$i = 0;
			while($list2 = mysqli_fetch_assoc($results2)) {
				$znak1 = "-";
				$znak2 = "";
				$broj_rate = 0;
				if($list2['Type'] == "Uplata"){
					$i++;
					$broj_rate = $i;
					$znak1 = "";
					$znak2 = "-";	
				}					
        $pdf -> SetX(20);
				$pdf->SetFont('Arial','',5);

				$temp_type=$list2['Type'];
				if($temp_type == 'Info'){
					$temp_type = 'Zaduzenje';
				}
				$pdf->Cell(10,4,$temp_type,1,0,'C',false); // First column of row 1 

				$pdf->Cell(10,4,$broj_rate,1,0,'C',false); // First column of row 1 

				$temp_Uplata_rata_date = $list2['Uplata_rata_date'];
				if($temp_Uplata_rata_date != null and $temp_Uplata_rata_date != ""){
		         $temp_Uplata_rata_date = date("Y-m-d", strtotime($list2['Uplata_rata_date']));
			    } 
				$pdf->Cell(15,4,$temp_Uplata_rata_date,1,0,'C',false); // First column of row 1

				$pdf->Cell(10,4,$list2['Nalog'],1,0,'C',false); // First column of row 1 
				$pdf->Cell(10,4,$list2['Bank_account'],1,0,'C',false); // First column of row 1 
				$pdf->Cell(10,4,$list2['Budzet'],1,0,'C',false); // First column of row 1 

				$temp_Uplata_date = $list2['Uplata_date'];
				if($temp_Uplata_date != null and $temp_Uplata_date != ""){
		         $temp_Uplata_date = date("Y-m-d", strtotime($list2['Uplata_date']));
			    } 
				$pdf->Cell(15,4,$temp_Uplata_date,1,0,'C',false); // Second column of row 1 

				$pdf->SetFont('Arial','B',8);
				$temp_duguje='0.00';
				if($list2['Zaduzenje_iznos'] != null){
					$temp_duguje = $list2['Zaduzenje_iznos'];
				}
				$pdf->Cell($width_cell[4],4,$temp_duguje,1,0,'R',false); // Third column of row 1 

				$temp_potrazuje='0.00';
				if($list2['Uplata_iznos'] != null){
					$temp_potrazuje = $znak1.$list2['Uplata_iznos'];
				}
				$pdf->Cell($width_cell[5],4,$temp_potrazuje,1,0,'R',false); // Fourth column of row 1 

				$pdf->Cell($width_cell[6],4,'',1,0,'R',false);
				$pdf->Cell(45,4,$list2['Napomena'],1,1,'C',false);

	        }
        } 
      $pdf->SetFont('Arial','B',8);
    	$pdf->Ln(2);     
      $pdf -> SetX(80);
			$pdf->Cell(17,4,'UKUPNO:',1,0,'C',true); // Second header column
			$pdf -> SetX(100);
			$pdf->Cell(15,4,number_format($list['Zaduzenje_iznos'], 2, '.', ','),1,0,'R',true); // Second header column
			$pdf -> SetX(115);
			$pdf->Cell(15,4,number_format($list['Zaduzenje_uplaceno'], 2, '.', ','),1,0,'R',true); // Second header column
			$pdf -> SetX(130);
			$pdf->Cell(15,4,number_format((0-($list['Zaduzenje_iznos'] - ($list['Zaduzenje_uplaceno']))), 2, '.', ','),1,0,'R',true); // Second header column
        $pdf->Ln(4);
        $pdf->Ln(4);
       
      }
}

/////////////////////////////////////////////////////////////////////////////////////

$pdf->Output();
// $pdf->Output("pdf.pdf");
// echo $pdf;

?>


