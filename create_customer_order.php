<?php 
  require_once("private/classes.php");
  $modul = new atribut($_GET['modul_id']);
  // echo "id:".$modul->Id;
  // echo "Name:".$modul->Name;

  // $order = new order($_GET['order_id']);
  // $customer = new customer($order->Customer_id);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com -->
  <title>Modifikovanje modula</title>
  <!-- <meta charset="utf-8"> -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="live_css/bootstrap.min.css">
  <link href="live_css/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="live_css/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="live_css/jquery.min.js"></script>
  <script src="live_css/bootstrap.min.js"></script>
  <style>
  body {
    font: 400 15px Lato, sans-serif;
    line-height: 1.8;
    color: #818181;
  }
  h2 {
    font-size: 24px;
    text-transform: uppercase;
    color: #303030;
    font-weight: 600;
    margin-bottom: 30px;
  }
  h4 {
    font-size: 19px;
    line-height: 1.375em;
    color: #303030;
    font-weight: 400;
    margin-bottom: 30px;
  }  
  .jumbotron {
    background-color: #f4511e;
    color: #fff;
    padding: 100px 25px;
    font-family: Montserrat, sans-serif;
  }
  .container-fluid {
    padding: 60px 50px;
  }
  .bg-grey {
    background-color: #f6f6f6;
  }
  .logo-small {
    color: #f4511e;
    font-size: 50px;
  }
  .logo {
    color: #f4511e;
    font-size: 200px;
  }
  .thumbnail {
    padding: 0 0 15px 0;
    border: none;
    border-radius: 0;
  }
  .thumbnail img {
    width: 100%;
    height: 100%;
    margin-bottom: 10px;
  }
  .carousel-control.right, .carousel-control.left {
    background-image: none;
    color: #f4511e;
  }
  .carousel-indicators li {
    border-color: #f4511e;
  }
  .carousel-indicators li.active {
    background-color: #f4511e;
  }
  .item h4 {
    font-size: 19px;
    line-height: 1.375em;
    font-weight: 400;
    font-style: italic;
    margin: 70px 0;
  }
  .item span {
    font-style: normal;
  }
  .panel {
    border: 1px solid #f4511e; 
    border-radius:0 !important;
    transition: box-shadow 0.5s;
  }
  .panel:hover {
    box-shadow: 5px 0px 40px rgba(0,0,0, .2);
  }
  .panel-footer .btn:hover {
    border: 1px solid #f4511e;
    background-color: #fff !important;
    color: #f4511e;
  }
  .panel-heading {
    color: #fff !important;
    background-color: #f4511e !important;
    padding: 25px;
    border-bottom: 1px solid transparent;
    border-top-left-radius: 0px;
    border-top-right-radius: 0px;
    border-bottom-left-radius: 0px;
    border-bottom-right-radius: 0px;
  }
  .panel-footer {
    background-color: white !important;
  }
  .panel-footer h3 {
    font-size: 32px;
  }
  .panel-footer h4 {
    color: #aaa;
    font-size: 14px;
  }
  .panel-footer .btn {
    margin: 15px 0;
    background-color: #f4511e;
    color: #fff;
  }
  .navbar {
    margin-bottom: 0;
    background-color: #f4511e;
    z-index: 9999;
    border: 0;
    font-size: 12px !important;
    line-height: 1.42857143 !important;
    letter-spacing: 4px;
    border-radius: 0;
    font-family: Montserrat, sans-serif;
  }
  .navbar li a, .navbar .navbar-brand {
    color: #fff !important;
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
    color: #f4511e !important;
    background-color: #fff !important;
  }
  .navbar-default .navbar-toggle {
    border-color: transparent;
    color: #fff !important;
  }
  footer .glyphicon {
    font-size: 20px;
    margin-bottom: 20px;
    color: #f4511e;
  }
  .slideanim {visibility:hidden;}
  .slide {
    animation-name: slide;
    -webkit-animation-name: slide;
    animation-duration: 1s;
    -webkit-animation-duration: 1s;
    visibility: visible;
  }
  @keyframes slide {
    0% {
      opacity: 0;
      transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      transform: translateY(0%);
    }
  }
  @-webkit-keyframes slide {
    0% {
      opacity: 0;
      -webkit-transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      -webkit-transform: translateY(0%);
    }
  }
  @media screen and (max-width: 768px) {
    .col-sm-4 {
      text-align: center;
      margin: 25px 0;
    }
    .btn-lg {
      width: 100%;
      margin-bottom: 35px;
    }
  }
  @media screen and (max-width: 480px) {
    .logo {
      font-size: 150px;
    }
  }
  td ,th ,tr {
    text-align: center;
    font-weight: bold;
    font-size: 15px;
    height: 12px;
  }

  /* Dropdown Button */
.dropbtn {
  background-color: #04AA6D;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
  cursor: pointer;
}

/* Dropdown button on hover & focus */
.dropbtn:hover, .dropbtn:focus {
  background-color: #3e8e41;
}

/* The search field */
#myInput {
  box-sizing: border-box;
  /*background-image: url('searchicon.png');*/
  background-position: 14px 12px;
  background-repeat: no-repeat;
  font-size: 16px;
  padding: 14px 20px 12px 45px;
  border: none;
  border-bottom: 1px solid #ddd;
}

/* The search field when it gets focus/clicked on */
#myInput:focus {outline: 3px solid #ddd;}

/* The container <div> - needed to position the dropdown content */
.dropdown {
  position: relative;
  display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f6f6f6;
  min-width: 230px;
  border: 1px solid #ddd;
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #f1f1f1}

/* Show the dropdown menu (use JS to add this class to the .dropdown-content container when the user clicks on the dropdown button) */
.show {display:block;}
  </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60" style="background-color: grey">

<nav class="navbar navbar-default navbar-fixed-top" style="background-color: darkslategrey">
  <div class="container" style="width: 100%;margin-left: 0px;padding: 15px;">
    <div class="navbar-header" style="width:50%">

      
      <img src="logo.jpg" style="height:60px;float: left;">
      <div style="width:80%;float: left;margin-left: 10px;">
        <a class="navbar-brand" style="margin-left: 10px;float: none;width: 100%;font-size: 13px;">Modul: <?php echo $modul->Name; ?> </a>
        <input id="modul_id" type="hidden" value=" <?php echo $modul->Id; ?> " > 
      </div>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="index.php?modul_id=<?php echo $_GET['modul_id']; ?>">Nazad</a></li>
        
        <li><a href="logout.php">LogOut</a></li>
        <!-- <li><a href="#pricing">PRICING</a></li> -->
        <!-- <li><a href="#contact">CONTACT</a></li> -->
      </ul>
    </div>
  </div>
</nav>
<div style="background-color: grey;">
  <div style="width:100%;">
  </div>
</div>

<div class="jumbotron text-center" style="background-color: grey;height: 500px;">
  <div style="width:100%;height: 350px;float: left;">
    <form method="post" action="create_order.php" style="width: 100%">

    <input type="text" id="customer_id" name="customer_id" value="" required style="opacity: 0;">
    <input type="hidden" id="modul_id" name="modul_id" value="<?php echo $modul->Id; ?>" >    
    <input type="hidden" id="modul_value" name="modul_value" value="<?php echo $modul->Name; ?>" >

      <div style="float:left; width:100%; ">
        <br> <br> <br>
        <div style="width:30%;float: left;">

            <div style="width:100%;border:0px solid black;float:left;"> 
              <div style="width: 40%;float:left;font-size: 14px;padding: 5px;">
                   <label style="margin-left:0px"><b>*Vrsta naloga: </b></label>
              </div>
              <!-- <div style="width: 60%;float:left;color: black; font-size: 19px;">
                   <input class="w3-input w3-border w3-margin-bottom" type="text" value="Zaduzenje"  name="tip" id="tip"  style="width:100%" required >    
              </div> -->
               <select class="w3-input w3-border w3-margin-bottom" id="Status" name="tip" style="width:60%;color:black;" >
                   <option value="Zaduzenje">Zaduzenje</option>
                   <option value="Pocetno_stanje">Pocetno_stanje</option>
                </select>        
            </div>
            
            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 14px;padding: 5px;">
                   <label style="margin-left:0px"><b>*Broj ugovora: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black; font-size: 19px;">
                   <input class="w3-input w3-border w3-margin-bottom" type="text" value=""  name="broj_ugovora" id="broj_ugovora" style="width:100%" required >    
              </div>    
            </div>
            
            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 13px;padding: 5px;">
                   <label style="margin-left:0px"><b>*Datum sklapanja ugovora: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black; font-size: 19px;">
                   <input type="date" class="w3-input w3-border w3-margin-bottom"  value=""  name="datum_ugovora" id="datum_ugovora" style="width:100%" required > 
                   
              </div>    
            </div>

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 13px;padding: 5px;">
                   <label style="margin-left:0px"><b>*Datum pocetka placanja: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black; font-size: 19px;">
                   <input type="date" class="w3-input w3-border w3-margin-bottom" type="date('Y-m-d')" value=""  name="datum_kraja_ugovora" id="datum_kraja_ugovora" style="width:100%" required >    
              </div>    
            </div>

             <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 14px;padding: 5px; height: 24%;">
                   <label style="margin-left:0px"><b>Napomena </b></label>
              </div>
              <div style="width: 60%;float:left;color: black;">
                   <!-- <input class="w3-input w3-border w3-margin-bottom" type="text" value="Neka napomena text,text,text,text....Napomena"  name="Zaduzenje" style="width:100%">     -->
                   <textarea style="width:100%; font-size: 17px; height: 24%;" name="Napomena" id="Napomena" cols="40"  rows="3" ></textarea>
              </div>    
            </div>

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 14px;padding: 5px;">
                   <label style="margin-left:0px"><b>Nalog: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black; font-size: 19px;">
                   <input class="w3-input w3-border w3-margin-bottom" type="text" value=""  name="Nalog" id="Nalog" style="width:100%"  >    
              </div>    
            </div>

        </div>

        <div style="width:50%;float: left;">

          <div style="width:55%;float:left;">
            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 14px;padding: 5px;">
                   <label style="margin-left:0px"><b>*Iznos zaduzenja: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black; font-size: 19px;">
                   <input class="w3-input w3-border w3-margin-bottom" type="text" value="0.00"  name="iznos_zaduzenje" id="iznos_naloga2" style="width:100%"  onchange="updejtuj_rate();" required >    
              </div>    
            </div>

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 13px;padding: 5px;">
                   <label style="margin-left:0px"><b>Broj rata za uplatu: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black; font-size: 19px;">
                   <input class="w3-input w3-border w3-margin-bottom" type="text" value="0"  name="broj_rata" id="broj_rata2" style="width:100%" onchange="updejtuj_rate();">    
              </div>    
            </div>

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 14px;padding: 5px;">
                   <label style="margin-left:0px"><b>Iznos jedne rate: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black; font-size: 19px;">
                   <input class="w3-input w3-border w3-margin-bottom" type="text" value="0.00"  name="iznos_jedne_rate" id="iznos_rate2" style="width:100%" >    
              </div>    
            </div>

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 13px;padding: 5px;">
                   <label style="margin-left:0px"><b>Datum pocetka rata: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black; font-size: 19px;">
                   <input type="date" class="w3-input w3-border w3-margin-bottom" type="date('Y-m-d')" value=""  name="datum_pocetka_rata" id="datum_pocetka_rata" style="width:100%" >    
              </div>    
            </div>

           <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 14px;padding: 5px;">
                   <label style="margin-left:0px"><b>Iznos prve rate: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black; font-size: 19px;">
                   <input class="w3-input w3-border w3-margin-bottom" type="text" value="0.00"  name="iznos_prve_rate" id="iznos_prve_rate2" style="width:100%" onchange="updejtuj_rate();">    
              </div>    
            </div>

            <div style="width:100%;border:0px solid black;float:left;">
              <div style="width: 40%;float:left;font-size: 14px;padding: 5px;">
                   <label style="margin-left:0px"><b>Datum prve rate: </b></label>
              </div>
              <div style="width: 60%;float:left;color: black; font-size: 19px;">
                   <input type="date" class="w3-input w3-border w3-margin-bottom" type="date('Y-m-d')" value=""  name="datum_prve_rate" id="datum_prve_rate" style="width:100%" >    
              </div>    
            </div>
            <script type="text/javascript">
          
          function updejtuj_rate(){

            var temp_iznos = document.getElementById('iznos_naloga2').value;
            var temp_broj_rata = document.getElementById('broj_rata2').value;
            var temp_iznos_rate = document.getElementById('iznos_rate2').value;
            var temp_iznos_prve_rate = document.getElementById('iznos_prve_rate2').value;

            if(temp_broj_rata > 1 && temp_iznos > 0){
              if(temp_iznos_prve_rate > 0){
                temp_iznos_rate = (temp_iznos - temp_iznos_prve_rate)/(temp_broj_rata - 1 );
              }
              else {
                temp_iznos_rate = (temp_iznos)/(temp_broj_rata); 
              }
            }
            else {
              temp_iznos_rate = 0.00;
            }            
            document.getElementById('iznos_rate2').value = temp_iznos_rate.toFixed(2);
          }

        </script>

           

          </div>

           <div style="width:45%;float:left;"> 

           <div class="dropdown" style="width:100%">
              <button type="button" onclick="myFunction()" class="dropbtn" style="width: 80%">Izaberi klijenta</button>
              <div id="myDropdown" class="dropdown-content">
                <input type="text" placeholder="" id="myInput" onkeyup="filterFunction()" style="color:black;width: 100%;">
                <?php  echo order::get_all_list_of_customers(); ?>                 
              </div>
              <div>
                 <input type="text"  id="customer_name" style="color:black;width: 100%;" readonly required>
              </div>
            </div>   

            <script type="text/javascript">

            /* When the user clicks on the button,
              toggle between hiding and showing the dropdown content */
              function myFunction() {
                document.getElementById("myDropdown").classList.toggle("show");
              }

              function filterFunction() {
                var input, filter, ul, li, a, i;
                input = document.getElementById("myInput");
                filter = input.value.toUpperCase();
                div = document.getElementById("myDropdown");
                a = div.getElementsByTagName("a");
                for (i = 0; i < a.length; i++) {
                  txtValue = a[i].textContent || a[i].innerText;
                  if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    a[i].style.display = "";
                  } else {
                    a[i].style.display = "none";
                  }
                }
              }

              function set_customer(cust_id,cust_name){
                // alert("Izabrali ste klijenta:"+cust_name);
                document.getElementById('customer_id').value = cust_id;
                document.getElementById('customer_name').value = cust_name;
                document.getElementById("myDropdown").classList.toggle("show");
              }
            
            // function redirect_to_order(order_id){
            //   alert('klinio'+order_id);
            //   window.location.replace("./index5.php?order_id="+order_id);
            // }

          </script>       
             

            <div style="width:100%;border:2px solid black;float:left;margin-top: 0px; height: 60px;">              
              <div style="width:100%;">
                <button type="submit" value="submit" id="submit" name="submit" class="button" style="width:100%;background-color:blue; height: 55px;"> Kreiraj zaduzenje</button>
              </div>  
            </div>             

          </div>

        </div>

        <div style="width:20%;border:0px solid black;float:left;">
          <div style="width: 100%;float:left;font-size: 14px;padding: 5px;">
               <label style="margin-left:0px;font-size:25px;color: white"><b style="color: white"><?php if(isset($_SESSION['info'])){ echo $_SESSION['info']; } ?> </b></label>
          </div>
            
        </div>


        </div>

      </form>
  

      </div>

      <div style="width: 30%;float: left;height: 350px;">

          <div style="width:100%;">
            <h1 style="font-size: 40px;float: right;font-style: italic;margin-top: 0px;"></h1> 
          </div>

          <p style="font-size: 12px;float: left;width: 100%;text-align: right;margin-bottom: 0px;"></p></br>
          <p style="font-size: 12px;float: left;width: 100%;text-align: right;margin-bottom: 0px"></p></br>
          <p style="font-size: 12px;float: left;width: 100%;text-align: right;margin-bottom: 0px"></p> </br>
          <p style="font-size: 12px;float: left;width: 100%;text-align: right;margin-bottom: 0px"></p> </br>
          <p style="font-size: 12px;float: left;width: 100%;text-align: right;margin-bottom: 0px"></p> </br>

      </div>

  </div>



  

<!-- Container (About Section) -->
<div id="about" class="container-fluid" style="padding-top: 0px;">
 



</div>




</body>
</html>
