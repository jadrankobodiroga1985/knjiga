<?php 
  require_once("private/classes.php");

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com -->
  <title>Izvjestaji</title>
  <!-- <meta charset="utf-8"> -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="live_css/bootstrap.min.css">
  <link href="live_css/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="live_css/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="live_css/jquery.min.js"></script>
  <script src="live_css/bootstrap.min.js"></script>

  <link rel="stylesheet" href="live_css/w3.css">
  <style>
  body {
    font: 400 15px Lato, sans-serif;
    line-height: 1.8;
    color: #818181;
  }
  h2 {
    font-size: 24px;
    text-transform: uppercase;
    color: #303030;
    font-weight: 600;
    margin-bottom: 30px;
  }
  h4 {
    font-size: 19px;
    line-height: 1.375em;
    color: #303030;
    font-weight: 400;
    margin-bottom: 30px;
  }  
  .jumbotron {
    background-color: #f4511e;
    color: #fff;
    padding: 100px 25px;
    font-family: Montserrat, sans-serif;
  }
  .container-fluid {
    padding: 60px 50px;
  }
  .bg-grey {
    background-color: #f6f6f6;
  }
  .logo-small {
    color: #f4511e;
    font-size: 50px;
  }
  .logo {
    color: #f4511e;
    font-size: 200px;
  }
  .thumbnail {
    padding: 0 0 15px 0;
    border: none;
    border-radius: 0;
  }
  .thumbnail img {
    width: 100%;
    height: 100%;
    margin-bottom: 10px;
  }
  .carousel-control.right, .carousel-control.left {
    background-image: none;
    color: #f4511e;
  }
  .carousel-indicators li {
    border-color: #f4511e;
  }
  .carousel-indicators li.active {
    background-color: #f4511e;
  }
  .item h4 {
    font-size: 19px;
    line-height: 1.375em;
    font-weight: 400;
    font-style: italic;
    margin: 70px 0;
  }
  .item span {
    font-style: normal;
  }
  .panel {
    border: 1px solid #f4511e; 
    border-radius:0 !important;
    transition: box-shadow 0.5s;
  }
  .panel:hover {
    box-shadow: 5px 0px 40px rgba(0,0,0, .2);
  }
  .panel-footer .btn:hover {
    border: 1px solid #f4511e;
    background-color: #fff !important;
    color: #f4511e;
  }
  .panel-heading {
    color: #fff !important;
    background-color: #f4511e !important;
    padding: 25px;
    border-bottom: 1px solid transparent;
    border-top-left-radius: 0px;
    border-top-right-radius: 0px;
    border-bottom-left-radius: 0px;
    border-bottom-right-radius: 0px;
  }
  .panel-footer {
    background-color: white !important;
  }
  .panel-footer h3 {
    font-size: 32px;
  }
  .panel-footer h4 {
    color: #aaa;
    font-size: 14px;
  }
  .panel-footer .btn {
    margin: 15px 0;
    background-color: #f4511e;
    color: #fff;
  }
  .navbar {
    margin-bottom: 0;
    background-color: #f4511e;
    z-index: 9999;
    border: 0;
    font-size: 12px !important;
    line-height: 1.42857143 !important;
    letter-spacing: 4px;
    border-radius: 0;
    font-family: Montserrat, sans-serif;
  }
  .navbar li a, .navbar .navbar-brand {
    color: #fff !important;
  }
  .navbar-nav li a:hover, .navbar-nav li.active a {
    color: #f4511e !important;
    background-color: #fff !important;
  }
  .navbar-default .navbar-toggle {
    border-color: transparent;
    color: #fff !important;
  }
  footer .glyphicon {
    font-size: 20px;
    margin-bottom: 20px;
    color: #f4511e;
  }
  .slideanim {visibility:hidden;}
  .slide {
    animation-name: slide;
    -webkit-animation-name: slide;
    animation-duration: 1s;
    -webkit-animation-duration: 1s;
    visibility: visible;
  }
  @keyframes slide {
    0% {
      opacity: 0;
      transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      transform: translateY(0%);
    }
  }
  @-webkit-keyframes slide {
    0% {
      opacity: 0;
      -webkit-transform: translateY(70%);
    } 
    100% {
      opacity: 1;
      -webkit-transform: translateY(0%);
    }
  }
  @media screen and (max-width: 768px) {
    .col-sm-4 {
      text-align: center;
      margin: 25px 0;
    }
    .btn-lg {
      width: 100%;
      margin-bottom: 35px;
    }
  }
  @media screen and (max-width: 480px) {
    .logo {
      font-size: 150px;
    }
  }
  td ,th ,tr {
    text-align: center;
    font-weight: bold;
    font-size: 15px;
    height: 12px;
  }
  </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60" style="background-color: grey;">

<nav class="navbar navbar-default navbar-fixed-top" style="background-color: darkslategrey">
  <div class="container" style="width: 100%;margin-left: 0px;padding: 15px;">
    <div class="navbar-header" style="width:50%">
      <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button> -->
      
      <img src="logo.jpg" style="height:60px;float: left;">
      <div style="width:80%;float: left;margin-left: 10px;">
        <br>
        <a class="navbar-brand" style="margin-left: 10px;float: none;width: 100%;font-size: 22px;">Izaberite report: </a>
        
      </div>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        
        <li><a href="moduls.php">Moduli</a></li>
        <li><a href="logout.php">LogOut</a></li>
        <!-- <li><a href="#pricing">PRICING</a></li> -->
        <!-- <li><a href="#contact">CONTACT</a></li> -->
      </ul>
    </div>
  </div>
</nav>
<div style="background-color: grey;">
  <div style="width:100%;">
  </div>
</div>

<div class="jumbotron text-center" style="background-color: grey;height: 350px;">
  <div style="width:100%;height: 350px;float: left;">    
    
    <div style="float:center; width:100%; ">
        <div style="width:100%;border: 1px solid whitesmoke;">
          <!-- <br><br> -->
          <div style="width: 40%; border: 2px solid black;float: left;">
              <div style="width:100%;border:1px solid black;float:left;height: 50px;">
                <label for="Pocetak" style="float:left">Datum Od:</label>
                <div style="width: 70%;float:right;color: black; font-size: 16px;">
                     <input class="w3-input w3-border w3-margin-bottom" type="date" name="datum_od" id="datum_od"  style="width:100%">    
                </div>    
              </div>
              </br>
              <div style="width:100%;border:1px solid black;float:left;">
                <label for="Kraj" style="float:left">Datum Do:</label>
                <div style="width: 70%;float:right;color: black; font-size: 16px;">
                     <input class="w3-input w3-border w3-margin-bottom" type="date" name="datum_do" id="datum_do"  style="width:100%">    
                </div>    
              </div>
              </br>
              <div style="width: 100%;border: 1px solid black;margin-top: 50px;"  >     
                <div style="width:100%">
                   <button onclick="document.getElementById('id01').style.display='block'" class="w3-button w3-green w3-large" style="float: left;color: black;">Izaberi klijenta</button>
                   <input type="racun" style="width: 70%;float: right;color: black;" size="50" placeholder="svi" class="form-control" id="customer_name" size="50"  name="customer_name" readonly>
                   <input type="hidden" class="form-control" id="customer_id" size="50" value="all"  name="customer_id">  
                </div>
                </br>
                <div style="width: 100%;border: 1px solid black;margin-top: 40px;">
                  <button  type="button" onclick="document.getElementById('id03').style.display='block'" class="w3-button w3-green w3-large" style="float: left;color: black;">Izaberi modul</button>
                  <input type="racun" style="width:70%;float:right;color: black;" size="50" placeholder="svi"  style="margin-top: 10px;background: ghostwhite;"  id="modul_name" name="modul_name" readonly>
                  <input type="hidden"  size="50" placeholder=""  style="width: 50%;margin-left: 20xp;"  name="modul_id" id="modul_id" value = "all">
                </div>                           
                           
            </div>          
          </div>

          <div style="width:60%;float: left;border: 1px solid black;">

              <div style="width:50%;float:left;">

                <div style="width:100%;border:0px solid black;float:right;margin-top: 0px; height: 60px;">             
                  <div style="width:100%;">
                    <button  id="izvjestaj1" type="button" class="button" style="width:100%;background-color:blue; height: 55px;" onClick="get_report(1)"> Analiticka kartica obveznika</button>

                    <!-- <button type="button" class="btn" style="width:100%;background-color:blue; height: 55px;" onclick="get_report2()">Basic</button> -->
                    <!-- <input type="button" value="Analiticka kartica obveznika" onClick="get_report2()"> -->

                  </div>  
                </div>

                 <div style="width:100%;border:0px solid black;float:left;margin-top: 0px; height: 60px;">            
                  <div style="width:100%;">
                    <button type="button"  class="button" style="width:100%;background-color:blue; height: 55px;" onClick="get_report(2);"> Spisak svih obveznika</button>
                  </div>  
                </div> 

                <div style="width:100%;border:0px solid black;float:left;margin-top: 0px; height: 60px;">            
                  <div style="width:100%;">
                    <button type="button"  class="button" style="width:100%;background-color:blue; height: 55px;" onclick="get_report(3);"> Neizmirena dugovanja </button>
                  </div>  
                </div> 

                <div style="width:100%;border:0px solid black;float:left;margin-top: 0px; height: 60px;">            
                  <div style="width:100%;">
                    <button type="button"  class="button" style="width:100%;background-color:blue; height: 55px;" onclick="get_report(4);"> Presjek stanja (ukupno zaduženje/ukupne uplate) </button>
                  </div>  
                </div> 

              </div>   

              <div style="width:50%;float:left;">

                <div style="width:100%;border:0px solid black;float:right;margin-top: 0px; height: 60px;">             
                  <div style="width:100%;">
                    <button type="button"  class="button" style="width:100%;background-color:blue; height: 55px;" onclick="get_report(5);">Finansijska kartica</button>
                  </div>  
                </div>

            </div>

            </div>

           

            <!-- <div> -->
                


<!-- Klijent Start -->

    <div id="id01" style="display:none" class="w3-modal">
      <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:1000px">

        <div class="w3-center"><br>
          <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal" style="background-color:red">&times;</span>
          <!-- <img src="img_avatar4.png" alt="Avatar" style="width:30%" class="w3-circle w3-margin-top"> -->
        </div>

        
          <div class="input-group">

            <div class="dropdown" style="margin-left: 5%;">
              <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown" style="float: left;">Filtar
              <span class="caret"></span></button>
              <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                <li role="presentation"><a role="menuitem" tabindex="-1" onclick="filtar_set('PIB');">PIB</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" onclick="filtar_set('PDV_broj');">PDV broj</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" onclick="filtar_set('Name');">Naziv klijenta</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" onclick="filtar_set('JMBG');">JMBG</a></li>

                <!-- <li role="presentation" class="divider"></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ostalo...</a></li> -->
              </ul>
            </div>
            <script type="text/javascript">             

             function get_report(num){
              // alert("usao opet");
                   prvi_znak = '?';

                   modul_id = document.getElementById('modul_id').value;
                   modul_id_txt = "";
                  if(modul_id != null && modul_id != 'all' && modul_id != ''){
                    modul_id_txt = prvi_znak+'modul_id='+modul_id;
                    prvi_znak = '&';
                  }

                   datum_od = document.getElementById("datum_od").value;
                   datum_od_txt = "";
                  if(datum_od != null  && datum_od != 'all'  && datum_od != ''){
                    datum_od_txt = prvi_znak+'datum_od='+datum_od;
                    prvi_znak = '&';
                  }

                   datum_do = document.getElementById("datum_do").value;
                   datum_do_txt = "";
                  if(datum_do != null  && datum_do != 'all'  && datum_do != ''){
                    datum_do_txt = prvi_znak+'datum_do='+datum_do;
                    prvi_znak = '&';
                  }

                   customer_id = document.getElementById("customer_id").value;
                   customer_id_txt = "";
                  if(customer_id != null  && customer_id != 'all'  && customer_id != ''){
                    customer_id_txt = prvi_znak+'customer_id='+customer_id;
                    prvi_znak = '&';
                  }
                  URL_txt = './test/izvjestaj'+num+'.php'+modul_id_txt+datum_od_txt+datum_do_txt+customer_id_txt;

                  // alert('num:'+num+' modul_id:'+modul_id+' datum_od:'+datum_od+' datum_do:'+datum_do+' customer_id:'+customer_id)
                  // alert('URL_txt:'+URL_txt);    
                  window.open(URL_txt,'_blank');              
                }

              var filtar = "";
              function filtar_set(new_filtar){
                filtar = new_filtar;
                // alert("filtar"+filtar);
              }

              function get_customers_filtared_list(){
                var filtar_value = document.getElementById('search_customer_input').value;
                // alert("filtar_value"+filtar_value);
                var xhttp; 
                xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                  if (this.readyState == 4 && this.status == 200) {
                    // alert("rezultat: "+this.responseText);  
                    document.getElementById("customers_filtared_list").innerHTML = this.responseText;
                    }          
                };             
                xhttp.open("GET", "get_orders_configuration_AJAX.php?filtar="+filtar+"&filtar_value=" + filtar_value +"&process=customers", true);
                xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xhttp.send( );
              }

            </script>

            <input id="search_customer_input" type="email" class="form-control" size="50" placeholder="Unesite željenu riječ ..." required style="width: 50%;margin-left: 20xp;">
            <div class="input-group-btn" style="float: left;">
              <button type="button" class="btn btn-danger" onclick="get_customers_filtared_list();">Pretraži</button>
            </div>
          </div>

        <div id="about" class="container-fluid" style="padding-top: 0px;">
        <table class="table table-bordered" style="color:black">
          <thead>
            <tr>
              <th>#</th>
              <th>Kompanija:</th>
              <th>Zaduzenje:</th>
              <th>Uplate:</th>
              <th>Saldo:</th>
              <th style="width:50px"></th>
              <!-- <th>Dugovanje u valuti:</th> -->
            </tr>
          </thead>
          <tbody id="customers_filtared_list"> 
            <tr>
            <td>0</td>
            <td>Svi klijenti</td>
            <td></td>
            <td></td>
            <td></td>
            <td><button type="button" class="btn btn-primary" onclick="change_customer('all','svi');">Odaberi</button></td>
            </tr>
          </tbody>
        </table>
      </div>

      <script type="text/javascript">
              
          function change_customer(customer_id,customer_name){
            document.getElementById('customer_id').value = customer_id;
            document.getElementById('customer_name').value = customer_name;
            document.getElementById('id01').style.display='none';

            document.getElementById("customers_filtared_list").innerHTML = "<tr>"+
            "<td>0</td>"+
            "<td>Svi klijenti</td>"+
            "<td></td>"+
            "<td></td>"+
            "<td></td>"+
            "<td><button type=\"button\" class=\"btn btn-primary\" onclick=\"change_customer('all','svi');\">Odaberi</button></td>"+
            "</tr>";
          }

      </script>

      </div>
    </div>
<!-- Klijent END -->

<!-- Modul Start -->

     <div id="id03" style="display:none" class="w3-modal">
      <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:1000px">

        <div class="w3-center"><br>
          <span onclick="document.getElementById('id03').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal" style="background-color:red">&times;</span>
        </div>        
        <div id="about" class="container-fluid" style="padding-top: 0px;">
        <table class="table table-bordered" style="color:black">
          <thead>
            <tr>
              <th>#</th>
              <th>Modul:</th>  
              <th style="width:50px"></th>            
            </tr>
          </thead>
          <tbody id="promijeni_modul">
            <tr>
            <td>0</td>
            <td>Svi moduli</td>
            <td><a><button type="button" class="btn btn-primary" onclick="odaberi_modul('all','all');">Odaberi</button></a></td>
            </tr>

            <?php echo atribut::get_atributs_for_orders(); ?>
           
          <script type="text/javascript">
            
            function odaberi_modul(modul_id,modul_opis){
              document.getElementById('modul_name').value = modul_opis;
              document.getElementById('modul_id').value = modul_id;
              document.getElementById('id03').style.display='none';
            }

          </script>
            
          </tbody>
        </table>
      </div>
      </div>
    </div>

<!-- Modul END -->


        <!-- </div> -->


    </div>

  </div>



  </div>

   <script type="text/javascript">
    
   

  </script>


<div id="about" class="container-fluid" style="padding-top: 0px;">

  <table id="tabela" class="table table-bordered">
    
      
        

 
  </table>

</div>


</body>
</html>
